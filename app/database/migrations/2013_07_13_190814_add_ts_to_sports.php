<?php

use Illuminate\Database\Migrations\Migration;

class AddTsToSports extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
   public function up()
   {
      Schema::table('sports', function($table) {
         $table->timestamps();
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::table('sports', function($table) {
         $table->dropColumn('updated_at');
         $table->dropColumn('created_at');
      });
   }

}