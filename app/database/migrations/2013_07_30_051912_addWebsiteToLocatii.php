<?php

use Illuminate\Database\Migrations\Migration;

class AddWebsiteToLocatii extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
   public function up()
   {
      Schema::table('locatii', function($table) {
         $table->string('website', 500);
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::table('locatii', function($table) {
         $table->dropColumn('website');
      });
   }

}