<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEvenimenteAsSubcategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
      Schema::table('sports_subcategorii', function($table) {

         $table->engine = 'InnoDB';

         $table->bigIncrements('id');
      });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
      Schema::table('sports_subcategorii', function($table)
      {
         $table->dropColumn('id');
      });
	}

}
