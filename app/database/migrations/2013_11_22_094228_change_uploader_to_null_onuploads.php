<?php

use Illuminate\Database\Migrations\Migration;

class ChangeUploaderToNullOnuploads extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('alter table uploads modify column uploader_id int(11) null;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('alter table uploads modify column uploader_id int(11) not null;');
	}

}