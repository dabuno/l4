<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
      Schema::rename('notifications', 'site_notifications');
		Schema::create('notifications', function($t)
      {
         $t->increments('id');
         $t->integer('user_id');
         $t->tinyInteger('suntconvocat')->default(0);
         $t->tinyInteger('evenimentnou')->default(0);
         $t->tinyInteger('comentariuprofil')->default(0);
         $t->tinyInteger('comentariueveniment')->default(0);
         $t->tinyInteger('competitienoua')->default(0);
         $t->tinyInteger('functionalitatenoua')->default(0);
         $t->string('basetoken', 100);
         $t->timestamps();

         $t->index('user_id');
      });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications');
      Schema::rename('site_notifications', 'notifications');
	}

}
