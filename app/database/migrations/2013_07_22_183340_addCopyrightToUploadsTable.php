<?php

use Illuminate\Database\Migrations\Migration;

class AddCopyrightToUploadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
   public function up()
   {
      Schema::table('uploads', function($table) {
         $table->string('copyright', 500);
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::table('uploads', function($table) {
         $table->dropColumn('copyright');
      });
   }
}