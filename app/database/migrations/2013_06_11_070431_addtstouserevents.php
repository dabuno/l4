<?php

use Illuminate\Database\Migrations\Migration;

class Addtstouserevents extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_events', function($table) {
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_events', function($table) {
			$table->dropColumn('updated_at');
			$table->dropColumn('created_at');
		});
	}

}