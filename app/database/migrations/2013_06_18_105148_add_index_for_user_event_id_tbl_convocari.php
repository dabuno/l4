<?php

use Illuminate\Database\Migrations\Migration;

class AddIndexForUserEventIdTblConvocari extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('convocari', function($table) {
			$table->index('user_event_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('convocari', function($table) {
			$table->dropIndex('user_event_id');
		});
	}

}