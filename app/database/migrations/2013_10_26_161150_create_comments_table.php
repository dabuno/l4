<?php

use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
      Schema::create('comments', function($table) {
         
         $table->engine = 'InnoDB';

         $table->bigIncrements('id');
         $table->bigInteger('parent')->nullable()->default(null);
         $table->string('hashurl', 32);
         $table->string('url', 250);
         $table->string('comment', 500);
         $table->integer('owner');
         $table->boolean('visible')->default(1);
         $table->timestamps();
         $table->softDeletes();

         //#indexes
         $table->index('hashurl');
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::drop('comments');
   }

}