<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationEvents extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
      Schema::create('notifications_events', function($t)
      {
         $t->increments('id');
         $t->integer('user_id');
         $t->integer('sport_id');
         $t->integer('oras_id');
         $t->integer('locatie_id');
         $t->timestamps();

         $t->index('user_id');
      });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications_events');
	}

}
