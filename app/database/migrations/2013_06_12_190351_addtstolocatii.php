<?php

use Illuminate\Database\Migrations\Migration;

class Addtstolocatii extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
   public function up()
   {
      Schema::table('locatii', function($table) {
         $table->timestamps();
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::table('locatii', function($table) {
         $table->dropColumn('updated_at');
         $table->dropColumn('created_at');
      });
   }

}