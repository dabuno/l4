<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IncreaseLocationDescription extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE locatii MODIFY COLUMN descriere TEXT, ADD COLUMN program TEXT after `descriere`');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE locatii MODIFY COLUMN descriere varchar(500), DROP COLUMN program');
	}

}
