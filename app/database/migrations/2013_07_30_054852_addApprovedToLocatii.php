<?php

use Illuminate\Database\Migrations\Migration;

class AddApprovedToLocatii extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
   public function up()
   {
      Schema::table('locatii', function($table) {
         $table->boolean('approved');
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::table('locatii', function($table) {
         $table->dropColumn('approved');
      });
   }
}