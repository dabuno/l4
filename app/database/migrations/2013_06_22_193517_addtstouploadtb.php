<?php

use Illuminate\Database\Migrations\Migration;

class Addtstouploadtb extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
   public function up()
   {
      Schema::table('uploads', function($table) {
         $table->timestamps();
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::table('uploads', function($table) {
         $table->dropColumn('updated_at');
         $table->dropColumn('created_at');
      });
   }
}