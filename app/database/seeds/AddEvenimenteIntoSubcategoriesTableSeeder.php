<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AddEvenimenteIntoSubcategoriesTableSeeder extends Seeder {

	public function run()
	{
      $subcategorie = Subcategorie::whereName('Evenimente')->first();
      if ($subcategorie)
         $subcategorie->delete();
      $subcategorie = new Subcategorie;
      $subcategorie->name = 'Evenimente';
      $subcategorie->save();

      $subsub = SportSubcategorie::where('id_sub', '=','2')->get();
      foreach ($subsub as $sub) {
         $sub->id_sub = $subcategorie->id;
         $sub->save();
      }
	}

}