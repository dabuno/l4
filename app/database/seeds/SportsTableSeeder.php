<?php

class SportsTableSeeder extends Seeder {

   public function run()
   {
      $data = array(array('name' => 'Squash', 'active' => 1));

     // Uncomment the below to run the seeder
      DB::table('sports')->insert($data);
   }

}