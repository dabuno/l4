<?php

class NotificationTableSeeder extends Seeder {

    public function run()
    {
        $notification = array(
            ["dest_id" => 4, "msg" =>"test msg"]
        );

        // Uncomment the below to run the seeder
         DB::table('notifications')->insert($notification);
    }

}