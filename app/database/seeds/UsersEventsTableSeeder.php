<?php

class UsersEventsTableSeeder extends Seeder {

  public function run()
  {
    // Uncomment the below to wipe the table clean before populating
    // DB::table('events_seed')->delete();

    $events_seed = array(

    );

    $users = User::active()->with('sports')->get();
    foreach ($users as $value) {
       $validId[] = $value;
    }
    $rand = rand(0, count($validId) - 1);

    $all = Locatie::active()->get();
    foreach ($all as $value) {
       $arene[] = $value;
    }
    $randar = rand(0, count($arene) - 1);




    // Uncomment the below to run the seeder
    //DB::table('users_events')->insert($events_seed);
      UserEvent::create([
          'event' => 'event' . uniqid(),
          'sport_id'      => $validId[$rand]->sports[0]->id_sport ?: rand(3,4),
          'data'       => date('Y-m-d', strtotime('+'.rand(1,10).' days')),
          'ora'        => date('H:i'),
          'oras_id' => rand(1,42),
          'arena_id'      => $arene[$randar]->id,
          'pret'       => (microtime(true)*0xFFFF),
          'locuri'     => rand(1,10),
          'user_id'     => $validId[$rand]->id,
      ]);
  }

}