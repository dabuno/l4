<?php


Route::filter('before', function()
{
   // Do stuff before every request to your application...
   $uri = URI::current();
   $pattern = '#^admin($|(?!/login)/.*)$#';

   die();exit();
   if (preg_match($pattern, $uri)) {
      // YOUR admin/auth part starts here
      return 'you are now going through the filter';
      // YOUR admin/auth part ends here
   }

});

Route::filter('sportnamecheck', function() {

  $sport = Request::segment(2);

  $sport = Sport::where('name', '=', $sport)->first();

  if (!$sport || !$sport->id) {
     Log::error('['.__METHOD__.'] sport name is not correct ['.$sport.']');
     return Redirect::to("/");
  }
  View::share('sport', compact('sport'));
});


Route::filter('old', function(){
   if (Input::get('age') < 200)
   {
       return Redirect::to('users/' . Input::get('age'));
   }

});

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

use Illuminate\Database\Eloquent\ModelNotFoundException;

App::error(function(ModelNotFoundException $e)
{
   Log::error(__METHOD__ . ' an exeption occured for model ['.$e->getModel().'] with exeption ' . $e);
   return Response::make('Ups, o eroare a aparut.', 404);
});

App::before(function($request)
{
	//

});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('login');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

/*
Event::listen('404', function() {
  die(404);
});

Event::listen('user.login', function($user)
{
    $user->last_login = new DateTime;

    $user->save();

    die();
});
*/