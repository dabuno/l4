<?php

function prarr($arr)
{
   return '';


   foreach ($arr as $a) {
      echo $a['created_at'] .',';
   }
   echo "\n";
}

/**
 * binary searches for $elem in $arr using $compare_fn given as parameter
 * @param  [type]  $elem       [description]
 * @param  [type]  $arr        [description]
 * @param  integer $offset     [description]
 * @param  [type]  $compare_fn [description]
 * @return [type]              [description]
 */
function binary_search($elem, $arr, $offset = 0, $compare_fn)
{
   prarr($arr, '', 'a1');
   $foffset = 0;
   if (!count($arr))
   {
      return $offset;
   }

   if (count($arr) == 1)
   {
      if ($compare_fn($arr[0], $elem) > 0)
      {
         return $offset;
      }
      return ++$offset;
   }


   $middle_position = (int)(count($arr) / 2);
   $middle_value = $arr[$middle_position];


   // printf("mp|mv|elem|offset\n");
   // printf(" %d| %d|   %d|    %d\n", $middle_position, $middle_value, $elem, $offset);

   if ($compare_fn($middle_value, $elem) > 0)
   {
      prarr($arr->slice(0, $middle_position));
      return $offset + binary_search($elem, $arr->slice(0, $middle_position), 0, $compare_fn);
   }
   prarr($arr->slice($middle_position + 1));
   return $offset + binary_search($elem, $arr->slice($middle_position + 1), $middle_position + 1, $compare_fn);

}


function merge($a1, $a2, $compare_fn)
{
   foreach ($a2 as $a) {
      $position = binary_search($a, $a1, $offset = 0, $compare_fn);

      $a1left = $a1->slice(0, $position);
      $a1left->add($a);
      $a1right = $a1->slice($position);
      $a1right->each(function($elem) use ($a1left){
         $a1left->add($elem);
      });
      $a1 = $a1left;
   }

   return $a1;
}

/**
 * formats a tel phone number. expecting a 10 digit string or $tel will be returned
 * @return string $tel or formated tel: 0723.123.123
*/
function fmtel($tel)
{
   if (!preg_match('@(^[0-9]{4})([0-9]{3})([0-9]{3})@', $tel, $matches))
   {
      Log::error(__METHOD__ . ' failed to match regex on tel['.$tel.']');
      return $tel;
   }

   if (!isset($matches) || count($matches) != 4)
   {
      return $tel;
   }

   list($no, $prefix, $middle, $end) = $matches;

   return $prefix . '.' . $middle . '.' . $end;
}

function showComments($current, $comments, $htmlstart, $htmlend, $commentsObj) {
   static $visited = [];
   static $stack = 0;

   foreach ($current as $id => $threads) {
      if (!$id || !isset($comments[$id]) || isset($visited[$id])) {
         continue;
      }

      $visited[$id] = true;

      $comment = $commentsObj[$id];

      if ($stack) {
         echo '<div class="comment-child">';
      }

      echo '<div id="comment-'. $comment->id . '" class="comment-entry">';
      echo '
      <div class="comment-header">
      <a name="comment-'. $comment->id.'"></a>' .
      ($comment->users ? '<a href="#">'. a_profile($comment->users) .'</a>' : 'anonim') .
      ' <span class="faded">'. acumTime($comment->created_at).'</span>
      </div>
      <div class="comment-body">
         <p>' . e($comment->comment) .'</p>
      </div>';

      echo '
      <div class="comment-footer">
         <div class="comment-msg"></div>
      <div class="comment-reply-body bhidden">'.
      Form::textarea('comment', Input::old('comment'), [
         'class' => 'comment-reply-entry comment-new',
         'placeholder' => 'Raspunde...',
         'style' => 'margin-top: 10px; overflow: auto; width:400px; height:100px']) . '
         <div>
            <a href="javascript:void(0);" class="btn comment-trimite">Raspunde</a>
            <span class="comment loading bhidden"><img src="' . URL::to('img/ajax-loader.gif') . '"/></span>
            <a href="javascript:void(0);" class="btn comment-cancel">Anuleaza</a>
         </div>
      </div>
      ' . (is_logged() ? '<a href="javascript:void(0);" class="faded comment-reply">raspunde</a>' : '') .'
      </div>';

      $stack++;

      if (gettype($comments[$id]) == 'array') {
         showComments($comments[$id], $comments, $htmlstart, $htmlend, $commentsObj);
      }

      $stack--;
      if ($stack) {
         echo '</div>';
      }
      echo '</div>';
   }
}

function acumTime($datetime) {

   $pref = 'acum ';

   $time = strtotime('now') - strtotime($datetime);


   if ($time <= 1) {
      return $pref . 'o secunda';
   }

   if ($time > 1 && $time < 60) {
      return $pref . $time . ' secunde';
   }

   if ($time < 120) {
      return $pref . 'un minut';
   }

   if ($time >= 120 && $time < 3600) {
      return $pref . round($time / 60) .' minute';
   }

   if ($time >= 3600 && $time <= 7200) {
      return $pref . 'o ora';
   }
   if ($time > 7200 && $time < 86400) {
      return $pref . round($time / 3600) .' ore';
   }

   if ($time >= 86400 && $time < 172800) {
      return $pref . 'o zi';
   }

   if ($time >= 172800 && $time < 31536000) {
      return $pref . round($time / 86400) .' zile';
   }

   if ($time >= 31536000 && $time < 63072000) {
      return $pref . 'un an';
   }

   if ($time >= 63072000) {
      return $pref . round($time / 31536000) .' ani';
   }


}


function ucf($str) {
   $str = ucfirst(strtolower($str));
   return $str;
}

function lower($str) {
   $str = strtolower($str);
   return $str;
}

