<?php

Validator::extend('noxss_tags', function($attribute, $value, $parameters)
{
   if (!$value) {
      return false;
   }

   if (strip_tags($value) != $value) {
      return false;
   }


   return true;
});


Validator::extend('testrecapthca', function($attribute, $value, $parameters)
{
   $captcha = new Services\Validators\privateReCaptcha;
   $response = $captcha->check();
   
   if (!$response->isValid()) {
      return false;
   }

   return true;
});


Validator::extend('multipleint', function($attribute, $value, $parameters)
{
   if (!$value || !is_array($value)) {
      return false;
   }

   foreach ($value as $asval) {
      if (!is_numeric($asval) || $asval <= 0) {
         return false;
      }
   }

   return true;
});

Validator::extend('alpha_spaces', function($attribute, $value)
{
   return preg_match('/^([-a-z0-9_-\s])+$/i', $value);
});
