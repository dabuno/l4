<?php

define ('ROLE_ADMIN',      100);
define ('ROLE_DEMO',         0);
define ('ROLE_USER',         1);
define ('ROLE_MGR_ARENA',    5);

function is($role) {
   if (Auth::guest()) {
      return false;
   }

   if ((Auth::user()->role & $role) == $role) {
      return Auth::user()->id;
   }

   return false;
}

function is_logged() {
   if (Auth::guest()) {
      return false;
   }
   return Auth::user()->id;
}

function is_admin() {
   return is(ROLE_ADMIN);
}

function is_demo() {
   return is(ROLE_DEMO);
}

function is_user() {
   return is(ROLE_USER);
}

function is_mgr_arena() {
   return is(ROLE_ADMIN);
}

function href_locatie($locatie) {
   return URL::to('/locatii/' . preg_replace('@[^0-9a-zA-Z-]@', '-', $locatie->name) . '-' . $locatie->id);
}

function a_locatie($locatie) {
   if (!$locatie) {
      Log::error('invalid $locatie supplied');
      return '';
   }
   return '<a href="'.href_locatie($locatie).'" title="'.htmlentities($locatie->name).'">'.htmlentities($locatie->name).'</a>';
}

function full_a_locatie($locatie) {
   return '<a href="'.URL::to(href_locatie($locatie)).'" title="'.htmlentities($locatie->name).'">'.htmlentities($locatie->name).'</a>';
}

/**
 * user hrefs
 * @param  User   $user [description]
 * @return [type]       [description]
 */
function href_profile($user, $comment = '') {
   return URL::to('/' . preg_replace('@[^0-9a-zA-Z-]@', '-', $user->alias) . '-' . $user->id . ($comment ? '#comment-' . $comment : ''));
}

function a_profile($user) {
   return '<a href="'.href_profile($user).'" title="'.htmlentities($user->alias).'">'.htmlentities($user->alias).'</a>';
}

function full_a_profile($user, $comment = '') {
   return '<a href="'.URL::to(href_profile($user, $comment)).'" title="'.htmlentities($user->alias).'">'.htmlentities($user->alias).'</a>';
}


/**
 * Event hrefs
 * @param  Event   $Event [description]
 * @return [type]       [description]
 */
function href_event($event, $comment = '') {
   return route('events.show' , $event->id);
}

function a_event($event) {
   return '<a href="'.href_event($event).'" title="'.htmlentities($event->event).'">'.htmlentities($event->event).'</a>';
}

function full_a_event($event, $comment = '') {
   return '<a href="'.URL::to(href_event($event, $comment)).'" title="'.htmlentities($event->event).'">'.htmlentities($event->event).'</a>';
}