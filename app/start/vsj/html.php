<?php

Html::macro('flash', function($template = '_parts.flash')
{
    $message_status = Session::get('message_status');
    $message        = Session::get('message');
    
    return View::make($template)
        ->with('message', $message)
        ->with('message_status', $message_status)
        ->render();
});

Html::macro('oddeven', function($name = 'default')
{
   static $status = array();

   if (!isset($status[$name]))
   {
      $status[$name] = 0;
   }

   $status[$name] = 1 - $status[$name];

   return (($status[$name] % 2 == 0) ? 'even' : 'odd');
});

Html::macro('skipfirst', function($name = 'default', $html)
{
   static $status = array();

   if (!isset($status[$name]))
   {
      $status[$name] = 0;
   }
   else {
      $status[$name] = 1;
   }
   
   
   if ($status[$name]) {
      return $html;
   }

});