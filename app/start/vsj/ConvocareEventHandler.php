<?php

Event::listen('convocare.*', function($convocare)
{
   $eventName = Event::firing();
   Log::debug(__METHOD__ . ' event: ' . $eventName . ' for convocare from src ['.$convocare->src_id.'] to dest ['.$convocare->dest_id.']');

   $sendTo = $convocare->dest_id;
   if ($eventName == 'convocare.acceptata' || $eventName == 'convocare.refuzata') {
      Log::debug(__METHOD__ . ' event ['.$eventName.'] requires email to be sent to the event owner ['.$convocare->src_id.']');
      $sendTo = $convocare->src_id;
   }
   else {
      Log::debug(__METHOD__ . ' event ['.$eventName.'] requires email to be sent to the event destination ['.$convocare->dest_id.']');
   }

   $notificari = Notification::with('user')->whereUserId($sendTo)->first();

   if ($notificari && $notificari->suntconvocat) {

      Log::debug(__METHOD__ . ' email notification activated for user ['.$notificari->user_id.']');

      switch($eventName) {

         /**
          * Notifies Dest that Src has challenged him
          * Dest is the one receiving the email
          **/
         case 'convocare.adaugata':
            $subject = 'Ai fost invitat sa joci ' . $convocare->events->sports->name;
            $body = full_a_profile($convocare->srcids) . ' te-a invitat sa joci '.$convocare->events->sports->name.' la evenimentul ' . full_a_event($convocare->events);
            $queueEvent = (new ConvocareEvent($convocare->destids, $convocare, $subject, $body))->queueEvent();
            break;


         /**
          * Notifies challenged person that convocare is canceled
          * Dest is the one receiving the email
          **/
         case 'convocare.anulata':
            $subject = 'Invitatie anulata pentru evenimentul ' . $convocare->events->event;
            $body = full_a_profile($convocare->srcids) . ' a anulat invitatia pentru evenimentul ' . full_a_event($convocare->events);
            $queueEvent = (new ConvocareEvent($convocare->destids, $convocare, $subject, $body))->queueEvent();
            break;


         /**
          * Notifies back convocare initiator to let him know who accepted.
          * Src is the one receiving the email
          **/
         case 'convocare.acceptata':
            $subject = 'Invitatie acceptata pentru evenimentul ' . $convocare->events->event;
            $body = full_a_profile($convocare->destids) . ' a acceptat invitatia pentru evenimentul ' . full_a_event($convocare->events);
            $queueEvent = (new ConvocareEvent($convocare->srcids, $convocare, $subject, $body))->queueEvent();
            break;


         /**
          * Notifies back convocare initiator to let him know who refused.
          * Src is the one receiving the email
          **/
         case 'convocare.refuzata':
            $subject = 'Invitatie acceptata pentru evenimentul ' . $convocare->events->event;
            $body = full_a_profile($convocare->destids) . ' a refuzat invitatia pentru evenimentul ' . full_a_event($convocare->events);
            $queueEvent = (new ConvocareEvent($convocare->srcids, $convocare, $subject, $body))->queueEvent();
            break;


         default:
            Log::warning(__METHOD__ . ' unhandled event ['.$eventName.']');
            break;
      }
   }
   else {
      Log::debug(__METHOD__ . ' email notification NOT activated for user ['.$convocare->dest_id.']');
   }

});


class ConvocareEvent {
   protected $toUser;
   protected $convocare;
   protected $subject;
   protected $body;
   protected $emailTemplate = 'emails.invitatie-eveniment';

   public function __construct(User $toUser,Convocare $convocare, $subject, $body, $template = null) {
      $this->toUser  = $toUser;
      $this->convocare  = $convocare;
      $this->subject = $subject;
      $this->body    = $body;
      $this->emailTemplate = $template ?: $this->emailTemplate;
   }

   public function queueEvent()
   {
      $userId        = $this->toUser->id;
      $convocareId   = $this->convocare->id;
      $subject       = $this->subject;
      $body          = $this->body;
      $emailTemplate = $this->emailTemplate;

      Queue::push(function($job) use ($userId, $subject, $body, $emailTemplate) {
         $user = User::find($userId);
         $data = array('body' => $body);

         Mail::send($emailTemplate, $data, function($message) use ($user, $subject) {
            Log::debug(__METHOD__ . ' sending email notification to ['.$user->email.']');
            $message->to($user->email, $user->alias)
                    ->subject($subject);
         });

         $job->delete();
      });
   }

}