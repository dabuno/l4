<?php

Comment::created(function($comment)
{
   Log::debug(__METHOD__ . 'added comment [' . $comment->id . ']');


   $entity_name = null;
   if (preg_match('@(.*)/([1-9][0-9]*)@', $comment->url, $matches)) {
      $entity_name = $matches[1];
      Log::debug(__METHOD__ . ' found entity_name [' . $entity_name .'] from url ['. $comment->url .']');
   }
   else {
      Log::error('['.__METHOD__.'] failed to find entity name for url: ['.$comment->url.']');
   }

   switch ($entity_name) {
      case 'profile':
         $event = Event::fire('profile.comment', array($comment));
         break;

      case 'events':
         $event = Event::fire('events.comment', array($comment));
         break;

      default:
         Log::error(__METHOD__ . ' got unhandled entity_name: ['.$entity_name.']');
         break;
   }

});

Event::listen('events.comment', function($comment)
{
   Log::debug(__METHOD__ . ' ' . Event::firing() . ' comment from ['.$comment->owner.'] to ['.$comment->entity_id.']');

   $event = UserEvent::with('users')->find($comment->entity_id);
   if (!$event) {
      Log::error(__METHOD__ . ' event not found for id ['.$comment->entity_id.']');
   }
   else {
      $notificari = Notification::with('user')->where('user_id', '=', $event->users->id)->first();
      if ($notificari && $notificari->comentariueveniment) {
         Log::debug(__METHOD__ . ' email notification activated for user ['.$notificari->user_id.']');

         $subject = $comment->users->alias . ' ti-a scris pe pagina evenimentului ' . $event->event;
         $url = route('events.show' , $event->id);
         $event = (new CommentEvent($comment, $notificari, $event->users->id, $subject, $url))->queueEvent();
      }
      else {
         Log::debug(__METHOD__ . ' email notification NOT activated for user ['.$comment->entity_id.']');
      }
   }
});


Event::listen('profile.comment', function($comment)
{
   Log::debug(__METHOD__ . ' ' . Event::firing() . ' comment from ['.$comment->owner.'] to ['.$comment->entity_id.']');

   $notificari = Notification::with('user')->where('user_id', '=', $comment->entity_id)->first();
   if ($notificari && $notificari->comentariuprofil) {
      Log::debug(__METHOD__ . ' email notification activated for user ['.$notificari->user_id.']');

      $commentId = $comment->id;
      $notificariId = $notificari->id;

      $subject = $comment->users->alias . ' ti-a lasat un comentariu pe pagina ta de profil';
      $url = href_profile($notificari->user);
      $event = (new CommentEvent($comment, $notificari, $comment->entity_id, $subject, $url))->queueEvent();
   }
   else {
      Log::debug(__METHOD__ . ' email notification NOT activated for user ['.$comment->entity_id.']');
   }
});



class CommentEvent {
   protected $comment;
   protected $notificari;
   protected $userId;
   protected $emailSubject;
   protected $url;
   protected $emailTemplate = 'profile.comment';
   protected $commentHash = '%s#comment-%d';

   public function __construct(Comment $comment, Notification $notificari, $userId, $subject, $url = null, $template = null) {
      $this->comment       = $comment;
      $this->notificari    = $notificari;
      $this->userId        = $userId;
      $this->emailSubject  = $subject;
      $this->url           = $url;
      $this->emailTemplate = $template ?: $this->emailTemplate;
   }

   public function getCommentUrl() {
      return sprintf($this->commentHash, $this->url, $this->comment->id);
   }

   public function getUrlHtml() {
      return '<a href="'. $this->getCommentUrl() .'">Vezi comentariu</a>';
   }

   public function queueEvent() {
      Log::debug(__METHOD__ . ' started');

      $commentId    = $this->comment->id;
      $notificariId = $this->notificari->id;
      $ownerId      = $this->userId;
      $subject      = $this->emailSubject;
      $url          = $this->getUrlHtml();
      $template     = $this->emailTemplate;

      Log::debug(__METHOD__ . ' got commentId ['.$commentId.'], $notificariId ['.$notificariId.'] ownerId ['.$ownerId.']');

      Queue::push(function($job) use ($commentId, $notificariId, $ownerId, $subject, $url, $template) {
         $notificari = Notification::find($notificariId);
         $comment    = Comment::find($commentId);
         $user       = User::find($ownerId);
         $data       = array('comment' => $comment, 'user' => $user, 'url' => $url);

         Mail::send($template, $data, function($message) use ($notificari, $comment, $subject, $template) {
            Log::debug(__METHOD__ . ' sending email notification to ['.$notificari->user->email.']');
            $message->to($notificari->user->email, $notificari->user->alias)
                    ->subject($subject);
         });

         $job->delete();
      });

      Log::debug(__METHOD__ . ' ended');
   }
}