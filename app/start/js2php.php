<?php
/**
* PHP Javascript Interface
*
*/

class vsjCalendar {

   public static $DAYS = array(
            'Luni' => 'Monday',
            'Marti' => 'Tuesday',
            'Miercuri' => 'Wednesday',
            'Joi' => 'Thursday',
            'Vineri' => 'Friday',
            'Sambata' => 'Saturday',
            'Duminica' => 'Sunday',
         );

   public static $MONTHS = array(
      'Ianuarie' => 'January',
      'Februarie' => 'February',
      'Martie' => 'March',
      'Aprilie' => 'April',
      'Mai' => 'May',
      'Iunie' => 'June',
      'Iulie' => 'July',
      'August' => 'August',
      'Septembrie' => 'September',
      'Octombrie' => 'October',
      'Noiembrie' => 'November',
      'Decembrie' => 'December',
   );

}

if ( ! function_exists('date2php'))
{
   function date2php($jsdate) {
      //global vsjCalendar::$DAYS, vsjCalendar::$MONTHS;
      $jsdate = str_replace(array_keys(vsjCalendar::$DAYS), vsjCalendar::$DAYS, $jsdate);
      $jsdate = str_replace(array_keys(vsjCalendar::$MONTHS), vsjCalendar::$MONTHS, $jsdate);
      return date('Y-m-d', strtotime($jsdate));
   }
}

if ( ! function_exists('dmy2js'))
{
   function dmy2js($jsdate) {
      //global vsjCalendar::$DAYS, vsjCalendar::$MONTHS;
      $jsdate = str_replace(array_keys(vsjCalendar::$DAYS), vsjCalendar::$DAYS, $jsdate);
      $jsdate = str_replace(array_keys(vsjCalendar::$MONTHS), vsjCalendar::$MONTHS, $jsdate);
      return date('d.m.Y', strtotime($jsdate));
   }
}

if ( ! function_exists('date2js'))
{
   function date2js($jsdate) {
      //global vsjCalendar::$DAYS, vsjCalendar::$MONTHS;
      $jsdate = date("l, j F Y", strtotime($jsdate));
      $jsdate = str_replace(vsjCalendar::$DAYS, array_keys(vsjCalendar::$DAYS), $jsdate);
      $jsdate = str_replace(vsjCalendar::$MONTHS, array_keys(vsjCalendar::$MONTHS), $jsdate);
      return $jsdate; // afiseaza Luni, 30 Mai 2005
   }
}

if ( ! function_exists('time2js'))
{
   function time2js($time) {
      return date('H:i', strtotime($time));
   }
}