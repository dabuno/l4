<?php

require_once('js2php.php');
require_once('vsj/blade.php');
require_once('vsj/validator.php');
require_once('vsj/html.php');
require_once('vsj/users.php');
require_once('vsj/functions.php');
require_once('vsj/CommentEventHandler.php');
require_once('vsj/ConvocareEventHandler.php');

/**
 * handling minimal audit
 */
App::after(function($request, $response)
{
   $audit = new Audit();
   $audit->url = $request->url();
   $audit->ip = $request->getClientIp();
   $audit->save();
   Log::debug(__METHOD__ . ' finished request for url [' . $request->url().'] for client ['.$request->getClientIp().']');
});

/*
Request::setTrustedProxies(array(
    '127.0.0.1' // IP address of your intermediary
));
*/



/*
App::error(function(ModelNotFoundException $e)
{
   echo 'Page not found!';
   return Response::make('Not Found', 404);
});

*/

/*
App::missing(function($exception)
{
   return "<pre>Error " . $exception;
   //return View::make('errors.missing');
});
*/


/*
App::missing(function($exception)
{
   return Response::make('Page not found - 404', '404');
   //return Response::view('themes/'.Config::get('wardrobe.theme').'/404', array(), 404);
});
*/




// constants
/*********************
 * Convocari statuses
 ********************/
define('CONVOCARE_SRC_STEARSA',     10);
define('CONVOCARE_SRC_ACCEPTATA',    5);
define('CONVOCARE_SRC_ACTIVA',       1);
define('CONVOCARE_SRC_ANULATA',      0);
define('CONVOCARE_SRC_REFUZATA',    -1);

define('CONVOCARE_DEST_STEARSA',    10);
define('CONVOCARE_DEST_ACCEPTATA',   5);
define('CONVOCARE_DEST_ACTIVA',      1);
define('CONVOCARE_DEST_ANULATA',     0);
define('CONVOCARE_DEST_REFUZATA',   -1);






/*********************
 * User alias prefs
 ********************/
define('USER_PREF_SHOW_FULLNAME', 1);
define('USER_PREF_SHOW_NAMETRIM', 2);
define('USER_PREF_SHOW_ALIAS', 3);

if (!function_exists('prin')) {
function prin($vars,$die=0,$caption='', $flush = true) {
   //return false;
   /*
   if ($_SERVER["REMOTE_ADDR"] != '10.21.2.20' && $_SERVER["REMOTE_ADDR"] != '10.21.8.20'  && $_SERVER["REMOTE_ADDR"] != '10.21.3.9' && $_SERVER["REMOTE_ADDR"] != '95.76.160.48' )
   return false;
   */
   echo '<div style="background:white;text-align:left">';
   if (@disable_prin == 1)
     return false;
   if ($die>=0) {
      if ( is_array($vars) || is_object($vars) ) {
         echo (is_string($die)?$die:'').$caption.'<b style="color:red">:: '.(is_array($vars)?'array':'object').' ::</b><pre><span style="color:blue">';
         print_r($vars);
         echo '</pre>';
      } else {
         echo (is_string($die)?$die:'').$caption.'<b style="color:red">:: '.('variable').' ::</b><pre><span style="color:blue">';
         print_r($vars);
         echo '</pre>';
      }
   }

   if ($die<0) {
      if ( is_array($vars) || is_object($vars) ) {
         echo (is_string($die)?$die:'').$caption.'<b style="color:red">:: '.(is_array($vars)?'array':'object').' ::</b><pre><span style="color:blue">';
         var_dump($vars);
         echo '</pre>';
      } else {
         echo (is_string($die)?$die:'').$caption.'<b style="color:red">:: '.('variable').' ::</b><pre><span style="color:blue">';
         var_dump($vars);
         echo '</pre>';
      }
   }

   if ($flush)
   {
      echo str_pad(' ', 1024*8);
      flush();
      //sleep($flush);
   }

   if ($die == 1 || $die=='-1')
      die("<b>:: died ::</b>");
   echo '</div>';
}
}
/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',

));


/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

//Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a rotating log file setup which creates a new file each day.
|
*/

$logFile = 'log-'.php_sapi_name().'.txt';

Log::useDailyFiles(storage_path().'/logs/'.$logFile);

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
	Log::error($exception);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/


/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
        return Response::make("Be right back!", 503);
});

require __DIR__.'/../filters.php';