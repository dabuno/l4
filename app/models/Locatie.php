<?php

class Locatie extends Eloquent {
   protected $guarded = array();

   public static $rules = array();

   /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
   protected $visible = array('id', 'name', 'created_at');

   protected $fillable = array('name', 'adresa', 'nume_contact', 'puncte_reper', 'descriere', 'telefon', 'oras_id',
      'latitudine', 'longitudine', 'email', 'website', 'locatie_type');

   protected $table = 'locatii';

   public function scopeActive($query) {

      if (is_admin()) {
         Log::info('ignoring active query when admin');
         return $query;
      }

      return $query->where('active', '=', '1')->where('approved', '=', '1');
   }

   public function scopeApproved($q) {
      return $q->where('approved', '=', 1);
   }

   public function scopeLast($q) {
      return $q->where('active', '=', '1')->approved()->orderBy('created_at', 'DESC');
   }

   public function scopeOwned($query) {
      if (!is_admin() && is_logged()) {
         $query->where('owner_id', '=', Auth::user()->id);
      }

      return $query;
   }


   /**
    * validate used for register/create user
    * @param  array $input user form submission
    * @return Validation object        laravel
    */
   public static function validate($input = false) {

      if (!$input) {
         $input = Input::all();
      }

      $rules = array(
         'name'         => 'Required|Min:3|Max:250|noxss_tags',
         'adresa'       => 'Required|Min:3|Max:500|noxss_tags',
         'nume_contact' => 'Max:100|noxss_tags',
         'puncte_reper' => 'Max:500|noxss_tags',
         'descriere'    => 'Max:2000|noxss_tags',
         'program'      => 'Max:2000|noxss_tags',
         'locatie_type' => 'exists:locatii_types,id',
         'telefon'      => 'noxss_tags',
         'latitudine'   => 'numeric',
         'longitudine'  => 'numeric',
         'sport'        => 'Required|multipleint',
         'oras_id'      => 'Required|Integer|Min:1',
         'email'        => 'email',
         'delimg'       => 'multipleint',
         'copyright'    => 'url',
         'website'      => 'url',
         );

      if (!is_logged()) {
         $rules['recaptcha_response_field'] = 'Required|testrecapthca';
      }

      return Validator::make($input, $rules);
      # validation code
   }


   //relationships

   public function sports()
   {
    return $this->belongsToMany('Sport', 'locatii_sports', 'locatie_id','sport_id');
 }

 public function arene() {
   return $this->hasOne('UserEvent');
}

public function orase() {
   return $this->belongsTo('Oras', 'oras_id');
}

public function locatiitype() {
   return $this->belongsTo('LocatieType', 'locatie_type');
}



}