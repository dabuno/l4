<?php

class Propune extends Eloquent {
   protected $guarded = array();

   public static $rules = array();

   protected $fillable = array('email', 'text');

   protected $table = 'propuneri';

   protected $primaryKey = "id_prop";
}