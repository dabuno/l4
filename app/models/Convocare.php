<?php

class Convocare extends Eloquent {
   protected $guarded = array();

   public static $rules = array();

   protected $visible = array('id', 'src_id', 'dest_id', 'user_event_id');

   protected $table = 'convocari';


   public function scopeIminvolved($q) {
      return $q->orWhere(function($q) {
         return $q->orWhere('src_id', '=', is_logged())
            ->orWhere('dest_id', '=', is_logged());
      });
   }

   public function scopeIamsrc($q) {
      return $q->where('src_id', '=', is_logged());
   }

   public function scopeIamdest($q) {
      return $q->where('dest_id', '=', is_logged());
   }

   public function scopeVisible($q) {
      return $q->where('src_status', '!=', CONVOCARE_SRC_STEARSA)->where('src_id', '=', is_logged());
   }

   public function scopeSrcValid($q) {
      return $q->orWhere(function($q) {
         $q->where('src_id', '=', is_logged())
            ->where('src_status', '!=', CONVOCARE_SRC_STEARSA)
            ->where('src_id', '=', is_logged());
      });
   }
   public function scopeDestValid($q) {
      return $q->orWhere(function($q) {
         $q->where('dest_id', '=', is_logged())
            ->where('dest_status', '!=', CONVOCARE_DEST_STEARSA)
            //->where('src_status', '!=', CONVOCARE_SRC_STEARSA)
            ->where('dest_id', '=', is_logged());
      });
   }



   public function events() {
      return $this->belongsTo('UserEvent', 'user_event_id')->orderBy('data', 'DESC');
   }

   public function srcids() {
      return $this->belongsTo('User', 'src_id');
   }

   public function destids() {
      return $this->belongsTo('User', 'dest_id');
   }

}