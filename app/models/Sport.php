<?php

class Sport extends Eloquent {
   protected $guarded = array();

   protected $hidden = array('active', 'ord');

   public static $rules = array();

   public function scopeActive($query) {
      $query->where('sports.active', '=', 1);
   }


   public static function validate($rules = false) {

      if (!$rules) {
         $rules = array(
            'name' => 'Required|Min:3|Max:80|alpha_spaces|unique:sports,name'
         );
      }

      return Validator::make(Input::all(), $rules);
   }

   /**
    * locatii has a many to many rel. with sports.
    *
    * @return Collection
    */
   public function locatii()
   {
      return $this->belongsToMany('Locatie', 'locatii_sports', 'sport_id', 'locatie_id');
   }

   public function subcategorii()
   {
      return $this->belongsToMany('Subcategorie', 'sports_subcategorii', 'id_sport', 'id_sub');
      //return $this->hasMany('Subcategorie');
   }

   public function users()
   {
      return $this->belongsToMany('User', 'users_sports', 'id_sport',  'id_user');
   }

   public function events() {
      return $this->hasOne('UserEvent');
   }
}