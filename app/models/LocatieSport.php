<?php

class LocatiiSports extends Eloquent {
   protected $guarded = array();

   public static $rules = array();

   protected $table = 'locatii_sports';

   public $timestamps = false;
}