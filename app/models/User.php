<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;


class User extends Eloquent implements UserInterface, RemindableInterface {


   protected $visible = array('id', 'alias', 'created_at');

   /**
    * The database table used by the model.
    *
    * @var string
    */
   protected $table = 'users';

   /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
   protected $hidden = array('password', 'hashed_password');

   /**
    * Get the unique identifier for the user.
    *
    * @return mixed
    */
   public function getAuthIdentifier()
   {
      return $this->getKey();
   }

   /**
    * Get the password for the user.
    *
    * @return string
    */
   public function getAuthPassword()
   {
      return $this->password;
   }

   /**
    * Get the e-mail address where password reminders are sent.
    *
    * @return string
    */
   public function getReminderEmail()
   {
      return $this->email;
   }

   public function scopeActive($q) {
      return $q->where('active', '=', 1);
   }


   public function scopeLast($q) {
      return $q->active()->orderBy('created_at', 'DESC');
   }


   public function userEvents()
   {
      return $this->hasMany('UserEvent', 'user_id');
   }

   public function orase()
   {
      return $this->belongsToMany('Oras', 'users_orase', 'id_user', 'id_oras');
   }

   public function sports()
   {
      return $this->belongsToMany('Sport', 'users_sports', 'id_user', 'id_sport');
   }

   public function comments()
   {
      return $this->hasMany('Comment', 'owner_id');
   }

   public function uploads() {
      return $this->hasMany('Upload', 'uploader_id');
   }

   /**
    * validate used for register/create user
    * @param  array $input user form submission
    * @return Validation object        laravel
    */
   public static function validate($input, $rules = array()) {
      if (!count($rules)) {
         $rules = array(
             'first_name' => 'Required|Min:3|Max:80|alpha_dash',
             'last_name'  => 'Required|Min:1|Max:80|alpha_dash',
             'email'      => 'Required|Between:3,64|Email',
             'gen'        => 'required|numeric|min:1|max:2',
             'password'   => 'Required|Min:5',
             'sport'      => 'Required|Integer|Min:1',
             'recaptcha_response_field' => 'Required|testrecapthca'
         );
      }

      return Validator::make($input, $rules);
      # validation code
   }

   public static function pwdvalidate($rules = false) {
      if (!$rules) {
         $rules = array(
            'parola' => 'required',
            'newpassword' => 'required|confirmed'
         );
      }

      return Validator::make(Input::all(), $rules);
   }

   public function getRememberToken()
   {
       return $this->remember_token;
   }

   public function setRememberToken($value)
   {
       $this->remember_token = $value;
   }

   public function getRememberTokenName()
   {
       return 'remember_token';
   }
}
