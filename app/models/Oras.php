<?php

class Oras extends Eloquent {
   protected $guarded = array();

   public static $rules = array();

   protected $table = 'orase';

   protected $primaryKey = "oras_id";

   public function orase()
   {
      return $this->belongsToMany('User', 'users_orase', 'id_user', 'id_oras');
   }

   /*
   public function scopeActive($query) {
      $query->where('orase.active', '=', 1);
   }
   */
}