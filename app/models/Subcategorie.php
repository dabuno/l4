<?php

class Subcategorie extends Eloquent {
   public $timestamps;

   protected $guarded = array();

   public static $rules = array();

   protected $table = 'subcategorii';

   public function scopeActive($query) {
      $query->where($this->table . '.active', '=', 1);
   }

   public function subcategorii()
   {
      return $this->belongsToMany('Sport', 'sports_subcategorii', 'id_sport', 'id_sub');
   }

}