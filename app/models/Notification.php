<?php

class Notification extends Eloquent {
   protected $guarded = array();

   public static $rules = array();

   protected $table = 'notifications';

   protected $visible = array('id', 'user_id');

   protected $fillable = array('suntconvocat', 'evenimentnou', 'comentariuprofil','comentariueveniment', 'competitienoua','functionalitatenoua');

   public function user() {
      return $this->belongsTo('User', 'user_id');
   }
}
