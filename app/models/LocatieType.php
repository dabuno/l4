<?php

class LocatieType extends Eloquent {
   protected $guarded = array();

   public static $rules = array();

   /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
   protected $visible = array('id', 'tip');

   protected $fillable = array('tip');

   protected $table = 'locatii_types';

   /**
    * validate used for register/create user
    * @param  array $input user form submission
    * @return Validation object        laravel
    */
   public static function validate($input = false) {

      if (!$input) {
         $input = Input::all();
      }

      $rules = array(
         'tip'          => 'Required|Min:3|Max:100|noxss_tags',
      );

      return Validator::make($input, $rules);
      # validation code
   }


   //relationships

   public function locatii() {
      return $this->belongsTo('Locatie');
   }
}