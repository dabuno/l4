<?php

class Upload extends Eloquent {
   protected $guarded = array();

   public static $rules = array();

   protected $visible = array('id', 'filename');

   public function users()
   { 
      return $this->belongsTo('User', 'id');
   }
   
}