<?php

class Comment extends Eloquent {
	protected $guarded = array();

	public static $rules = array();


   public function scopeURL($q, $url) {
      $q->where('hashurl', '=', md5($url))->take(Config::get('sitesettings.noOfComments', 10));
   }

   public function scopeCounting($q, $url) {
      $q->where('hashurl', '=', md5($url));
   }


   public function users() {
      return $this->belongsTo('User', 'owner');
   }

   // public function touser() {
   //    return $this->belongsTo('User', 'entity_id');
   // }


   public static function counter($url) {
      return Comment::Counting($url)->count();
   }

   public static function getComments($url) {
      $comments = Comment::URL($url)->with('users')->get()->all();

      $arr = [];
      $indexed = [];
      foreach ($comments as $comment) {
         if (!$comment->parent) {
            //its a root comment
            $arr[$comment->id] = [];
         }
         else {
            if (!isset($arr[$comment->parent])) {
               $arr[$comment->parent] = [];
               //$arr[$comment->parent][$comment->id] = $comment;
            }
            $arr[$comment->parent][$comment->id] = $comment->id;


            if (!isset($arr[$comment->id])) {
               $arr[$comment->id] = [$comment->parent];
            }
         }
         $indexed[$comment->id] = $comment;
      }

      return ['comments' => $comments, 'tree' => $arr, 'indexed' => $indexed];
   }


   public static function Validate($input = null, $rules = null) {

      if (!$input) {
         $input = Input::all();
      }

      if (!$rules) {

         //todo: check if logged, show captcha
         $rules = [
            'comment' => 'required|max:500',
            'reply' => 'numeric|min:0',
         ];

         if (!is_logged()) {
            $rules['recaptcha_response_field'] = 'required|testrecapthca';
         }
      }

      return Validator::make($input, $rules);
   }
}
