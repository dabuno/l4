<?php

class SportSubcategorie extends Eloquent {
   public $timestamps;
   protected $guarded = array();

   public static $rules = array();

   protected $table = 'sports_subcategorii';
}