<?php

class UserEvent extends Eloquent {
   protected $fillable = array('event', 'sport_id', 'data', 'ora', 'oras_id', 'arena_id', 'pret', 'locuri');

   protected $visible = array('id', 'event', 'data', 'ora', 'created_at');

   public static $rules = array();

   protected $table = 'users_events';

   //scopes

   public function scopeActive($query) {
      $query->where( $this->table . '.active', '=', 1);
   }

   public function scopeOwned($query) {
      $query->where( $this->table . '.user_id', '=', Auth::user()->id);
   }

   public function scopeFindBy($query, $id, $field = 'user_id') {
      $query->where($this->table . '.' .$field, '=', $id);
   }


   public function scopeLast($q) {
      $q->active()->orderBy('created_at', 'DESC');
   }

   /**
    * validate used for register/create user
    * @param  array $input user form submission
    * @return Validation object        laravel
    */
   public static function validate($input = false) {

      if (!$input) {
         $input = Input::all();
      }

      /*
      $this->form_validation->set_rules('event_name', 'Numele evenimentului', 'trim|required|xss_clean');
      $this->form_validation->set_rules('sport', 'Sport', 'trim|required|xss_clean|is_natural_no_zero');
      $this->form_validation->set_rules('data', 'Data', 'trim|required|xss_clean');
      $this->form_validation->set_rules('ora', 'Ora', 'trim|required|xss_clean');
      $this->form_validation->set_rules('localitate', 'Localitatea', 'trim|required|xss_clean|is_natural_no_zero');
      $this->form_validation->set_rules('arena', 'Arena', 'trim|required|xss_clean|is_natural_no_zero');
      postponed to future versions
      $this->form_validation->set_rules('tip_eveniment', 'Eveniment de tip', 'trim|required|xss_clean');
      $this->form_validation->set_rules('pret', 'Pretul', 'trim|required|xss_clean');
      $this->form_validation->set_rules('locuri', 'Nr. de locuri disponibile', 'trim|required|xss_clean|is_natural_no_zero');
      */

      //@todo XSS?

      $rules = array(
          'event' => 'Required|Min:3|Max:50|noxss_tags|alpha_spaces',
          'sport_id'      => 'Required|Integer|Min:1',
          'data'       => 'Required|Min:1|noxss_tags',
          'ora'        => 'Required|Min:5|Max:5|noxss_tags',
          'oras_id' => 'Required|Integer|Min:1',
          'arena_id'      => 'Required|Min:1|noxss_tags',
          'pret'       => 'Numeric',
          'locuri'     => 'Required|Numeric|Min:1',
      );

      return Validator::make($input, $rules);
      # validation code
   }



   //relationships

   public function users()
   {
      return $this->belongsTo('User', 'user_id');
   }

   public function sports()
   {
      return $this->belongsTo('Sport', 'sport_id');
   }

   public function orase() {
      return $this->belongsTo('Oras', 'oras_id');
   }

   public function arene() {
      return $this->belongsTo('Locatie', 'arena_id');
   }

   public function convocari() {
      return $this->hasMany('Convocare', 'user_event_id');
   }
}