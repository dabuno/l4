<?php

return array(
   'sitename' => 'vreausajoc.ro',
   'breadsitename' => 'vreau sa joc',
   'siteowner' => 'bgd',

   'perpage' => 10,

   'noOfComments' => 100,

   'lastXevents' => 10,
   'lastXusers' => 10,
   'lastXlocatii' => 10,



   "uimgtb" => "/uploads/uimage/tb/",
   "uimgmtb" => "/uploads/uimage/mtb/",
   "uimg" => "/uploads/uimage/",
   #resize image max 800x600
   "uimgMW" => 800,
   "uimgMH" => 600,
   #thumbnail 150x150
   "uimgtbMW" => 100,
   "uimgtbMH" => 100,
   #thumbnail 200x150
   "uimgmtbMW" => 200,
   "uimgmtbMH" => 150,

   "imglocatiitb" => "/uploads/locatii/tb/",
   "imglocatii" => "/uploads/locatii/",
   #resize image max 800x600
   "locatieW" => 800,
   "locatieH" => 600,
   #thumbnail 200x150
   "locatietbW" => 200,
   "locatietbH" => 150,


   "google_maps_url" => "http://maps.googleapis.com/maps/api/js?key=AIzaSyC9wnxLIlLCebQBUWqx24J6kQyPm32Cz6o&sensor=false&language=ro&region=ro&v=3",
   //"google_maps_url" => "",

   "MGen" => 'Masculin',
   "FGen" => 'Feminin',
   "userGen" => array('0' => 'Alege', '1' => 'Masculin', '2' => 'Feminin'),

   "enotif" => array(
      'arene' => 'bogdan.durla@gmail.com',
      'users' => 'bogdan.durla@gmail.com',
      'propunere' => 'bogdan.durla@gmail.com',
   ),

   "admin" => 'bogdan.durla@gmail.com',


   'ret' => ['error' => 1, 'msg' => 'O eroare a aparut. Va rugam incercati mai tarziu.'],


);
