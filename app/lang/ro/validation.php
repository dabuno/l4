<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| such as the size rules. Feel free to tweak each of these messages.
	|
	*/

	"accepted"         => "Campul :attribute trebuie sa fie acceptat.",
	"active_url"       => "Campul :attribute trebuie sa fie un URL valid.",
	"after"            => "Campul :attribute must be a date after :date.",
	"alpha"            => "Campul :attribute poate sa contina doar litere.",
   "alpha_dash"       => "Campul :attribute poate sa contina doar litere, cifre, si -.",
	"alpha_spaces"       => "Campul :attribute poate sa contina doar litere, cifre, spatii si -.",
	"alpha_num"        => "Campul :attribute poate sa contina doar litere si cifre.",
	"before"           => "Campul :attribute trebuie sa fie o data inainte de :date.",
	"between"          => array(
		"numeric" => "Campul :attribute trebuie sa fie intre :min - :max.",
		"file"    => "Campul :attribute trebuie sa fie intre :min - :max kilobytes.",
		"string"  => "Campul :attribute trebuie sa aiba intre :min - :max caractere.",
	),
	"confirmed"        => "Campul confirmare :attribute nu e identic cu campul :attribute.",
	"date"             => "Campul :attribute nu e o data valida.",
	"date_format"      => "Campul :attribute nu are formatul :format.",
	"different"        => "Campul :attribute si :other trebuie sa fie diferite.",
	"digits"           => "Campul :attribute trebuie sa contina :digits cifre.",
	"digits_between"   => "Campul :attribute trebuie sa fie intre :min si :max cifre.",
	"email"            => "Campul :attribute are formatul invalid.",
	"exists"           => "Campul selected :attribute is invalid.",
	"image"            => "Campul :attribute trebuie sa fie o imagine.",
	"in"               => "Campul selectat :attribute este invalid.",
	"integer"          => "Campul :attribute trebuie sa fie un numar intreg.",
	"ip"               => "Campul :attribute must be a valid IP address.",
	"max"              => array(
		"numeric" => "Campul :attribute trebuie sa fie maxim :max.",
		"file"    => "Campul :attribute trebuie sa aiba maxim :max kilobytes.",
		"string"  => "Campul :attribute trebuie sa aiba maxim :max caractere.",
	),
	"mimes"            => "Campul :attribute trebuie sa fie o fila de tipul: :values.",
	"min"              => array(
		"numeric" => "Campul :attribute trebuie sa fie minim :min.",
		"file"    => "Campul :attribute trebuie sa aiba minim :min kilobytes.",
		"string"  => "Campul :attribute trebuie sa aiba minim :min caractere.",
	),
	"not_in"           => "Campul selected :attribute este invalid.",
	"numeric"          => "Campul :attribute trebuie sa fie un numar real.",
	"regex"            => "Campul :attribute are formatul invalid.",
	"required"         => "Campul :attribute este obligatoriu.",
	"required_with"    => "Campul :attribute field is required when :values is present.",
	"required_if"    => "Campul :attribute este necesar cand :value este selectat.",
	"required_without" => "Campul :attribute field is required when :values is not present.",
	"same"             => "Casil :attribute and :other must match.",
	"size"             => array(
		"numeric" => "Campul :attribute must be :size.",
		"file"    => "Campul :attribute must be :size kilobytes.",
		"string"  => "Campul :attribute must be :size caractere.",
	),
	"unique"           => "Campul :attribute este deja utilizat.",
	"url"              => "Campul :attribute are formatul invalid.",

   "testrecapthca"    => "Codul de verificare a fost invalid.",
	"multipleint"      => "Selectia multipla pentru :attribute este invalida.",
	"noxss_tags"      => "Campul :attribute nu poate sa contina tag-uri html: a, div,...",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
      "first_name" => array(
         'required' => "Campul :attribute este obligatoriu.",
      ),
      "last_name" => array(
         'required' => "Campul :attribute este obligatoriu.",
      ),
      "email_name" => array(
         'required' => "Campul :attribute este obligatoriu.",
      ),
      "sport_id" => array(
         'min' => "Campul sport trebuie sa fie selectat din lista de sporturi",
      ),
      "oras_id" => array(
         'min' => "Campul oras trebuie sa fie selectat din lista de orase",
      ),
      "arena_id" => array(
         'noxss_tags' => "Campul arena trebuie sa fie selectat din lista de arene",
      ),
      "gen" => array(
         'min' => "Campul gen trebuie sa fie ". Config::get('sitesettings.MGen') ." sau ". Config::get('sitesettings.FGen'),
         'max' => "Campul gen trebuie sa fie ". Config::get('sitesettings.MGen') ." sau ". Config::get('sitesettings.FGen'),
      ),
      "newpassword" => array(
         'required' => "Campul :attribute este obligatoriu.",
      ),

   ),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(
         "first_name" => 'prenume',
         "last_name" => 'nume',
         "email" => 'adresa de email',
         "password" => 'parola',
         "recaptcha_response_field" => 'Cod verficare',
         "event" => "nume eveniment",
         "north" => "latitudine",
         "east" => "longitudine",
         "newpassword" => "parola noua",
         "confirmed" => "confirmare",
      ),

);
