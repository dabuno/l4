<?php
class ConvocariController extends BaseController {


   public function __construct() {
      # code...
      //$this->beforeFilter('auth');
      $this->beforeFilter('auth');
   }



   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
   public function getIndex()
   {

      $convocari = Convocare::with('events', 'srcids', 'destids')
         ->srcValid()
         ->destValid()
         ->paginate(Config::get('sitesettings.perpage'));

      return View::make('convocari.convocari', compact('convocari'));
   }

   public function getAccepta($id) {
      DB::transaction(function() use($id) {

         $convocare = Convocare::where('id', '=', $id)->iamdest()->first();
         if (!isset($convocare->id) || !$convocare->id) {
            Log::error('[ '. __METHOD__ . '] invalid id ['.$id.'] supplied');
            return Redirect::to('convocari');
         }

         if ($convocare->events->locuri == 0) {
            Log::info('[ '. __METHOD__ . '] no locuri left for event ['.$id.']');
            Session::flash('flash_error', 'Nu mai sunt locuri disponibile pentru evenimentul '.$convocare->events->event);
            return Redirect::back();
         }

         $convocare->dest_status = CONVOCARE_DEST_ACCEPTATA;
         $convocare->src_status = CONVOCARE_SRC_ACCEPTATA;
         $convocare->save();

         $convocare->events->locuri = $convocare->events->locuri - 1;
         $convocare->push();

         $event = Event::fire('convocare.acceptata', array($convocare));

         Session::flash('message', 'Convocarea pentru evenimentul '.$convocare->events->event.' a fost acceptata.');
      });

      return Redirect::back();
   }


   public function getDeletes($id) {

      DB::transaction(function() use($id) {

         $convocare = Convocare::where('id', '=', $id)->iamsrc()->first();
         if (!isset($convocare->id) || !$convocare->id) {
            Log::error('[ '. __METHOD__ . '] invalid id ['.$id.'] supplied');
            return Redirect::to('convocari');
         }

         $convocare->src_status = CONVOCARE_SRC_STEARSA;
         //$convocare->dest_status = CONVOCARE_DEST_ANULATA;
         $convocare->save();

      });

      return Redirect::back();
   }

   /**
    * cancel from source convocarea
    * @param  int $id convocare id
    * @return redirect     redirect to convocare listing
    */
   public function getCancels($id) {
      DB::transaction(function() use($id) {

         $convocare = Convocare::where('id', '=', $id)->iamsrc()->visible()->first();
         if (!isset($convocare->id) || !$convocare->id) {
            Log::error('[ '. __METHOD__ . '] invalid id ['.$id.'] supplied');
            return Redirect::to('convocari');
         }

         $convocare->src_status = CONVOCARE_SRC_ANULATA;
         $convocare->events->locuri = $convocare->events->locuri + 1;
         $convocare->dest_status = CONVOCARE_DEST_ANULATA;
         $convocare->save();
         $convocare->events->push();

         $event = Event::fire('convocare.anulata', array($convocare));
      });

      return Redirect::back();
   }


   /**
    * deletes from convocari where logged user is a destination in convocare
    * @param  int $id convocare id
    * @return retdirect
    */
   public function getDeleted($id) {

      DB::transaction(function() use($id) {

         $convocare = Convocare::where('id', '=', $id)->iamdest()->first();
         if (!isset($convocare->id) || !$convocare->id) {
            Log::error('[ '. __METHOD__ . '] invalid id ['.$id.'] supplied');
            return Redirect::to('convocari');
         }

         $convocare->dest_status = CONVOCARE_DEST_STEARSA;
         $convocare->save();

      });

      return Redirect::back();
   }


   public function getRefuza($id) {
      DB::transaction(function() use($id) {

         $convocare = Convocare::where('id', '=', $id)->iamdest()->first();
         if (!isset($convocare->id) || !$convocare->id) {
            Log::error('[ '. __METHOD__ . '] invalid id ['.$id.'] supplied');
            return Redirect::to('convocari');
         }

         $convocare->dest_status = CONVOCARE_DEST_REFUZATA;
         $convocare->src_status = CONVOCARE_SRC_REFUZATA;
         $convocare->save();

         $event = Event::fire('convocare.refuzata', array($convocare));

      });

      return Redirect::back();
   }


   /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
   public function create()
   {
       //
   }

   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
   public function store()
   {
       //
   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
   public function show($id)
   {
       //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
   public function edit($id)
   {
       //
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
   public function update($id)
   {
       //
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
   public function destroy($id)
   {
       //
   }

   /**
    * ajax adds one or more convocari
    * @return json with the convocare status
    */
   public function postAdd() {

     $ret = array(
        'error' => 1,
        'content' => 'Operatia nu a reusit.',
     );

     $rules = array(
        'uid' => 'required|numeric|min:1|exists:users,id',
        'events' => 'required|multipleint'
     );

     $v = Validator::make(Input::all(), $rules);
     if ($v->fails()) {
        Log::error('['. __METHOD__ .'] form validation failed for [' . Auth::user()->id . ']');
        return Response::json($ret);
     }


     $events = Input::get('events');

      $ret['error'] = 0;

      foreach ($events as $event) {
         $eventdb = UserEvent::find($event);
         if (!$eventdb || !$eventdb->user_id || $eventdb->user_id != Auth::user()->id ) {
            Log::error('['. __METHOD__ .'] event id ['.$event.'] is not owned by uid [' . Auth::user()->id . ']');
            return Response::json($ret);
         }

         //for at least 1 convocare, no places left.
         if (!$eventdb->locuri) {
            $ret['error'] = 1;
            $ret['msg'][$eventdb->id] = 'Locurile s-au completat pentru ' . $eventdb->event . '.';
            continue;
         }

         $convocare = new Convocare;
         $convocare->user_event_id = $eventdb->id;
         $convocare->dest_id = Input::get('uid');
         $convocare->src_id = Auth::user()->id;

         $convocare->src_status = CONVOCARE_SRC_ACTIVA;
         $convocare->dest_status = CONVOCARE_DEST_ACTIVA;

         if (!$convocare->save()) {
            Log::error(__METHOD__ . ' failed to save convocare ['.$this.']');
         }

         $event = Event::fire('convocare.adaugata', array($convocare));

         if (!is_array($ret['content'])) {
            $ret['content'] = [];
         }

         $ret['content'][] = $convocare->id;
         $ret['msg'][$eventdb->id] = 'Convocare a fost trimisa pentru ' . $eventdb->event . '.';
     }

     if (!count($ret['content'])) {
        $ret['error'] = 1;
        $ret['content'] = 'Nu ai nici un eveniment configurat.';
     }

     return Response::json(array("content" => $ret, 'error' => $ret['error']));
   }

   public function getAdd() {
      //var_dump(Input::all());
   }

   }