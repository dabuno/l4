<?php

class LocatiiController extends BaseController {

   public function __construct() {
      $this->beforeFilter('auth',
         array('except' =>
            array(
               'show',
               'index',
               'create',
               'store'
               )
            )
         );

   }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      $locatii = Locatie::active()->with('orase')->paginate(Config::get('sitesettings.perpage'));

      /*
       * for all locatii get img ids from uplods
       */
      $idImages = array();
      foreach ($locatii as $locatie) {
         $locatie->images = json_decode(isset($locatie->images) ? $locatie->images : null );
         if ($locatie->images) {
            list($first) = $locatie->images;
            $idImages[$locatie->id] = $first;
         }
      }

      $locatiiImg = array();
      if (count($idImages)) {

         $imgIdflipped = array_flip($idImages);


         $locatiiImg = Upload::whereIn('id', $idImages)->get();

         $imgIdObj = array();
         foreach ($locatiiImg as $img) {
            $imgIdObj[$imgIdflipped[$img->id]] = $img;
         }
      }

      //$imgIdObj = Locatie::getFirstImg($locatii);

      $pageTitle = 'Lista locatii sportive';
      $pageDescription = 'Locatii, terenuri si arene pentru sport';
      return View::make('locatii.locatii', compact('locatii'))
      ->with(compact('imgIdObj'))
      ->with(compact('pageTitle'))
      ->with(compact('pageDescription'));
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
      $locatie = new Locatie;
      $orase = array('0' => 'Alege') + Oras::lists('oras', 'oras_id');
      $sports = Sport::active()->lists('name', 'id');

      $tipLocatii = LocatieType::lists('tip', 'id');

      $captcha = null;
      if (!is_logged()) {
         $captcha = new Services\Validators\privateReCaptcha;
      }

      $pageTitle = 'Creare locatie';
      return View::make('locatii.addlocatie')
      ->with(compact('locatie'))
      ->with(compact('sports'))
      ->with(compact('orase'))
      ->with(compact('captcha'))
      ->with(compact('tipLocatii'))
      ->with(compact('pageTitle'));
   }

   /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
   public function store()
   {
      $v = Locatie::validate();
      if ($v->fails()) {
         return Redirect::back()->withInput()->withErrors($v);
      }

      $locatie = new Locatie;

      $locatie->fill(Input::all());

      if (is_logged()) {
       $locatie->added_by_user_id = Auth::user()->id;
       $locatie->owner_id = Auth::user()->id;
      }
      else {
         //try find email.
         $user = User::where('email', '=', Input::get('email'))->first();
         if ($user && $user->id) {
            $locatie->added_by_user_id = $user->id;
         }
      }


      $locatie->images = $this->handleImages($locatie);

         //approving it by default
      if (is_admin()) {
         $locatie->approved = true;
      }

      $id = $locatie->save();


      $locatie->sports()->sync(Input::get('sport'));

      Log::debug('['.__METHOD__.'] created locatie $id ['.$locatie->id.'] by uid ' . is_logged());

      if (is_admin()) {
         Session::flash('message', 'Locatia a fost creata cu success.');
      }
      else {
         Session::flash('message', 'Locatia a fost creata cu success. Un moderator va aproba locatia in cel mai scurt timp.');
      }


      $data = $locatie->toArray() + ['url' => full_a_locatie($locatie)];

      \Services\Notifier::admin_email('O noua locatie a fost adaugata', $data) ;


      /*
         $emailArr = $locatie->toArray();
      $emailArr['adresa'] = $locatie->adresa;
      $emailArr['oras'] = $locatie->orase->oras;
      $emailArr['sport'] = $locatie->sports()->first()->name;
      $emailArr['added_by_user_id'] = $locatie->added_by_user_id;
      $emailArr['alias'] = Auth::user()->alias;

      $data = array('emailArr' => $emailArr);

      Mail::send('emails/locatii/newlocatie', $data, function($message) use($emailArr)
      {
         $message->to(Config::get('sitesettings.enotif.areneadmin', 'vreausajoc.ro@gmail.com'))
            ->subject('vreausajoc - o noua admin locatie a fost adaugata');
      });

      */


   return Redirect::route('locatii.show', $locatie->id);
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

      $locatie = Locatie::where('id', '=', $id)->with('sports', 'orase', 'locatiitype')->first();

      if (!isset($locatie->id) || !$locatie->id) {
         Log::error('[ '. __METHOD__ . '] invalid id ['.$id.'] supplied for edit by uid ['.Auth::user()->id.']');
         return Redirect::to('locatii');
      }

      $locatie->images = json_decode(isset($locatie->images) ? $locatie->images : null );
      if (count($locatie->images)) {
         $locatie->images = Upload::whereIn('id', $locatie->images)->get();
      }

      $strSports = '';
      foreach ($locatie->sports as $sport) {
         $strSports .= $sport->name . ', ';
      }
      $strSports = substr($strSports, 0, strrpos($strSports, ','));


      //Comments
      $noComments = Comment::counter('locatii/' . $id);
      $comments = Comment::getComments('locatii/' . $id);
      if (!is_logged()) {
         $captcha = new Services\Validators\privateReCaptcha;
      }



      $pageTitle = 'Locatie ' . $locatie->name;
      $pageDescription = 'Locatia '. $locatie->name . ' unde vreau sa joc: ' . $strSports;
      return View::make('locatii.viewlocatie')
      ->with(compact('locatie'))

      ->with(compact('pageTitle'))
      ->with(compact('pageDescription'))


      ->with(compact('captcha'))
      ->with(compact('noComments'))
      ->with(compact('comments'));



   }


   private function sportString(&$locatie) {
      $strSports = '';
      foreach ($locatie->sports as $sport) {
         $strSports .= $sport->name . ', ';
      }
      $strSports = substr($strSports, 0, strrpos($strSports, ','));

      return $strSports;
   }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $locatie = Locatie::where('id', '=', $id)->owned()->first();
      if (!isset($locatie->id) || !$locatie->id) {
         Log::error('[ '. __METHOD__ . '] invalid id ['.$id.'] supplied for edit by uid ['.Auth::user()->id.']');
         return Redirect::to('locatii');
      }

      $locatie->images = json_decode(isset($locatie->images) ? $locatie->images : null );
      if (count($locatie->images)) {
         $locatie->images = Upload::whereIn('id', $locatie->images)->get();
      }

      $orase = array('0' => 'Alege') + Oras::lists('oras', 'oras_id');
      $sports = Sport::active()->lists('name', 'id');

      $tipLocatii = LocatieType::lists('tip', 'id');


      $pageTitle = 'Editare locatie ' . $locatie->name;
      return View::make('locatii.addlocatie')
      ->with(compact('locatie'))
      ->with(compact('sports'))
      ->with(compact('tipLocatii'))
      ->with(compact('orase'))
      ->with(compact('pageTitle'));

   }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
      $locatie = Locatie::where('id', '=', $id)->owned()->first();
      if (!isset($locatie->id) || !$locatie->id) {
         Log::error('[ '. __METHOD__ . '] invalid id ['.$id.'] supplied for edit by uid ['.Auth::user()->id.']');
         return Redirect::to('locatii');
      }


      $v = Locatie::validate();
      if ($v->fails()) {
         return Redirect::back()->withInput()->withErrors($v);
      }

      $locatie->images = $this->handleImages($locatie);


      $locatie->fill(Input::all());

      if (is_admin()) {
         $locatie->approved = true;
      }

      $locatie->save();

      $locatie->sports()->sync(Input::get('sport'));

      Log::debug('['.__METHOD__.'] updated locatie $id ['.$id.'] by uid ' . Auth::user()->id);

      Session::flash('message', 'Locatia a fost salvata cu success.');

      return Redirect::to(href_locatie($locatie));
   }

   private function handleImages(&$locatie) {
      //handling files
      //complicated approach - it would have been better to
      //store directly the filename as json...
      $files = Input::file('img') ?: array();

      $newimgs = array();
      $fileno = 0;
      $copyright = Input::has('url') ? Input::get('url') : array();

      foreach ($files as $file) {

         if (!is_object($file)) {
            Log::debug('empty $file or not object ' . print_r($file,1));
            continue;
         }

         if ($file && !$file->getError()) {

            Log::debug('valid file found ' . print_r($file,1));

            $filename = Str::random(8) .'.'. strtolower($file->getClientOriginalExtension());

            Image::make($file->getRealPath())
            ->resize(Config::get('sitesettings.locatieW'), Config::get('sitesettings.locatieH'),  function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->save(public_path() . Config::get('sitesettings.imglocatii') . $filename);
            Image::make($file->getRealPath())
            ->resize(Config::get('sitesettings.locatietbW'), Config::get('sitesettings.locatietbH'),  function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->save(public_path() . Config::get('sitesettings.imglocatiitb') . $filename);

            $newimgs[] = array(
               'filename' => $filename,
               'uploader_id' => is_logged() ?: null,
               'copyright' => isset($copyright[$fileno]) ? $copyright[$fileno] : ''
               );
         }
         else {
            Log::error('['.__METHOD__.'] file upload failed with error' . $file->getError());
         }

         $fileno++;
      }


      $imgIds = array();
      if (count($newimgs)) {
         foreach ($newimgs as $newimg) {
            $upload = new Upload($newimg);
            $upload->save();
            $imgIds[] = $upload->id;
         }
      }

      $inDb = json_decode($locatie->images);
      $inDb = $inDb ? $inDb : array();
      $inDb = array_diff($inDb, Input::get('delimg') ? Input::get('delimg') : array());
      $imgIds = array_merge($imgIds, $inDb);

      return json_encode($imgIds);
   }


   public function getAproba($id) {
      if (!is_admin()) {
         Log::error('aproba access for $id ['.$id.'] by not admin id ['.is_logged().']');
         return Redirect::back();
      }

      $locatie = Locatie::find($id);
      if (!$locatie) {
         return Redirect::to('/locatii')->with('flash_error', $id . ' was not found');
      }

      if ($locatie->approved) {
         return Redirect::to(href_locatie($locatie))->with('flash_error', $id . ' was already approved');
      }

      $locatie->approved = 1;
      if (!$locatie->save()) {
         Log::error('failed to save locatie as approved');
      }


      return Redirect::to(href_locatie($locatie))->with('message', $locatie->name . ' approved');
   }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      //handle destroy by making it invisible?
      //prin(1,1);
    }


    public function getLocatiiSport() {
      //return "eeeeeeeee;";

        //$locatii = Locatie::active()->sport($id);
        //var_dump($locatii);
    }

    public function hideLocatie() {

    }

    public function getDelete($id) {
      $locatie = Locatie::where('id', '=', $id)->owned()->first();
      if (!isset($locatie->id) || !$locatie->id) {
         Log::error('[ '. __METHOD__ . '] invalid id ['.$id.'] supplied for edit by uid ['.Auth::user()->id.']');
         return Redirect::to('locatii');
      }

      $locatie->active = 0;

      Log::debug('disabling locatie $id ['.$id.']');

      $locatie->save();

      Log::debug('disabled locatie $id ['.$id.']');

      return Redirect::back();

   }

}