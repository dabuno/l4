<?php

class SportsController extends BaseController {

   public function __construct() {
      # code...

      /*
      $this->beforeFilter('sportnamecheck:sport',
         array('except' =>
            array(
               'getIndex',
            )
         )
      );
      */
   }

   /**
   * Display a listing of the resource.
   *
   * @return Response
   */
   public function getIndex()
   {
      $events = UserEvent::active()
         ->where('data', '>=', date('Y-m-d'))
         //->where('user_id', '!=', is_logged())
         ->with('sports', 'arene')
         ->orderBy('data', 'asc')
         ->orderBy('ora', 'asc')
         ->paginate(Config::get('sitesettings.perpage', 10));

      $pageTitle = 'Evenimente sportive';
      $pageDescription = 'Evenimente sportive, jucatori dornici sa faca sport si locatii unde sa faca sport.';
      return View::make('sports.events')
         ->with('events', $events)
         ->with(compact('pageDescription'))
         ->with(compact('pageTitle'));
   }

   /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
   public function evenimente($sport)
   {

      $sport = Sport::where('name', '=', $sport)->first();
      if (!$sport || !$sport->id) {
         Log::error('['.__METHOD__.'] sport name is not correct ['.$sport.']');
         return Redirect::to("/");
      }


      $events = UserEvent::FindBy($sport->id, 'sport_id')
         ->active()
         ->where('data', '>=', date('Y-m-d'))
         //->where('user_id', '!=', is_logged())
         ->with('sports', 'arene')
         ->orderBy('data', 'asc')
         ->orderBy('ora', 'asc')
         ->paginate(Config::get('sitesettings.perpage', 10));


      /*
      var_dump(DB::getQueryLog());
      var_dump($events);die();
      */

      $pageTitle = 'Evenimente '. $sport->name . '. Vreau sa joc ' . $sport->name;
      $pageDescription = 'Evenimente '. $sport->name . '. Vreau sa joc ' . $sport->name;
      return View::make('sports.events')
         ->with('events', $events)
         ->with(compact('sport'))
         ->with(compact('pageTitle'))
         ->with(compact('pageDescription'));
   }


   public function jucatori($sport)
   {

      $sport = Sport::where('name', '=', $sport)->first();
      if (!$sport || !$sport->id) {
         Log::error('['.__METHOD__.'] sport name is not correct ['.$sport.']');
         return Redirect::to("/");
      }

      $users = DB::table('users')
         ->join('users_orase', 'users_orase.id_user', '=', 'users.id')
         ->join('orase', 'orase.oras_id', '=', 'users_orase.id_oras')
         ->join('users_sports', 'users_sports.id_user', '=', 'users.id')
         ->where('users_sports.id_sport', '=', $sport->id)
         ->where('users.active', '=', 1)
         ->paginate(Config::get('sitesettings.perpage', 10));

      $gender = Config::get('sitesettings.userGen');


      $pageTitle = 'Jucatori '. $sport->name;
      $pageDescription = 'Jucatori de '. $sport->name . '. Vreau sa joc ' . $sport->name;
      return View::make('sports.jucatori')
         ->with(compact('sport'))
         ->with(compact('gender'))
         ->with(compact('users'))
         ->with(compact('pageTitle'))
         ->with(compact('pageDescription'));
   }

   public function arene($sport)
   {

      $sport = Sport::where('name', '=', $sport)->first();
      if (!$sport || !$sport->id) {
         Log::error('['.__METHOD__.'] sport name is not correct ['.$sport.']');
         return Redirect::to("/");
      }

      //$locatii = Locatie::active()->paginate(Config::get('sitesettings.perpage', 10));
      $locatii = DB::table('locatii')
         ->join('orase', 'locatii.oras_id', '=', 'orase.oras_id')
         ->join('locatii_sports', 'locatii_sports.locatie_id', '=', 'locatii.id')
         ->where('locatii_sports.sport_id', '=', $sport->id)
         ->where('locatii.active', '=', '1')
         ->where('locatii.approved', '=', '1')
         ->paginate(Config::get('sitesettings.perpage', 10));

      /*
       *
       * for all locatii get img ids from uplods
       *@todo duplicated - check also LocatiiController
       *
       */
      $idImages = array();
      foreach ($locatii as $locatie) {
         $locatie->images = json_decode(isset($locatie->images) ? $locatie->images : null );
         if ($locatie->images) {
            list($first) = $locatie->images;
            $idImages[$locatie->id] = $first;
         }
      }

      $locatiiImg = array();
      if (count($idImages)) {

         $imgIdflipped = array_flip($idImages);
         $locatiiImg = Upload::whereIn('id', $idImages)->get();
         $imgIdObj = array();
         foreach ($locatiiImg as $img) {
            $imgIdObj[$imgIdflipped[$img->id]] = $img;
         }
      }


      $pageTitle = 'Locatii '. $sport->name;
      $pageDescription = 'Tereneri de '. $sport->name . '. Locatii pentru vreau sa joc ' . $sport->name;
      return View::make('sports.arene')
         ->with(compact('sport'))
         ->with(compact('imgIdObj'))
         ->with(compact('locatii'))
         ->with(compact('pageTitle'))
         ->with(compact('pageDescription'));
   }

   public function getAdd() {
      if (!is_admin()) {
         Log::error('['.__METHOD__.'] user tried enter add sport area');
         return Redirect::to("/");
      }

      $sport = new Sport();

      return View::make('sports.add', compact('sport'));
   }

   public function postCreate() {

      if (!is_admin()) {
         Log::error('['.__METHOD__.'] user tried enter add sport area');
         return Redirect::to("/");
      }

      $v = Sport::validate();
      if ($v->fails()) {
         return Redirect::back()->withInput()->withErrors($v);
      }

      $sport = new Sport();
      $sport->name = Input::get('name');

      $sport->save();

      if (Input::has('jucatori')) {
         $sport->subcategorii()->attach(array('id_sub' => 1));
      }


      if (Input::has('locatii')) {
         $sport->subcategorii()->attach(array('id_sub' => 2));
      }




      Session::flash('message', 'Sportul ' . $sport->name . ' a fost salvat cu succes.');

      return Redirect::back();

   }

}