<?php

class EventsController extends BaseController {

   public function __construct()
   {
      # code...
      $this->beforeFilter('auth',
         array('except' =>
            array(
               'show',
            )
         )
      );
   }

   /**
   * Display a listing of the resource.
   *
   * @return Response
   */

   public function index()
   {
      $events = UserEvent::FindBy(Auth::user()->id)->with('sports', 'arene')->orderBy('data', 'desc')->orderBy('ora', 'desc')->paginate(Config::get('sitesettings.perpage', 10));
      return View::make('events.index')->with('events', $events);
   }

   /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
   public function create()
   {

      $event = new UserEvent;
      $event->data = date('Y-m-d');
      $event->ora = date('H:00');
      if (count(Auth::user()->orase)) {
         $event->oras_id = Auth::user()->orase[0]->oras_id;
      }

      if (Input::has('aid')) {
         $event->arena_id = (int)Input::get('aid');
      }
      if (Input::has('sid')) {
         $event->sport_id = (int)Input::get('sid');
      }

      $sports = array('0' => 'Alege') + Sport::active()->lists('name', 'id');
      $arene = array('0' => 'Alege') + Locatie::active()->lists('name', 'id');
      $orase = array('0' => 'Alege') + Oras::lists('oras', 'oras_id');

      return View::make('events.create')
         ->with(compact('event'))
         ->with(compact('sports'))
         ->with(compact('orase'))
         ->with(compact('arene'));
   }

   /**
   * Store a newly created resource in storage.
   *   * @return Response
   */
   public function store()
   {
      $v = UserEvent::validate();
      if ($v->fails()) {
         return Redirect::back()->withInput()->withErrors($v);
      }

      $event = new UserEvent(Input::all());
      $event->data = date2php(Input::get('data'));
      $event->user_id = Auth::user()->id;
      $event->save();

      Session::flash('message', 'Evenimentul a fost creat.');

      return Redirect::route('events.edit',$event->id);
   }

   /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
   public function show($id)
   {
      $event = UserEvent::findOrFail($id);

      $participanti = Convocare::with('destids')->where('user_event_id', '=', $id)
        ->where('dest_status', '=', CONVOCARE_DEST_ACCEPTATA)
        ->where('src_status', '=', CONVOCARE_SRC_ACTIVA)
        ->get();


      //Comments
      $noComments = Comment::counter('events/' . $id);
      $comments = Comment::getComments('events/' . $id);
      $captcha = null;
      if (!is_logged()) {
         $captcha = new Services\Validators\privateReCaptcha;
      }


      $pageTitle = 'Eveniment ' . $event->event . ' organizat de '. $event->users->alias;
      $pageDescription = 'Eveniment vreau sa joc ' . $event->sports->name .
         ' organizat de ' . $event->users->alias . ' in ' . ucfirst(strtolower($event->orase->oras));
      return View::make('events.show')
         ->with('event', $event)
         ->with(compact('participanti'))

         ->with(compact('noComments'))
         ->with(compact('comments'))
         ->with(compact('captcha'))



         ->with(compact('pageDescription'))
         ->with(compact('pageTitle'));
   }

   /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
   public function edit($id)
   {
      $event = UserEvent::where('id', '=', $id)->active()->owned()->first();

      if (!isset($event->id) || !$event->id) {
         Log::error('[ '. __METHOD__ . '] invalid id ['.$id.'] supplied for edit');
         return Redirect::to('events');
      }

      $sports = array('0' => 'Alege') + Sport::active()->lists('name', 'id');
      $arene = array('0' => 'Alege') + Locatie::active()->lists('name', 'id');
      $orase = array('0' => 'Alege') + Oras::lists('oras', 'oras_id');

      return View::make('events.create')
         ->with(compact('event'))
         ->with(compact('sports'))
         ->with(compact('orase'))
         ->with(compact('arene'));

   }

   /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
   public function update($id)
   {
      $event = UserEvent::where('id', '=', $id)->active()->owned()->first();
      $v = UserEvent::validate();
      if ($v->fails()) {
         return Redirect::back()->withInput()->withErrors($v);
      }

      $event->fill(Input::all());
      $event->data = date2php(Input::get('data'));
      $event->save();

      Session::flash('message', 'Evenimentul a fost updatat.');

      return Redirect::back();
   }

   /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
   public function destroy($id)
   {
     //
   }

   private function change($id, $status) {

      $event = UserEvent::where('id', '=', $id)->owned()->first();
      if (!isset($event->id) || !$event->id) {
         Log::error('[ '. __METHOD__ . '] invalid id ['.$id.'] supplied for edit');
         return false;
      }

      $event->active = $status;
      $event->save();

      return true;

   }

   public function getHide($id) {
      if (!$this->change($id, $status = 0)) {
         Log::error('[ '. __METHOD__ . '] id ['.$id.'] was not hidden');
         return Redirect::back();
      }

      return Redirect::back();
  }

   public function getShow($id) {
      if (!$this->change($id, $status = 1)) {
         Log::error('[ '. __METHOD__ . '] id ['.$id.'] was not shown');
         return Redirect::back();
      }

      return Redirect::back();
   }


   public function getVreausajoc($event) {

      $ret = array(
         'error' => 1,
         'msg' => 'Actiunea n-a putut fi procesata.'
      );

      $event = UserEvent::find($event);
      if (!isset($event->id) || !$event->id) {
         Log::error('[ '. __METHOD__ . '] invalid id ['.$id.'] supplied');
         return Response::json($ret);
      }

      $ret['msg'] = 'Eveniment gasit. checking locuri';


      DB::transaction(function() use($event, &$ret) {
         if (!$event->locuri) {
            $ret['msg'] = 'Nu mai sunt locuri disponibile';
            Log::error('[ '. __METHOD__ . '] no places available ['.$event->id.']');
            return false;
         }

         if ($event->user_id == is_logged()) {
            $ret['msg'] = 'Nu poti sa te inscrii. Se presupune ca esti deja inscris prin creearea evenimentului.';
            Log::error('[ '. __METHOD__ . '] cannot be source and dest for your event ['.$event->id.']');
            return false;
         }


         $already = Convocare::where('dest_id', '=', is_logged())
            ->where('user_event_id', '=', $event->id)
            ->where('dest_status', '!=', CONVOCARE_DEST_STEARSA)
            ->where('dest_status', '!=', CONVOCARE_DEST_ANULATA)
            ->where('dest_status', '!=', CONVOCARE_DEST_REFUZATA)
            //->where('src_status', '!=', CONVOCARE_SRC_ACTIVA)
            ->first();
         if ($already) {
            Log::error('[ '. __METHOD__ . '] already accepted ['.$event->id.'] by uid ['.is_logged().']');
            $ret['msg'] = 'Esti deja inregistrat';
            return false;
         }

         $event->locuri = $event->locuri - 1;

         $ret['msg'] = 'Eveniment gasit [' . $event->locuri . ']';

         $event->save();
         $convocare = new Convocare();
         $convocare->src_id = $event->user_id;
         $convocare->dest_id = is_logged();
         $convocare->dest_status = CONVOCARE_DEST_ACCEPTATA;
         $convocare->user_event_id = $event->id;
         $convocare->save();

         $ret['msg'] = 'Te-ai inregistrat cu succes.';
         $ret['error'] = 0;

      });

      return Response::json($ret);
   }


   /**
    * for a sport and logged uid will return the owned events available
    * @param  int $sport which sport id
    * @return json        returns to an ajax call
    */
   public function getEvents($sport) {

      $events = UserEvent::owned()
         ->where('data', '>=', date('Y-m-d'))
         ->with(array('convocari' => function($query) {
            $query->where('src_id', '=', Auth::user()->id);
         }));

      if ($sport) {
         $events->where('sport_id', '=', $sport);
      }

      $events = $events->get();

      if (!$events || !count($events)) {
         Log::error('['.__METHOD__.'] no event found for ['.$sport.'] and src_id['.is_logged().']');
         return Response::json(array('error' => '0', 'content' => 'Nu ai nici un eveniment creat pentru acest sport.'));
      }

      //and if already convocat


      //send date2js
      $convocari = array();
      $ret = array();
      $k = 0;
      foreach ($events as &$event) {

         $ret[$k] = array(
            'id' => $event->id,
            'event' => $event->event,
            'jsdate' => date2js($event->data),
            'ora' => date('H:i', strtotime($event->ora)),
         );

         if (!$sport) {
            $ret[$k]['sport'] = $event->sports->name;
         }

         foreach ($event->convocari as $convocare) {
            if ($convocare->src_status == CONVOCARE_SRC_STEARSA) {
               continue;
            }
            if (!isset($convocari[$convocare->dest_id])) {
               $convocari[$convocare->dest_id] = array();
            }
            array_push($convocari[$convocare->dest_id], $event->id);

         }

         $k++;
      }

      return Response::json(array('content' => $ret, 'convocari' => $convocari));
   }

}
