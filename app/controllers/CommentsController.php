<?php

class CommentsController extends BaseController {
   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
   public function index()
   {

      //$url = 'events/2';
      //$comments = Comment::URL($url)->with('users')->get()->all();

      //prin($comments->users->alias);

      //return View::make('comments.index');
   }

   /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
   public function create()
   {
        return View::make('comments.create');
   }

   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
   public function store()
   {
      $this->ret = Config::get('sitesettings.ret');

      $v = Comment::validate(Input::all());
      if ($v->fails()) {
         $this->ret['msg'] = $v->messages()->all();
         return $this->ret();
      }

      $comment = new Comment;

      $comment->comment = Input::get('comment');
      $comment->owner = is_logged() ?: 0;
      $comment->url = Input::get('url');
      $comment->hashurl = md5(Input::get('url'));
      $findingId = preg_match('@.*/([1-9][0-9]*)@', Input::get('url'), $matches);
      if ($matches && isset($matches[1])) {
         $comment->entity_id = $matches[1];
      }
      else {
         Log::error('['.__METHOD__.'] failed to find entity id for url: ['.$comment->url.']');
      }

      if (Input::has('reply') && Input::get('reply')) {

         //check reply id and reply url
         $reply = Comment::find(Input::get('reply'));

         if (!$reply || !$reply->url || $reply->url != $comment->url) {
            Log::error('reply id ['.Input::get('reply').'] not found or invalid url');
            return $this->ret();
         }
         $comment->parent = Input::get('reply');
      }

      if (!$comment->save()) {
         Log::error('failed to save comment ['.print_r($comment, 1).']');
         return $this->ret();
      }

      Log::debug('successfully saved comment ['.$comment->id . ']');
      $this->ret['error'] = 0;
      $this->ret['msg'] = 'Commentul a fost salvat';
      $this->ret['comment'] = [
         'id' => $comment->id,
         'comment' => e($comment->comment),
         'created_at' => acumTime($comment->created_at),
      ];

      return $this->ret();
   }


   private function ret() {
      if (Request::ajax()) {
         return Response::json($this->ret);
      }

      return Redirect::to(URL::previous() . (isset($this->ret['comment']) ? '#comment-' . $this->ret['comment']['id'] : ''))->with('flash_comment', $this->ret);
   }


   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
   public function show($id)
   {
        return View::make('comments.show');
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
   public function edit($id)
   {
        return View::make('comments.edit');
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
   public function update($id)
   {
      //
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
   public function destroy($id)
   {
      //
   }

}
