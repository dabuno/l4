<?php

class NotificariController extends \BaseController {

   public function __construct(Notification $notification) {
      $this->user = Auth::user();
      $this->notification = $notification;
      $this->beforeFilter('auth', array(
         'execpt' => ['dezabonare']
      ));
   }

   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
   public function index()
   {
      $notificari = Notification::where('user_id', '=', $this->user->id)->first();
      if (!isset($notificari->user_id)) {
         $notificari = new Notification();
      }



      return View::make('notificari.notificari', compact('user'))->with(compact('notificari'))->with('user', $this->user);
   }


   public function update() {
      $notificari = Notification::where('user_id', '=', $this->user->id)->first();

      if (!isset($notificari->user_id)) {
         $notificari = new Notification();
      }
      else {
         //reset all fillable - notification settings
         foreach ($notificari->getFillable() as $value) {
            $notificari->$value = 0;
         }
      }

      $notificari->fill(Input::all());

      $notificari->user_id = $this->user->id;
      if (! $notificari->basetoken) {
         $notificari->basetoken = Str::random();
      }
      $notificari->save();

      return Redirect::back()->with('message', 'Modificarile au salvate.');
   }

   public function dezabonare() {
      //todo
   }
}