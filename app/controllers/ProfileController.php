<?php

class ProfileController extends \BaseController {

   public function __construct()
   {
      # code...
      //$this->beforeFilter('auth');
      $this->beforeFilter('auth',
         array('except' =>
            array(
               'show',
            )
         )
      );
   }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
      $user = Auth::user();
      $user->sports = $user->sports->toArray();
      $user->orase = $user->orase->toArray();

      $gender = Config::get('sitesettings.userGen');

      //Comments
      $commentURL = 'profile/' . (int)$user->id;
      $noComments = Comment::counter($commentURL);
      $comments = Comment::getComments($commentURL);
      $captcha = null;
      if (!is_logged()) {
         $captcha = new Services\Validators\privateReCaptcha;
      }



      $pageTitle = 'Profilul '. $user->alias;
      return View::make('profile.view')->with('user', $user)
         ->with(compact('gender'))
         ->with(compact('pageTitle'))

         ->with(compact('captcha'))
         ->with(compact('commentURL'))
         ->with(compact('noComments'))
         ->with(compact('comments'));

   }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
   {
      $user = User::find($id);

      $user->sports = $user->sports->toArray();
      $user->orase = $user->orase->toArray();

      $gender = Config::get('sitesettings.userGen');


      //Comments
      $commentURL = 'profile/' . (int)$id;
      $noComments = Comment::counter($commentURL);
      $comments = Comment::getComments($commentURL);
      $captcha = null;
      if (!is_logged()) {
         $captcha = new Services\Validators\privateReCaptcha;
      }


      $pageTitle = 'Profilul '. $user->alias;
      return View::make('profile.view')->with('user', $user)
         ->with(compact('gender'))
         ->with(compact('pageTitle'))

         ->with(compact('captcha'))
         ->with(compact('commentURL'))
         ->with(compact('noComments'))
         ->with(compact('comments'));

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
      if ($id != Auth::user()->id) {
         return Redirect::to('/');
      }

      $user = Auth::user();
      $user->sports = $user->sports->lists('id');
      $user->orase = $user->orase->lists('oras_id');

      $orase = array('0' => 'Alege') + Oras::all()->lists('oras', 'oras_id');
      $sports = Sport::active()->lists('name', 'id');
      $gen = Config::get('sitesettings.userGen');


      $pageTitle = 'Editare profil '. $user->alias;
      return View::make('profile.edit')
         ->with('user', $user)
         ->with('orase', $orase)
         ->with('sports', $sports)
         ->with(compact('gen'))
         ->with(compact('pageTitle'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
      Log::info('['.__METHOD__.'] processing post for user ['.$id.']');


      $rules = array(
         'profile_image' => 'mimes:jpeg,gif,jpg,png|max:10000',
         'user_pref'     => 'required',
         //'prenume'       => 'required_if:user_pref,prenume si nume e vizibil|required_if:user_pref,prenume si n. e vizibil|alpha_dash|min:3',
         //'nume'          => 'required_if:user_pref,prenume si nume e vizibil|required_if:user_pref,prenume si n. e vizibil|alpha_dash|min:1',
         'gen'           => 'required|numeric|min:1|max:2',
         //'alias'         => 'required_if:user_pref,alias e vizibil',
         'sport'         => 'required|multipleint',
         'orase'         => 'required|numeric',

      );


      $user_pref = 0;
      if (Input::get('user_pref') == USER_PREF_SHOW_FULLNAME) {
         $user_pref = USER_PREF_SHOW_FULLNAME;
         $rules['prenume'] = 'required|alpha_dash|min:3';
         $rules['nume'] = 'required|alpha_dash|min:1';
      }
      if (Input::get('user_pref') == USER_PREF_SHOW_NAMETRIM) {
         $user_pref = USER_PREF_SHOW_NAMETRIM;
         $rules['prenume'] = 'required|alpha_dash|min:3';
         $rules['nume'] = 'required|alpha_dash|min:1';
      }
      if (Input::get('user_pref') == USER_PREF_SHOW_ALIAS) {
         $user_pref = USER_PREF_SHOW_ALIAS;
         $rules['alias'] = 'required|noxss_tags|min:3';
      }

      if (!$user_pref) {
         Log::error('['.__METHOD__.'] user pref is missing for user id ['.$id.']');
      }

      $validation = Validator::make(Input::all(), $rules);

      if ($validation->fails()) {
         return Redirect::back()->withInput()->withErrors($validation);
      }

      $user = User::find($id);

      $file = false;
      if (Input::hasFile('profile_image')) {
         $file = Input::file('profile_image');
      }


      if ($file && !$file->getError()) {
         $file = Input::file('profile_image');
         $filename = Str::random(20) .'.'. strtolower($file->getClientOriginalExtension());
         Image::make($file->getRealPath())
            ->resize(Config::get('sitesettings.uimgMW'), Config::get('sitesettings.uimgMH'), $ratio = true)
            ->save(public_path() . Config::get('sitesettings.uimg') . $filename);
         Image::make($file->getRealPath())
            ->resize(Config::get('sitesettings.uimgtbMW'), Config::get('sitesettings.uimgtbMH'), $ratio = true)
            ->save(public_path() . Config::get('sitesettings.uimgtb') . $filename);
         Image::make($file->getRealPath())
            ->resize(
               Config::get('sitesettings.uimgmtbMW'),
               Config::get('sitesettings.uimgmtbMH'), $ratio = true)
            ->save(public_path() . Config::get('sitesettings.uimgmtb') . $filename);

         $user->profile_image = $filename;
      }
      elseif ($file && $file->getError()) {
         Log::error('['.__METHOD__.'] file upload failed with error' . $file->getError());
      }

      if ($user_pref == USER_PREF_SHOW_FULLNAME) {
         $user->alias = Input::get('prenume') . ' ' . Input::get('nume');
      }
      elseif ($user_pref == USER_PREF_SHOW_NAMETRIM) {
         $user->alias = Input::get('prenume') . ' ' . substr(Input::get('nume'), 0, 1);
      }
      else {
         $user->alias = strip_tags(Input::get('alias'));
      }

      $user->firstname = Input::get('prenume');
      $user->lastname = Input::get('nume');
      $user->nic_option = $user_pref;
      $user->gen = Input::get('gen');
      $user->orase()->sync(array(Input::get('orase')));
      $user->sports()->sync(Input::get('sport'));

      //@todo modify session alias
      //
      //@todo set alias with user pref
      //
      //@user pref in constants

      $user->save();

      Session::flash('message', 'Modificarile au fost salvate.');

      return Redirect::back();

	}


   public function getPwd() {
      $user = Auth::user();
      return View::make('profile.pwd', compact('user'));
   }

   public function postPwd() {

      $v = User::pwdvalidate();
      if ($v->fails()) {
         return Redirect::back()->withErrors($v);
      }

      $user = Auth::user();

      if (Hash::check(Input::get('parola'), $user->password))
      {
         // The passwords match...
         $user->password = Hash::make(Input::get('newpassword'));
         $user->save();
      }
      else {
         Session::flash('error', 'Parola curenta e incorecta');
         return Redirect::back();
      }

      Session::flash('message', 'Parola noua a fost schimbata cu succes.');
      return Redirect::back();
   }

}