<?php
use \WebGuy;

class HelperLogin
{
   public function loginWithValidCredentials(WebGuy $I)
   {
      $I->amOnPage('/vreausajoc.ro');
      $I->click('Login');

      $I->seeCurrentUrlEquals('/vreausajoc.ro/login');

      $I->fillField('email', 'bd@vsj.ro');
      $I->fillField('password', '12345678');

      $I->click('Login');

      $I->seeInCurrentUrl('/vreausajoc.ro/profile');
   }
}