<?php
namespace Codeception\Module;

use Illuminate\Support\Str;


// here you can define custom actions
// all public methods declared in helper class will be available in $I

class AcceptanceHelper extends \Codeception\Module\WebDriver
{

   protected $event;
   protected $sportName = "Squash";

   public function _beforeSuite()
   {
        //codecept_debug($this->webDriver);
   }

   public function loggedin($username, $password) {
      $this->amOnPage('/vreausajoc.ro');
      $this->click('Login');

      $this->seeCurrentUrlEquals('/vreausajoc.ro/login');

      $this->fillField('email', $username);
      $this->fillField('password', $password);

      $this->click('Login');

      $this->seeInCurrentUrl('/vreausajoc.ro/profile');
   }


   public function login_as_BD() {
      $this->loggedin('bd@vsj.ro', '12345678');
   }

   public function create_event() {
      $this->click('Evenimente');
      $this->seeInCurrentUrl('/events');
      $this->click('Creeaza eveniment');
      $this->seeInCurrentUrl('/events/create');

      $this->event = "Autogen" . rand(0, 9999999);
      $this->sportName = "Squash";

      $this->fillField('input[name="event"]', $this->event);
      $this->selectOption('select[name="sport_id"]', $this->sportName);
      $this->waitForJS("return $.active == 0;", 60);
      $this->executeJS('$(".arena").val('.rand(1,4).');');
      $this->fillField('#datepicker', date2js(date('Y-m-d', strtotime('+'.rand(1,7) .' days'))));
      $this->fillField('#timepicker', date('H:s'));
      $this->fillField('input[name="locuri"]', rand(1,2));
      $this->click('input[type="submit"]');
      $this->dontSeeElement('.alert-error');
      $this->see('Evenimentul a fost creat.');
      $this->seeInCurrentUrl('/edit');

   }

   public function see_event_in_sport() {
      $this->click($this->sportName);
      $this->see($this->event);
   }

   public function challenge_with_event() {
      $this->click($this->sportName);
      $this->click("Jucatori");
      $this->click('#go_16');
      $this->waitForJS("return $.active == 0;", 60);
      $this->executeJS("$('.convoacack:not(:checked').eq(0).trigger('click');");
      $this->click('.goajax');
      $this->waitForJS("return $.active == 0;", 60);
      $this->see('- Convocat');

   }
}