<?php
use \WebGuy;

class LoginCest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function tryToTest(WebGuy $I) {
 
    }

   public function loginWithInvalidCredentials(WebGuy $I)
   {
      $I->amOnPage('/vreausajoc.ro');
      $I->click('Login');
      $I->seeCurrentUrlEquals('/vreausajoc.ro/login');
      $I->click('Login');
      $I->see('Combinatia email/parola a fost incorecta.', '.alert.alert-error');         
   }


   public function loginWithValidCredentials(WebGuy $I)
   {
      $I->amOnPage('/vreausajoc.ro');
      $I->click('Login');
      
      $I->seeCurrentUrlEquals('/vreausajoc.ro/login');

      $I->fillField('email', 'bd@vsj.ro');
      $I->fillField('password', '12345678');

      $I->click('Login');

      $I->seeInCurrentUrl('/vreausajoc.ro/profile');      
   }

}