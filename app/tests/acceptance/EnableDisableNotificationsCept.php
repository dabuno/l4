<?php
$I = new WebGuy($scenario);
$I->wantTo('login as bd, disable and enable notificari');
$I->login_as_BD();
$I->click('Notificari');
$I->seeInCurrentUrl('profile/notificari');

#uncheck all
$I->uncheckOption('input[name="suntconvocat"]');
$I->uncheckOption('input[name="comentariuprofil"]');
$I->uncheckOption('input[name="comentariueveniment"]');
$I->uncheckOption('input[name="functionalitatenoua"]');
$I->click('Modifica');
$I->see('Modificarile au salvate.');
$I->dontSeeCheckboxIsChecked('input[name="suntconvocat"]');
$I->dontSeeCheckboxIsChecked('input[name="comentariuprofil"]');
$I->dontSeeCheckboxIsChecked('input[name="comentariueveniment"]');
$I->dontSeeCheckboxIsChecked('input[name="functionalitatenoua"]');

#check all
$I->checkOption('input[name="suntconvocat"]');
$I->checkOption('input[name="comentariuprofil"]');
$I->checkOption('input[name="comentariueveniment"]');
$I->checkOption('input[name="functionalitatenoua"]');
$I->click('Modifica');
$I->see('Modificarile au salvate.');
$I->seeCheckboxIsChecked('input[name="suntconvocat"]');
$I->seeCheckboxIsChecked('input[name="comentariuprofil"]');
$I->seeCheckboxIsChecked('input[name="comentariueveniment"]');
$I->seeCheckboxIsChecked('input[name="functionalitatenoua"]');