<?php
class CommentsCest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function tryToTest(WebGuy $I) {

    }

   public function CommentsLoggedCredentials(WebGuy $I)
   {
      $I->amOnPage('/vreausajoc.ro');
      $I->click('Login');

      $I->seeCurrentUrlEquals('/vreausajoc.ro/login');

      $I->fillField('email', 'bd@vsj.ro');
      $I->fillField('password', '12345678');

      $I->click('Login');

      $I->seeInCurrentUrl('/vreausajoc.ro/profile');

      $I->dontSee('Login');
      $I->dontSee('Inregistrare');

      $I->click('Trimite');
      $I->see('Nu ai scris nimic');

      $tmp = rand(0, 999999999);

      $I->fillField('#gcomment', 'test comment' . $tmp);
      $I->click('Trimite');
      $I->dontSee('Nu ai scris nimic');
      $I->waitForJS("return $.active == 0;", 60);

      $I->see('test comment' . $tmp);

      //$I->amOnPage('/events/4');
      //$I->seeCurrentUrlEquals('/vreausajoc.ro/events/4');
      //$I->seeResponseCodeIs(200);
      //$I->waitForText('Nu ai scris nimic', 1); // secs
   }

}