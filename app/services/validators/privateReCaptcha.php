<?php namespace Services\Validators;

class privateReCaptcha extends \Captcha\Captcha
{
   protected $publicKey = '6Lcq_90SAAAAAMNrXeSbwwPC1OuuDyf8pQKiTj1H';
   protected $privateKey = '6Lcq_90SAAAAAGG_3EVVlWZFzD248MSMn-dgkMg-';

   public $captcha;

   function __construct() {
      $this->captcha = new \Captcha\Captcha();
      $this->captcha->setPublicKey($this->publicKey);
      $this->captcha->setPrivateKey($this->privateKey);
   }
}