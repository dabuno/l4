<?php namespace Services;

use \Log, \Config, \Mail;

class Notifier {

   public static function email($template, $from, $to, $subject = null, $data = null) {

      Log::info('sending email with $subject ['. $subject .'] ', [__METHOD__]);

      //prin(func_get_args(), 1);

      if (app()->environment() == 'local') {
         Log::warning('['.__METHOD__.'] Emails are not sent. - just simulated');
         return false;
      }

      Mail::queue($template, compact('data'), function($message) use ($data, $to, $subject)
      {
         $message
            ->to($to)
            ->subject($subject);
      });

   }

   public static function admin_email($subject, $body, $from = null) {

      Log::info('sending email with $subject ['. $subject .'] ', [__METHOD__]);

      $admin = Config::get('sitesettings.admin', 'vreausajoc.ro@gmail.com');

      self::email('emails.dump', $from, $admin, $subject, $body);
   }


   public function fire($job, $data) {
      Log::debug('processing job', ['job' => $job, 'data' => $data]);

      //self::email($data['template'])

      $job->delete();
      //$job->release();
   }

}