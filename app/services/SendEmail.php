<?php namespace Services;

use \Mail, \Log;

class SendEmail {

   public function fire($job, $data)
   {

      Log::debug('['. __METHOD__ .'] data for email: ' . print_r($data));
      Mail::send($data['template'], $data, function($message) use ($data)
      {
         $message
            ->to($data['to'])
            ->subject($data['subject']);
      });

      //dd(1);


      $job->delete();
    }

}