<?php

//Route::controller('commentsapi', 'CommentsController');
// event Resource
Route::resource('comments', 'CommentsController');

// event Resource
Route::resource('events', 'EventsController');

//bc fuck resource box
Route::controller('eventsapi', 'EventsController');

//bc fuck resource box
Route::controller('locatiiapi', 'LocatiiController');

//bc fuck resource PDF_show_boxed(p, text, left, top, width, height, mode, feature)
Route::controller('convocariapi', 'ConvocariController');

//bc fuck resource box
Route::controller('convocari', 'ConvocariController');

//bc fuck resource box
//Route::controller('sports', 'SportsController');
Route::get('profile/notificari', 'NotificariController@index');
Route::post('profile/notificari', 'NotificariController@update');
Route::get('dezabonare', 'NotificariController@dezabonare');


Route::get('profile/pwd', 'ProfileController@getPwd');
Route::post('profile/pwd', 'ProfileController@postPwd');

Route::resource('profile', 'ProfileController', array("only" => array('index', "show", "edit", "update")));



View::composer('_parts.menu', function($view){
   $sports = Sport::active()->with(array('subcategorii' => function($query) {
      $query->active();
   }))->active()->get()->toArray();


   /* replaced with eager loading
   foreach($sports as &$sport) {
      $sport['subcategorii'] = Sport::find($sport['id'])->subcategorii()->active()->get()->toArray();
   }
   */
   //$view
   //View::share('leftmenuSports', $sports);
   $view->with('sports', $sports);
});





Route::get('forgot', function() {
   return View::make('auth.forgot');
});

Route::post('forgot', function() {

   $rules = array('email' => 'required|email');
   $v = Validator::make(Input::all(), $rules);

   if ($v->fails()) {
      return Redirect::back()->withInput()->withErrors($v);
   }

   $user = User::where('email', '=', Input::get('email'))->first();


   $pwd = Str::random(10);
   $user->password = Hash::make($pwd);

   $data = array('password' => $pwd);
   Mail::send('emails/auth/forgot', $data, function($message) use ($user)
   {
      $message->to($user->email)
      ->subject('vreausajoc - o nou user s-a inregistrat');
   });

   $user->save();

   Session::flash('message', 'Parola schimbata cu succes. <br />Noua parola ai primit-o pe email.');
   return Redirect::to('login');
});


Route::get('register', function() {
   $captcha = new Services\Validators\privateReCaptcha;

   $gen = Config::get('sitesettings.userGen');

   $pageTitle = 'Inregistreare utilizator';
   $pageDescription = 'Inregistrare utilizator vreausajoc.ro. Intra in comunitarea vreau sa joc, sa faci sport';
   return View::make('auth.register')
   ->with('sports',Sport::active()->lists('name', 'id'))
   ->with('captcha', $captcha)
   ->with(compact('gen'))
   ->with(compact('pageDescription'))
   ->with(compact('pageTitle'));
});

Route::post('register', function() {
   $v = User::validate(Input::all());

   if ($v->passes()) {

      $rules = array('email' => 'unique:users');
      $v = User::validate(Input::all(), $rules);
      if ($v->passes()) {
         $user = new User;
         $user->firstname = Input::get('first_name');
         $user->lastname  = Input::get('last_name');
         $user->email      = Input::get('email');
         $user->gen      = Input::get('gen');
         $user->password   = Hash::make(Input::get('password'));
         $user->alias   = $user->firstname . ' ' . $user->lastname;
         //$user->sport_id      = Input::has('sport') ? intval(Input::get('sport')) : null;
         if ($user->save()) {
            $user->sports()->attach(intval(Input::get('sport')));

            Log::info('['.__METHOD__.'] User id ['.$user->id.'] created.');


            /*
            |
            | Sending emails.
            |
             */
            $userId = $user->id;
            Queue::push(function($job) use ($userId) {
               $user = User::find($userId);
               $data = [ 'user' => $user ];

               /*
               |
               | Sending emails.
               |
               */
               Mail::send('emails/welcome', $data, function($message) use ($user) {
                  $message->to($user->email)
                        ->subject('Contul tau la vreausajoc.ro');
               });


               Mail::send('emails/auth/newuser', $data, function($message)
               {
                  $message->to(Config::get('sitesettings.enotif.users', 'bogdan.durla@gmail.com'))
                     ->subject('vreausajoc - o nou user s-a inregistrat');
               });


               $job->delete();
            });



            Auth::login($user);

            Log::info('['.__METHOD__.'] redirecting ['.$user->id.'] to profile.');

            return Redirect::to('profile/');
         }
      }
      /*
      $user = new stdClass;
      $user->name = 'Bogdan';
      $user->email = 'bogdan.durla@gmail.com';
      $data = [ 'user' => $user ];

      var_dump($user);
      */

      // email view, data for view, closure to send email
      /*
      Mail::send('emails/auth/welcome', $data, function($message) use ($user)
      {
         $message->to($user->email)->subject('Bine ai venit la vreausajoc!');
      });
      */

      //return 'Welcome email sent!';
}


return Redirect::to('register')
->with('flash_error', 'Crearea de user a esuat')
->withInput(Input::except('password'))
->withErrors($v->messages());



});


Route::get('/logout', array('as' => 'logout', function () {
  Auth::logout();

  return Redirect::to('/')
  ->with('flash_notice', 'You are successfully logged out.');
}))->before('auth');



Route::get('/login', function() {
   $pageTitle = 'Login utilizator';
   $pageDescription = 'Login utilizator vreausajoc.ro. Intra in comunitarea vreau sa joc, sa faci sport';
   return View::make('auth.login')
   ->with('message', false)
   ->with('email', false)
   ->with(compact('pageTitle'))
   ->with(compact('pageDescription'));

})->before('guest');;

Route::post('/login', function() {
   $credentials = array(
     'email' => Input::get('email'),
     'password' => Input::get('password')
     );

   //var_dump($credentials);

   $password = Hash::make(Input::get('password'));
   // perform validation
   if ( Auth::attempt($credentials) )
   {
      // Credentials match. Logged in!
      //echo 'valid';
      Auth::user()->touch();
      //Symfony\Component\HttpFoundation\Request::trustProxyData();
      Log::info('['.__METHOD__.'] User id ['.Auth::user()->id.'] IP ['.Request::getClientIp().'] logged in');
      //User::find(Auth::user()->id)->touch();

      return Redirect::intended('profile/');
   }
   Log::error('['.Input::get('email') .' failed to login.]');

   return Redirect::to('login')
   ->with('flash_error', 'Combinatia email/parola a fost incorecta.')
   ->withInput(Input::except('password'));
});






/*
|--------------------------------------------------------------------------
| Root /
|--------------------------------------------------------------------------
|
| Here you may configure the connection information for each server that
| is used by your application. A default configuration has been added
| for each back-end shipped with Laravel. You are free to add more.
|
*/
Route::get('/', function() {

   $last10events  = UserEvent::last()->with('orase')->orderBy('created_at')->take(Config::get('sitesettings.lastXevents', 10))->get();
   $last10users   = User::last()->orderBy('created_at')->take(Config::get('sitesettings.lastXusers', 10))->get();
   $last10locatii = Locatie::last()->with('orase')->orderBy('created_at')->take(Config::get('sitesettings.lastXlocatii', 10))->get();;

   $out = merge($last10events, $last10users, function($a1, $a2) {
      return -(strcmp($a1->created_at, $a2->created_at));
   });

   $out = merge($out, $last10locatii, function($a1, $a2) {
      return -(strcmp($a1->created_at, $a2->created_at));
   });

   $ultimeleXnoutati = $out->slice(0, Config::get('sitesettings.ultimeleXnoutati', 10));

   return View::make('index')
      ->with(compact('ultimeleXnoutati'));
});








//add a location
//used in create/edit event
Route::post('locatii/add/', function() {

   $rules = array(
      'name' => 'Required|Min:3|Max:250|noxss_tags',
      'address' => 'Required|Min:5|Max:500|noxss_tags',
      'localitate' => 'Required|Min:1|noxss_tags',
      'sport' => 'Required|Min:1|noxss_tags',
      );

   $v = Validator::make(Input::all(), $rules);
   if ($v->fails()) {
      Log::warning('['.__METHOD__.'] failed to validate arena' . print_r(Input::all(),1));
      $messages = '';
      foreach ($v->messages()->all() as $value) {
         $messages .= $value . ' ';
      }
      return Response::json(array('status' => 'error', 'msg' => 'Adaugarea de arena a esuat.' . $messages));
   }

   $locatie = new Locatie;
   $locatie->name = Input::get('name');
   $locatie->adresa = Input::get('address');
   $locatie->oras_id = Input::get('localitate');
   $locatie->added_by_user_id = Auth::user()->id;

   $locatie->save();

   $locatie->sports()->sync(array('sport_id' => Input::get('sport')));

   $emailArr = $locatie->toArray();
   $emailArr['adresa'] = $locatie->adresa;
   $emailArr['oras'] = $locatie->orase->oras;
   $emailArr['sport'] = $locatie->sports()->first()->name;
   $emailArr['added_by_user_id'] = $locatie->added_by_user_id;
   $emailArr['alias'] = Auth::user()->alias;

   $data = array('emailArr' => $emailArr);

   Mail::send('emails/locatii/newlocatie', $data, function($message) use($emailArr)
   {
      $message->to(Config::get('sitesettings.enotif.areneadmin', 'bogdan.durla@gmail.com'))
      ->subject('vreausajoc - o noua admin locatie a fost adaugata');
   });



   //@todo send email to admin
   return Response::json(array('id' => $locatie->id, 'status' => 'ok', 'msg' => 'Arena a fost adaugata.'));

})->before('auth');




//vsj.ro/locatii/sport-locatii/3
//@called in create/edit event
Route::get('locatii/sport-locatii/{id}', function($id) {
   return $locatii = Sport::where('id', '=', $id)->with("Locatii")->first();
});




Route::get('sports/', array('uses' => 'SportsController@getIndex'));
Route::get('sports/add', array('uses' => 'SportsController@getAdd'));


Route::get('sports/{name}', array('uses' => 'SportsController@arene'))->where('name', '[A-Za-z-]+');
Route::get('sports/{name}/Jucatori', array('uses' => 'SportsController@jucatori'))->where('name', '[A-Za-z-]+');
Route::get('sports/{name}/Evenimente', array('uses' => 'SportsController@evenimente'))->where('name', '[A-Za-z-]+');
Route::get('sports/{name}/Locatii', array('uses' => 'SportsController@arene'))->where('name', '[A-Za-z-]+');

Route::controller('sports', 'SportsController');




Route::get('locatii/{name}-{id}', function($name, $id)
{
   $split = preg_split('@-@', $id);
   $id = (int)array_pop($split);

   if (!$id) {
      Log::error('['.__METHOD__.'] empty id with fnc param ['.$id.']' );
      return Redirect::to('/');
   }

   $request = Request::create('/locatii/' . $id , 'GET');
   return Route::dispatch($request)->getContent();
});

Route::resource('locatii', 'LocatiiController');




Route::get('contact', function() {
   //var_dump(Config::get('sitesettings.enotif.propunere', 'bogdan.durla@gmail.com'));
   //var_dump(Config::get('sitesettings.areneadmin', 'bogdan.durla@gmail.com'));
   $captcha = new Services\Validators\privateReCaptcha;

   $pageTitle = 'Contacteaza echipa';
   return View::make('contact')->with(compact('captcha'))
   ->with(compact('pageTitle'));
});
Route::post('contact', function() {
   $rules = array(
      'text' => 'required|max:500',
      );

   if (!is_logged()) {
      $rules['email'] = 'required|email';
      $rules['recaptcha_response_field'] = 'Required|testrecapthca';
   }

   $v = Validator::make(Input::all(), $rules);
   if ($v->fails()) {
      return Redirect::back()->withInput()->withErrors($v);
   }

   $prop = new Propune;
   $prop->email = is_logged() ? Auth::user()->email : Input::get('email');
   $prop->prop_text = Input::get('text');
   if (is_logged()) {
      $prop->id_user = Auth::user()->id;
   }
   $prop->save();

   $data = array('propunere' => $prop);
   Mail::send('emails/propunere', $data, function($message)
   {
      $message->to(Config::get('sitesettings.enotif.propunere', 'bogdan.durla@gmail.com'))
      ->subject('vreausajoc - o noua prounere');
   });


   Session::flash('message', 'Comment-ul tau a fost trimis cu succes.');

   return Redirect::back();

});






/**
 * Always have to be the last one
 * since is matching all routes/
 */


//vsj.ro/User-13
//
// for urls like mysite/work/1-the-title
Route::get('{alias}', function($alias)
{
   $split = preg_split('@-@', $alias);
   $id = (int)array_pop($split);

   if (!$id) {
      Log::error('['.__METHOD__.'] empty id');
      return Redirect::to('/');
   }

   $request = Request::create('/profile/' . $id, 'GET');
   return Route::dispatch($request)->getContent();
});
