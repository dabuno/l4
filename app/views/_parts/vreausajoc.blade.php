<script type="text/javascript">
   $(function() {
      $('.vreausajoc').click(function() {

         if (!$.defaultsLoggedId) {
            if (confirm('Trebuie sa fii logat pentru a putea convoca un utilizator. \r\nMergi la pagina de login?')) {
               document.location.href = '{{URL::to('login');}}';
               return false;
            }

            return false;
         }

         var event = $(this).attr('id').split('_')[1];
         var $holder = $(this).parents('.holder');
         var holder = $holder.html();

         $.ajax({
            url: '{{URL::to('/eventsapi/vreausajoc/')}}' + '/' + event,
            success: function(pdata) {
               if (pdata.error) {

               }
               else {
                  holder = '';
               }
               $holder.html(pdata.msg + '<br / >' + holder);


            },
            error: function() {
               $holder.html('Actiunea n-a putut fi procesata.');
            }
         });

      });
   });
</script>