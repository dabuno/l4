<!-- parts/menu.blade.php -->
   <div class="span3"  style="width:220px">
      <div class="well sidebar-nav">
         <ul class="nav nav-list">

            @if (Auth::check())
               <li class="nav-header">Contul meu</li>

               <li @if (Request::segment(1) == 'profile' && Request::segment(2) == '') class="active" @endif>
                  <a href="{{URL::to('profile')}}">Profil</a>

                  @if (Request::segment(1) == 'profile')
                  <ul class="level0">
                     <li @if (Request::segment(3) == 'edit') class="active level1" @else class="level1" @endif>
                     <a href="{{route('profile.edit', $user->id)}}">Editeaza profil</a></li>
                     </li>

                     <li @if (Request::segment(2) == 'pwd') class="active level1" @else class="level1" @endif>
                     <a href="{{URL::to('profile/pwd')}}">Schimba parola</a></li>
                     </li>

                     <li @if (Request::segment(2) == 'notificari') class="active level1" @else class="level1" @endif>
                     <a href="{{URL::to('profile/notificari')}}">Notificari</a></li>
                     </li>

                  </ul>
                  @endif
               </li>

               <li @if (Request::segment(1) == 'events' && Request::segment(2) != 'create') class="active" @endif>
                  <a href="{{URL::to('events')}}">Evenimente</a>

                  @if (Request::segment(1) == 'events')
                  <ul class="level0">
                     <li @if (Request::segment(2) == 'create')  class="active level1 " @else class="level1" @endif>
                        <a href="{{URL::to('events/create')}}">Creeaza eveniment</a>
                     </li>
                  </ul>
                  @endif
              </li>
               <?php
               ?>
               <li @if (Request::segment(1) == 'convocari' && Request::segment(2) == '') class="active" @endif>
                  <a href="{{URL::to('convocari')}}">Convocari</a>
               </li>

            @endif

            <li class="nav-header"><a class="nav-header" href="{{URL::to('sports')}}">Sporturi</a></li>

            @foreach($sports as $sport)

               <li @if (Request::segment(2) == $sport['name'] && !Request::segment(3)) class="active" @endif>
                  <a href="{{URL::to('sports/' . $sport['name'])}}">{{$sport['name']}}
                  </a>

               @if ($sport['name'] == Request::segment(2))
                  <ul class="level0">
                  @foreach ($sport['subcategorii'] as $sub)
                     <li @if (Request::segment(2) == $sport['name'] && Request::segment(3) == $sub["name"]) class="active level1 " @else class="level1" @endif>
                     <a href="{{URL::to('sports/' . $sport['name'] . '/' . $sub['name'])}}">
                        {{$sub['name']}}{{-- ({{$sub['number']}} --}}
                     </a>
                     </li>
                  @endforeach
                  </ul>
               @endif

               </li><!-- end li main category -->

            @endforeach
            <li class="nav-header">Locatii</li>
            <li @if (Request::segment(1) == 'locatii' && Request::segment(2) == '') class="active" @endif>
               <a href="{{route('locatii.index')}}">Lista locatii</a>
            </li>

            @if (is_admin())
               <li class="nav-header">Admin</li>
               <li>
                  <ul class="level0">
                  </ul>
               </li>
               <li @if (Request::segment(1) == 'locatii' && Request::segment(2) == 'create') class="active level1 " @else class="level1" @endif>
                  <a href="{{route('locatii.create')}}">Creeaza locatie</a>
               </li>

               <li @if (Request::segment(1) == 'sports' && Request::segment(2) == 'add') class="active level1 " @else class="level1" @endif>
                  <a href="{{URL::to('sports/add')}}">Adauga sport</a>
               </li>
            @endif
         </ul> <!-- menu -->
      </div><!--/.well -->
   </div><!--/span-->