<div class="panel panel-default">
   <div class="panel-heading comment-counter">
   @if (isset($noComments))
      @if ($comments && count($comments['comments']) == 1)
         <span class="comment-counter-value">{{count($comments['comments'])}}</span> <span class="comment-counter-text">Comentariu</span>
      @else
         <span class="comment-counter-value">{{count($comments['comments'])}}</span> <span class="comment-counter-text">Comentarii</span>
      @endif
   @else
      Comentarii
   @endif
   </div>


<div id="newcomment" class="panel-body">
   {{-- new comment errors and success messages on refreshing page --}}
   @if (Session::has('flash_comment'))
      @if (Session::get('flash_comment')['error'])
      <div class="alert alert-error">
      @else
      <div class="alert alert-success">
      @endif

      @if (is_array(Session::get('flash_comment')['msg']))
         @foreach( Session::get('flash_comment')['msg'] as $message )
            <button data-dismiss="alert" class="close" type="button">×</button>
            {{$message}}
         @endforeach
      @else
         <button data-dismiss="alert" class="close" type="button">×</button>
         {{Session::get('flash_comment')['msg']}}
      @endif
      </div>
   @endif


   @if(!is_logged())
      {{ Form::open(array('url' => 'comments'), 'POST') }}
      {{ Form::hidden('url', (isset($commentURL) ? $commentURL : Request::path())); }}
   @endif

   <div class="comment-entry" id="comment-0">
      <div class="comment-msg"></div>
      {{ Form::textarea('comment', Input::old('comment'), [
      'id' => 'gcomment',
      'class' => 'comment-new w400',
      'placeholder' => 'Lasa un mesaj...']) }}

      @if(!is_logged())
         <div class="comment captcha">{{$captcha->html()}}</div>
      @endif

      <div>
         <a href="javascript:void(0);" class="btn comment-trimite">Trimite</a>
         <span class="comment loading bhidden"><img src="{{URL::to('img/ajax-loader.gif')}}"/></span>
      </div>
      {{ Form::close() }}
      <div class="separator"></div>
   </div>


   <div class="panel-body comments-list">

      {{showComments(
         $comments['tree'],
         $comments['tree'],
         '<div class="comment-header">
            comment-header
         </div>
         <div class="comment-body">
            comment-body
         </div>
         <div class="comment-footer">
            comment-footer
         </div>',
         '<div class="comment-child">comment-child</div>',
         $comments['indexed'])}}

   @if (isset($noComments)
      && $noComments
      && isset($comments['indexed'])
      && count($comments['indexed'])
      && $noComments > count($comments['indexed']))

   <div class="separator"></div>
   <a href="javascript:void(0);" class="comment-more">Mai multe comentarii</a>

   @endif
   </div>

</div> {{-- newcomment --}}

</div> {{-- panel --}}

<script type="text/javascript">
$(function() {

   $.hideMsg = function($obj) {
      $obj.find('.comment-msg-error').hide();
      $obj.find('.comment-msg-success').hide();
   };

   $.showErr = function($obj, msg) {
      $.showMsg($obj, valid = false, msg);
   }

   $.showSucc = function($obj, msg) {
      $.showMsg($obj, valid = true, msg);
   }


   $.showMsg = function($obj, valid, msg) {
      valid = valid || false;

      var error = '<div class="comment-msg-error alert alert-error bhidden" style="margin-bottom: 0;">\
            <button data-dismiss="alert" class="close" type="button">×</button>\
            <span class="comment-msg-error"></span>\
         </div>';

      var succ = '<div class="comment-msg-success alert alert-success bhidden" style="margin-bottom: 0;">\
            <button data-dismiss="alert" class="close" type="button">×</button>\
            <span class="comment-msg-error"></span>\
         </div>';


      if (!valid) {
         if (!$obj.find('.comment-msg-error').size()) {
            $(error).appendTo($obj);
         }
         $obj.find('span.comment-msg-error').html(msg);
         $obj.find('.comment-msg-error').show();
      }
      else {
         if (!$obj.find('.comment-msg-succes').size()) {
            $(succ).appendTo($obj);
         }
         $obj.find('span.comment-msg-succes').html(msg);
         $obj.find('.comment-msg-succes').show();
      }

   };

   $(document).on('click', '.comment-trimite', function() {
      var $that = $(this),
         //$comment = $that.siblings('.comment-new'),
         $comment = $that.parents('.comment-entry').first().find('.comment-new:visible'),
         comment = $comment.val(),
         $commentId = $that.parents('.comment-entry').first(),
         commentId = 0;

      if (($commentId).size()) {
         commentId = parseInt($commentId.attr('id').split('-')[1], 10);
      }

      $.hideMsg($that.parents('.comment-entry').first().find('.comment-msg'));

      if (!$.defaultsLoggedId) {
         var $captcha = $that.parents('.comment-entry').first().find('.comment.captcha').find('input:visible');
      }

      if (!comment) {
         $.showErr($that.parents('.comment-entry').first().find('.comment-msg'), 'Nu ai scris nimic');

         return false;
      }

      if (typeof $captcha != 'undefined' && $captcha.val() == '') {
         /*$that.parents('.comment-entry').first().find('span.comment-msg-error').html('Nu ai completat captcha').   parent().show();
         */
         $.showErr($that.parents('.comment-entry').first().find('.comment-msg'), 'Nu ai completat captcha');
         return false;
      }

      if (!$.defaultsLoggedId) {
         $that.parents('form').submit();
      }


      $.ajax({
         url: '{{URL::action('CommentsController@store')}}',
         type: 'POST',
         data: {
            "comment": comment,
            "reply": commentId,
            "url": "{{(isset($commentURL) ? $commentURL : Request::path())}}",
            "recaptcha_response_field": $('#recaptcha_response_field').val()
         },
         dataType: 'json',
         beforeSend: function() {
            $.hideMsg($that.parents('.comment-entry').first().find('.comment-msg'));

            $that.parents('.comment-entry').first().find('.comment.loading').show();
            $that.hide();
         }
      })

      .done(function(data) {

         $that.parents('.comment-entry').first().find('.comment.loading').hide();
         $that.show();
         if (!$.defaultsLoggedId) {
            //location.reload();
            $('#recaptcha_reload').click();
         }

         if (data && data.error) {
            if (data.msg && typeof data.msg == 'object') {
               data.msg.join('<br />');
            }
            $.showErr($that.parents('.comment-entry').first().find('.comment-msg'), data.msg);

            return false;
         }

         $comment.val('');

         $.showSucc($that.parents('.comment-entry').first().find('.comment-msg'), data.msg);

         var $newcomment = data.comment;

         var str = commentId ? '<div class="comment-child">' : '';

         str += '\
         <div id="comment-' + $newcomment.id + '" class="comment-entry"> \
            <div class="comment-header"> \
               <a name="comment-' + $newcomment.id + '"></a> \
               @if (is_logged()){{a_profile(Auth::user())}}@else \
                  anonim @endif \
               <span class="faded">' + $newcomment.created_at + '</span> \
            </div> \
            <div class="comment-body"> \
               <p>' + $newcomment.comment +'</p> \
            </div> \
            <div class="comment-footer">\
               <div class="comment-msg"></div>\
               <div class="comment-reply-body bhidden">\
               {{Form::textarea('comment', null, [
               'class' => 'comment-reply-entry comment-new w400',
               'placeholder' => 'Raspunde...'])}} \
               <div>\
                  <a href="javascript:void(0);" class="btn comment-trimite">Raspunde</a>\
                  <span class="comment loading bhidden"><img src="{{URL::to('img/ajax-loader.gif')}}"/></span>\
                  <a href="javascript:void(0);" class="btn comment-cancel">Anuleaza</a>\
               </div>\
               </div>\
               @if (is_logged())
                  <a href="javascript:void(0);" class="faded comment-reply">raspunde</a>\
               @endif
            </div>\
         </div>';

         if (commentId) {
            //find all children and append
            $commentId.append(str);
            $commentId.find('.comment-reply-body').hide();
         }
         else {
            $('.comments-list').append(str);
            window.location.hash = '#comment-' + $newcomment.id;
         }

         {{-- updating number of comments counter --}}
         var $commentCounter = $('.comment-counter-value');
         if ($commentCounter.html() == '0') {
            $commentCounter.html('1');
            $('.comment-counter-text').html('Comentariu');
         }
         else {
            $commentCounter.html(1 + parseInt($commentCounter.html(), 10));
            $('.comment-counter-text').html('Comentarii');
         }
         $that.parents('.comment-entry').first().find('.comment-reply').show();




      });
   });

   $(document).on('click', '.comment-reply', function() {
      $.hideMsg($(this).parents('.comment-entry').find('.comment-msg'));

      $(this).siblings('.comment-reply-body').show();
      $(this).hide();
   });

   $(document).on('click', '.comment-cancel', function() {
      $(this).parents('.comment-reply-body').first().hide();
      $(this).parents('.comment-entry').first().find('.comment-reply').show();
   });

   $(document).on('click', '.comment-more', function() {
      //@todo
   });
});
</script>