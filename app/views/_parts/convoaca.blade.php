<script type="text/javascript">
$(function() {
   $('.ical').click(function() {
      var id = $(this).attr('id').split('_')[1];
      $( "#datepicker_" + id ).trigger('click');
   });

   $('.tohide').click(function() {
      $('#challenge_' + $(this).attr('id').split('_')[1]).hide();
   });

   //@todo remove it
   // $('.go').trigger('click');
   $('.alte_sporturi, .ascunde_alte_sporturi').click(function() {
      $(this).siblings('div.hidden, a.alte_sporturi, a.ascunde_alte_sporturi').toggle();
      $(this).hide();
   });

   $(document).on('click', '.convoacack', null, function(){
      if ($(this).is(':checked')) {
         $(this).parents('div.convoaca').find('.notactivego').removeClass('notactivego').addClass('go');
      }
      else {
         if ($(this).parent().siblings().find('.convoacack').filter(':checked').length == 0) {
            $(this).parents('div.convoaca').find('.go').removeClass('go').addClass('notactivego');
         }
      }
   });

   $('.go').click(function() {
      if (!$.defaultsLoggedId) {return confirm('Trebuie sa fii logat pentru a putea convoca un utilizator. \r\nMergi la pagina de login?');}
      //var sportId = $(this).parents('div').attr('class').match(/[0-9]+/)[0];
      var sportId = <?php echo isset($sport) && isset($sport->id) ? $sport->id : 0?>;

      if (typeof sportId == 'undefined') { return false}

      var $that = $(this);
      var $convoaca = $that.siblings('.convoaca');
      var uidConv = $(this).parents('.userlist').attr('id').split('_')[1]

      if ($(this).html() == 'Convoaca') {
         $(this).html('X');

         var load = loader();
         $.ajax({
            url: '{{URL::to('/eventsapi/events/')}}' + '/' + sportId,
            ifModified: true,
            beforeSend: function() {
               //show loader
               $convoaca.toggle();

               if (typeof $.getEvents == 'undefined') {
                  $.getEvents = {}
               }
               $.getEvents[load[1]] = 1;
               $convoaca.html(load[0]);
            },
            success: function(pdata) {
               $('#' + load[1]).remove();
               if (pdata == null || typeof pdata.content == 'undefined' || pdata.content == null) {
                  $convoaca.html('Nu au putut fi procesate evenimentele');
                  return false;
               }

               if (pdata.error) {
                  $convoaca.html(pdata.content);
                  return false;
               }

               str = 'Evenimente<br />';
               for (var i = 0; i < pdata.content.length; i++) {

                  bgmode = '#F5F5F5';
                  /*
                  if (i % 2 != 0) {
                     bgmode = '#88bbff';
                  }
                  */
                  found = false;
                  if (pdata.convocari && pdata.convocari[uidConv]) {
                     if ($.inArray(pdata.content[i].id, pdata.convocari[uidConv]) != -1) {
                        found = true;
                     }
                  }

                  str += '<div style="margin:4px 0; border-bottom:1px solid '+bgmode+'">';

                  if (!found) {
                     str += '<input type="checkbox" class="convoacack" name="convoacack_'+pdata.content[i].id+'" id="convoacack_'+pdata.content[i].id+'">';
                  }
                  str += ' ';
                  str += pdata.content[i].event;
                  if (!sportId) {
                     str += '(' + pdata.content[i].sport + ')';
                  }
                  str += ', ';
                  str += pdata.content[i].jsdate;
                  str += ' Ora: ';
                  str += pdata.content[i].ora;
                  str += ' ';

                  //@todo check if it's convocari
                  if (found) {
                     str += ' - Convocat';
                  }
                  else {
                     //str += '<span class="spanconvoaca"><input type="checkbox" class="convoacack" name="convoacack_'+pdata.content[i].id+'" id="convoacack_'+pdata.content[i].id+'"></span>';
                  }


                  str += '</div>';
               }

               //se face activ doar daca a selectat un eveniment pentru convocare.
               str += '<div><a href="javascript:;" class="notactivego goajax btn btn-success btn-small">Convoaca</a></div>';

               $convoaca.html(str);

               //$('.convoacack').trigger('click');
            },
            error: function() {
               $convoaca.html('Nu au putut fi procesate evenimentele');
            }

         });
      }
      else {
         $(this).html('Convoaca');
         $(this).siblings('.convoaca').toggle();
      }
   });

   $(document).on('click', '.go.goajax', null, function(){
      var load = loader();
      var $convoaca = $(this).parent();
      var bk = $convoaca.html();
      //#userid, events ids,
      var refCk = $convoaca.parent().find('.convoacack').filter(':checked');
      //id-uri convocari
      var ids = $.map(refCk, function(elem) {return $(elem).attr('id').split('_')[1]});

      var uidConv = $(this).parents('.userlist').attr('id').split('_')[1];

      $.ajax({
         url: '{{URL::to('/convocariapi/add')}}',
         type: 'post',
         data: {uid: uidConv, events: ids},
         ifModified: true,
         beforeSend: function() {
            //show loader
            if (typeof $.getEvents == 'undefined') {
               $.getEvents = {};
            }
            $.getEvents[load[1]] = 1;
            $convoaca.html(load[0]);
         },
         success: function(pdata) {
            $('#' + load[1]).remove();
            if (pdata == null || typeof pdata.content == 'undefined' || pdata.content == null) {
               $convoaca.html('Nu au putut fi procesate convocarile');
               return false;
            }

            if (pdata.error) {
               $convoaca.html(pdata.content);
               return false;
            }

            refCk.each(function(){
               var $p = $(this).parent();
               $p.find('input[type="checkbox"]').remove();
               $p.html($p.html() + (' - Convocat'));
            });

            bk = $(bk).removeClass('go').addClass('notactivego');
            $convoaca.html(bk);
         },
         error: function() {
            $convoaca.html('Nu au putut fi procesate convocarile <br />' + bk);
         }
      });
   });

   //$($('.go')[0]).trigger('click');
});
</script>