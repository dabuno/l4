@if (Session::has('flash_error'))
   <div class="alert alert-error">
      <button data-dismiss="alert" class="close" type="button">×</button>
      {{ Session::get('flash_error') }}
   </div>
@endif

@if ($errors->all())
   <div class="alert alert-error">
      <button data-dismiss="alert" class="close" type="button">×</button>
   @foreach( $errors->all() as $message )   
      {{$message}}   
      <br />
@endforeach
</div>
@endif

@if (Session::has('message'))
<div class="alert alert-success">
   <button data-dismiss="alert" class="close" type="button">×</button>
   {{Session::get('message')}}
</div>
@endif