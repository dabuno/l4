@extends('master')


@section('content')

@section('breadcrumb')
   <li>
      <a href="{{URL::to("profile")}}">Profil</a>
      <span class="divider">/</span>
   </li>
   <li class="active">Notificari</li>
@stop

{{ Form::open([
   'class' => "form-horizontal well",
   'method' => 'post']) }}
<fieldset>
   <legend>Setari notificari</legend>
   Vreau sa primesc notificari(pe email) atunci cand:
   <div class="control-group">
   <label class="checkbox">
    {{Form::checkbox('suntconvocat', 1, Input::old('suntconvocat') ? Input::old('suntconvocat') : $notificari->suntconvocat)}} cineva ma convoaca
   </label>
   {{-- <br />
   <label class="checkbox">
      {{Form::checkbox('evenimentnou', 1, Input::old('evenimentnou') ? Input::old('evenimentnou') : $notificari->evenimentnou)}} cineva creeaza un eveniment pentru sporturile [[pre]select] in orasul [[pre]select] in locatiile [[pre][select]]
   </label> --}}
   <br />
   <label class="checkbox">
      {{Form::checkbox('comentariuprofil', 1, Input::old('comentariuprofil') ? Input::old('comentariuprofil') : $notificari->comentariuprofil)}} cineva a comentat pe profilul meu</label>
   <br />
   <label class="checkbox">
      {{Form::checkbox('comentariueveniment', 1, Input::old('comentariueveniment') ? Input::old('comentariueveniment') : $notificari->comentariueveniment)}} cineva a comentat pe evenimentul meu</label>
   {{-- <br />
   <label class="checkbox">
      {{Form::checkbox('competitienoua', 1, Input::old('competitienoua') ? Input::old('competitienoua') : $notificari->competitienoua)}} cineva organizeaza o competitie</label>
   --}}
   <br />

   <label class="checkbox">
      {{Form::checkbox('functionalitatenoua', 1, Input::old('functionalitatenoua') ? Input::old('functionalitatenoua') : $notificari->functionalitatenoua)}} vreausajoc.ro adauga functionalitati noi</label>
   <br />
   </div>

  <div class="form-actions">
   {{Form::submit('Modifica', array('class' => "btn btn-primary"))}}
   &nbsp;
   {{Form::reset('Reset', array('class' => 'btn'))}}
  </div>
</fieldset>
{{Form::close()}}

<script type="text/javascript"></script>

@stop