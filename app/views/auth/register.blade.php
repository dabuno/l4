@extends('master')

@section('head')
   @parent
   <style type="text/css">
   body {
   padding-top: 40px;
   padding-bottom: 40px;
   background-color: #fff;
   }

   .form-signin {
   max-width: 500px;
   padding: 19px 29px 29px;
   margin: 0 auto 20px;
   background-color: #f5f5f5;
   border: 1px solid #e5e5e5;
   -webkit-border-radius: 5px;
      -moz-border-radius: 5px;
           border-radius: 5px;
   -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
      -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           box-shadow: 0 1px 2px rgba(0,0,0,.05);
           line-height: 12px;
   }
   .form-signin .form-signin-heading,
   .form-signin .checkbox {
   margin-bottom: 10px;
   }
   .form-signin input[type="text"],
   .form-signin input[type="password"] {
   margin-bottom: 15px;
   }
   iframe[src="about:blank"] {
  display:none;
}
   </style>
@stop

@section('topmenu')
@stop

@section('leftmenu')
@stop

@section('content')

{{ Form::open(array('class' => "form-signin")) }}
   <h2 class="form-signin-heading">
   Inregistrare - <a class="brand" href="{{URL::to('/')}}">vreausajoc</a>
   </h2>

   <p>Ai cont? Mergi la pagina de <a href="{{URL::to('/login')}}">Login</a></p>

@include('_parts.errors')

   {{Form::text('first_name', Input::old('first_name'), array('class'=>"span3", 'placeholder'=>"Prenume"))}}
    <br />
   {{Form::text('last_name', Input::old('last_name'), array('class'=>"span3", 'placeholder'=>"Nume"))}}
    <br />
    <div class="input-append">
        {{Form::text('email', Input::old('email'), array('class'=>"span3", 'placeholder'=>"Adresa E-mail"))}}
        <span class="add-on">
           <i class="icon-envelope"></i>
        </span>
    </div>

    <div class="input-append">
        {{Form::password('password', array('autocomplete' => 'off','class'=>"span3", 'placeholder'=>"Parola cont Vreausajoc"))}}
         <span class="add-on">
            <i class="icon-lock"></i>
         </span>
   </div>

   <p class="bold">Gen</p>
   <div class="controls">
       {{Form::select('gen', $gen, Input::old('gen'))}}

    </div>

   <p class="bold">
      Vreau sa joc<br />
      {{Form::select('sport', $sports)}}
      <?php
      //echo form_simple_dropdown('sport', $sports, 'id', 'name', array('id'=>'0', 'name'=>'Alege'), set_value('sport'));
      ?>
   </p>

   Cod verificare

  {{$captcha->html()}}

   <?php //echo recaptcha_get_html($publickey); ?>

   <br />

<div>
   {{Form::submit('Creeaza user', array('class' => "btn btn-primary"))}}
</div>

{{ Form::close() }}

@stop