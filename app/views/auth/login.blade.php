@extends('master')

@section('topmenu')
@show

@section('leftmenu')
@show

@section('content')

<style type="text/css">
body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #fff;
}

.form-signin {
  max-width: 330px;
  padding: 19px 29px 29px;
  margin: 0 auto 20px;
  background-color: #f5f5f5;
  border: 1px solid #e5e5e5;
  -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
          border-radius: 5px;
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
     -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
          box-shadow: 0 1px 2px rgba(0,0,0,.05);
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin input[type="text"],
.form-signin input[type="password"] {
  margin-bottom: 15px;
}
</style>

{{ Form::open(array('class' => "form-signin")) }}
<h2 class="form-signin-heading">
   Login - <a class="brand" href="{{URL::to('/')}}">vreausajoc</a>
</h2>
<p>
   <span>Nu ai cont?</span> <a href="{{URL::to('/register')}}">Creeaza-ti cont acum!</a>
</p>

@include('_parts.errors')

<div class="input-prepend">
   <span class="add-on">
      <i class="icon-envelope"></i>
   </span>
   {{Form::text('email', $email, array('class'=>"span3", 'placeholder'=>"Adresa E-mail"))}}
</div>

<div class="input-prepend">
   <span class="add-on">
      <i class="icon-lock"></i>
   </span>
   {{Form::password('password', array('autocomplete' => 'off', 'class'=>"span3", 'placeholder'=>"Parola"))}}
</div>
<label class="checkbox">
   <span class="bold">{{Form::checkbox('remember')}} Tine-ma minte!</span>
   <a class="pull-right" href="{{URL::to('/forgot')}}">Parola uitata?</a>
</label>
<div>
   {{Form::submit('Login', array('class' => "btn btn-primary"))}}
</div>

{{ Form::close() }}

@stop