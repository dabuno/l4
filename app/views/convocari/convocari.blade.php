@extends('master')

@section('breadcrumb')
   <li class="active">Lista convocari</li>
@stop

@section('content')

<table class="table table-hover table-striped" style="table-layout: fixed;">
   <tr>
      <th style="width: 110px">
         Organizator {{HTML::image('/img/arrow-top-left.png', 'Ai convocat', array('title' => 'Cine a organizat'));}}
      </th>
      <th style="width: 110px">
         Convocat {{HTML::image('/img/arrow-bottom-right.png', 'Ai convocat', array("style" => "margin-top: -2px",'title' => 'Cine este convocat'));}}
      </th>
      <th style="width: 150px">Data</th>
      <th>Eveniment</th>
      <th>Status</th>
      <th>Actiuni</th>
   </tr>

@foreach($convocari as $convocare)
   @if(!$convocare->events)
      @continue
   @endif
   <tr>
      <td>
         {{--
         @if ($convocare->src_id == is_logged())
            {{HTML::image('/img/arrow-top-left.png', 'Ai convocat', array('title' => 'Ai convocat'));}}
         @else
            {{HTML::image('/img/arrow-bottom-right.png', 'Ai convocat', array('title' => 'Ai fost convocat'));}}
         @endif
         --}}
      <?php //td for Organizator
      ?>
         {{a_profile($convocare->srcids);}}
      </td>

      <?php //td for Convocat?>
      <td>
         {{a_profile($convocare->destids);}}
      </td>

      <?php
      $expired = false;

      if ($convocare->events->data < date('Y-m-d')) {
          $expired = true;
      }
      ?>

      <?php //td for Data?>
      <td>
         @if ($expired)
            <strike>
         @endif
         {{date2js($convocare->events->data)}}
         <br />
         ora
         {{date('H:i', strtotime($convocare->events->ora))}}
         @if ($expired)
            </strike>
         @endif
      </td>

      <?php //td for Eveniment Name?>
      <td>
         <a href="{{route('events.show' , $convocare->events->id)}}">{{$convocare->events->event}}</a>
      </td>

      <?php //td for convocare status

      ?>
      <td>
         <?php

         $status = 'Activa';

         if ($convocare->src_status == CONVOCARE_SRC_ACTIVA
            && $convocare->dest_status == CONVOCARE_DEST_ACTIVA) {
            $status = 'Activa'; //just for future overwritten here
         }

         if ($convocare->dest_status == CONVOCARE_DEST_ACCEPTATA) {
            $status = 'Acceptata';
         }

         //Activa / Expirata / Anulata
         if ($convocare->src_status == CONVOCARE_DEST_ANULATA) {
            $status = 'Anulata';
            $expired = true;
         }

         //If the logged person is on dest_id
         if ($convocare->dest_id == is_logged()) {
            if (CONVOCARE_DEST_REFUZATA == $convocare->dest_status) {
               $status = 'Refuzata';
               $expired = true;
            }

            if (($convocare->dest_status == CONVOCARE_DEST_ACTIVA || $convocare->dest_status == CONVOCARE_DEST_ACCEPTATA) && ($convocare->src_status == CONVOCARE_DEST_ANULATA || $convocare->src_status == CONVOCARE_SRC_STEARSA)) {
               $status = 'Anulata';
               $expired = true;
            }

            //valid state from db. on cancels
            if (CONVOCARE_DEST_ANULATA == $convocare->dest_status) {
               $status = 'Anulata';
               $expired = true;
            }

         }

         //If the logged person is on src_id
         if ($convocare->src_id == is_logged()) {
            if (CONVOCARE_SRC_ACTIVA == $convocare->src_status && ($convocare->dest_status == CONVOCARE_DEST_REFUZATA || $convocare->dest_status == CONVOCARE_DEST_STEARSA))
            {
               $status = 'Refuzata';
               $expired = true;
            }

            //valid state from db. on cancels
            if (CONVOCARE_SRC_REFUZATA == $convocare->dest_status) {
               $status = 'Refuzata';
               $expired = true;
            }
         }


         if ($expired) {
            if ($status == 'Acceptata') {
               $status = 'Jucatata';
            }
            if ($status == 'Activa') {
               $status = 'Expirata';
            }

         }
         echo $status;

         ?>
      </td>

      <?php //td for Actiuni?>
      <td><?php
         if (!$expired && $convocare->src_id == is_logged()) {
            echo '<a href="'.URL::to('convocariapi/cancels/' . $convocare->id ).'">Anuleaza</a>';
         }
         elseif (!$expired) {

            if ($convocare->dest_status == CONVOCARE_DEST_ACCEPTATA) {
               echo ' - ';
            }

            else {
               echo '<a href="'. URL::to('convocariapi/accepta/' . $convocare->id ) .'">Accept</a>';
               echo ' | ';
               echo '<a href="'. URL::to('convocariapi/refuza/' . $convocare->id ) .'">Refuz</a>';
            }
         }
         else {
            //delete req by destination
            if ($convocare->dest_id == is_logged()) {
               echo '<a href="'.URL::to('convocari/deleted/' . $convocare->id ) .'">Sterge</a>';
            }
            //delete req by originator
            else {
               echo '<a href="'.URL::to('convocari/deletes/' . $convocare->id ) .'">Sterge</a>';
            }
         }
         ?>
      </td>

   </tr>

@endforeach

</table>

{{$convocari->links()}}

@stop