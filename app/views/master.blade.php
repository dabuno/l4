<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
@section('metadesc')
<?php
if(isset($pageDescription)) {
   echo $pageDescription;
}
else {
   echo 'Site pentru cei vor sa faca sport. Organizare evenimente sportive.';
}


?>
@stop
@section('head')
    <meta charset="UTF-8">
    <title>@if(isset($pageTitle)){{$pageTitle}} - @endif vreausajoc.ro</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="@yield('metadesc')">
    <meta name="keywords" content="vreausajoc.ro, vreau sa fac sport, vreau sa joc, evenimente sportive"/>
    <meta name="author" content="vreausajoc.ro">

   {{-- using reddint style of font
   @if (app()->environment() != 'local')
   <style>
     @import url(https://fonts.googleapis.com/css?family=Ubuntu);
   </style>
   @endif
   --}}

    {{ Html::style('css/bootstrap-utd.css') }}
    {{ Html::style('css/css.css') }}
    {{ Html::style('css/panel.css') }}

    {{ Html::style('css/bootstrap-responsive.min.css') }}
    {{ Html::style('css/css-resp.css') }}

    {{ Html::script('js/jquery-1.9.0.min.js') }}
    {{ Html::script('js/bootstrap.js') }}

    {{ Html::script('js/vsj.js') }}

    <link rel="shortcut icon" href="/img/ico/favicon.ico">
    {{--
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/img/ico/apple-touch-icon-57-precomposed.png">
    --}}


   @if (app()->environment() != 'local')
      {{-- javascript script tag included --}}
      @include('_parts.clicktale_start')
   @endif

   <script type="text/javascript">

   $(function() {
      $.defaultsLoggedId = '<?php echo (Auth::user()) ? Auth::user()->id : ""?>';
   });



   function loader() {
      var id = 'loader_' + (+new Date());
      return ['<div id="' + id + '"><img style="padding:2px;vertical-align:middle" src="/img/ajax-loader.gif>se incarca</div>', id];
   }
   function ci() {
      console.info(arguments);
   }

   </script>
@show
</head>

<body class="preview" data-spy="scroll" data-target=".subnav" data-offset="80">

@section('topmenu')
<div class="navbar navbar-fixed-top">
<div class="navbar-inner">
 <div class="container">
    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </a>
    <a class="brand" href="{{URL::to('/')}}">vreausajoc</a>
    <div class="nav-collapse collapse">
      <ul class="nav pull-right">
         @if(Auth::check())
            <li><a href="{{URL::to('profile')}}"><i class="icon-user"></i> {{Auth::user()->alias}}</a></li>
            <li><a href="{{URL::to('logout')}}"><i class="icon-off"></i> Logout</a></li>
         @else
            <li><a href="{{URL::to('register')}}">Inregistrare</a></li>
            <li><a href="{{URL::to('login')}}">Login</a></li>
         @endif
      </ul>
    </div><!--/.nav-collapse -->
  </div>
 </div>
</div><!-- navbar navbar-fixed-top -->
@stop

@section('leftmenu')
   <div class="span3">
      <div class="well sidebar-nav">
         <ul class="nav nav-list">

            @if (Auth::check())
               <li class="nav-header">Contul meu</li>

               <li @if (Request::segment(1) == 'profile' && Request::segment(2) == '') class="active" @endif>
                  <a href="{{URL::to('profile')}}">Profil</a>

                  @if (Request::segment(1) == 'profile' && (Request::segment(2) == '' || Request::segment(3) == 'edit'))
                  <ul class="level0">
                     <li @if (Request::segment(3) == 'edit') class="active level1" @else class="level1" @endif>
                     <a href="{{route('profile.edit', $user->id)}}">Editeaza profil</a></li>
                     </li>
                  </ul>
                  @endif
               </li>


               <li @if (Request::segment(1) == 'events' && Request::segment(2) == '') class="active" @endif>
                  <a href="{{URL::to('events')}}">Evenimente</a>

                  @if (Request::segment(1) == 'events' && (Request::segment(2) == '' || Request::segment(3) == 'create'))
                  <ul class="level0">
                     <li @if (Request::segment(3) == 'create')  class="active level1 " @else 'class="level1"' @endif>
                        <a href="{{URL::to('events/create')}}">Creeaza eveniment</a>
                     </li>
                  </ul>
                  @endif
              </li>

               <li @if (Request::segment(1) == 'convocari' && Request::segment(2) == '') 'class="active"' @endif>
                  <a href="{{URL::to('convocari')}}">Convocari</a>
               </li>

            @endif


            <li class="nav-header">Admin</li>
            <li>
               <ul class="level0">
               </ul>
            </li>
            <li><a href="#">Link</a></li>
         </ul> <!-- menu -->
      </div><!--/.well -->
   </div><!--/span-->
@stop

@yield('topmenu')

@section('container')
   <div class="container">
      <div class="row">

      @if (Request::segment(1) != 'login' && Request::segment(1) != 'register' && Request::segment(1) != 'logout' && Request::segment(1) != 'forgot')
         @include('_parts.menu')

         <div class="span9">
            <ul class="breadcrumb">
               @section('breadcrumbs')
                  <li>
                     <a href="{{URL::to("/")}}">{{Config::get('sitesettings.breadsitename')}}</a>
                     <span class="divider">/</span>
                  </li>
                  @yield('breadcrumb')
               @show
            </ul>

         @include('_parts.errors')

         @yield('content')

         </div> <!-- div class="span9" -->

      @else
         @yield('content')
      @endif


      </div><!-- /row in center container-->
@show

@section('footer')
<footer>
<hr>
<div>
   <div class="pull-left">&copy; {{Config::get('sitesettings.sitename', 'vreausajoc.ro');}} {{date('Y')}}</div>
   <div class="pull-right">
      <a href="{{URL::to('contact')}}">Contact</a>
   </div>
</div>

@if (app()->environment() != 'local')
   @include('_parts.analytics')
@endif
</footer>
</div><!--/.container </div> in footer end-->

   @if (app()->environment() != 'local')
      {{-- javascript script tag included --}}
      @include('_parts.clicktale_end')
   @endif

</body>
</html>
@show