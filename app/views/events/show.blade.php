@extends('master')

@section('head')
   @parent
   {{ Html::style('css/jquery-ui-1.10.0.custom.min.css') }}
   {{ Html::script('js/jquery-ui-1.10.0.custom.min.js') }}
@stop

@section('content')


@section('breadcrumb')
   <li class="active">
      <a href="{{route('events.index')}}">Evenimente</a>
      <span class="divider">/</span>
   </li>

   <li class="active">
      {{$event->event}}
   </li>
@stop


<div class="panel panel-default">
   <div class="panel-heading comment-counter">
      <h4>{{$event->event}}
         <small>eveniment {{$event->sports->name}}, organizat de {{a_profile($event->users)}}
      </h4>
   </div>

   <div class="panel-body">

   <address>
      <strong>
         <div class="pull-right">
            <div class="panel panel-default">
              <div class="panel-heading">
                  <div class="tc">Data eveniment</div>
               </div>
               <div class="panel-body">
                  <div id="datepicker"></div>
               </div>
            </div>
         </div>
         <a href="{{URL::to('sports/' . $event->sports->name)}}">{{$event->sports->name}}</a>:
         {{date2js($event->data);}}
       ora
         {{time2js($event->ora);}}

      </strong>
   <br />


   Locatia:
      {{a_locatie($event->arene)}},
      <span>{{ucfirst(strtolower($event->orase->oras))}}</span>
      {{-- {{$event->arene->orase->name}} --}}
   <br />
   </address>

   <address>
     Organizator:
      {{a_profile($event->users)}}
   </address>

   Pret de persoana:
   <?php
   echo $event->pret;
   ?> Lei
   <br />
   Locuri disponibile:
   <?php
   echo $event->locuri;
   ?>

   <?php
      $notIn = 0;
   ?>
   @if (count($participanti))
   <br /> Lista participanti<br />
   <ul>
   @foreach($participanti as $participant)
   <?php
   $notIn = 0;
   if (is_logged() == $participant->destids->id) {
      $notIn = 1;
   }
   ?>
     <li>{{a_profile($participant->destids)}}</li>
   @endforeach
   </ul>
   @endif

   <br /><br />
   @if ((!is_logged() || $event->user_id != Auth::user()->id) && $event->locuri && !$notIn)
      <div class="holder">
         Esti intersat de acest eveniment?
            <a id="event_{{$event->id}}" class="btn btn-success btn-small vreausajoc" href="javascript:;">
               Vreau sa joc!
            </a>
      </div>

      <br /><br />
      @include('_parts.vreausajoc')
   @endif

<div class="clearfix"></div>
<br />

@include('_parts.comments')
   </div>
</div>

@stop

@section('footer')
   @parent
   <script>
   $(function() {
      $( "#datepicker" ).datepicker({
         dateFormat: "yy-mm-dd"
      });
      $( "#datepicker" ).datepicker("setDate", "{{$event->data}}");
      $( "#datepicker a" ).removeClass('ui-state-highlight');
      $( "#datepicker a.ui-state-active" ).addClass('ui-state-highlight');
   });
   </script>
@stop

