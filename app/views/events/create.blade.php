@extends('master')

@section('head')
   @parent
   {{ Html::style('css/jquery-ui-1.10.0.custom.min.css') }}
   {{ Html::script('js/jquery-ui-1.10.0.custom.min.js') }}
   {{ Html::script('js/jquery-ui-timepicker-addon.js') }}


   {{ Html::style('css/chosen/chosen.css') }}
   {{ Html::script('js/chosen/chosen.jquery.js') }}

    <script type="text/javascript">
       $(function() {
          $.datepicker.setDefaults({
             dateFormat: 'DD, d MM yy',
             dayNames: ['Duminica', 'Luni', 'Marti', 'Miercuri', 'Joi', 'Vineri', 'Sambata'],
             monthNames: ['Ianuarie','Februarie','Martie','Aprilie','Mai','Iunie','Iulie','August','Septembrie','Octombrie','Noiembrie','Decemberie']
          });
       });
    </script>

@stop

@section('breadcrumb')
   <li class="active">
      <a href="{{route('events.index')}}">Evenimente</a>
      <span class="divider">/</span>
   </li>

   <li class="active">
      @if ($event->id) Editeaza eveniment @else Creeaza eveniment @endif
   </li>
@stop

@section('content')


@if ($event->id)
{{ Form::open(array(
   'method' => 'put',
   'route' => array('events.update', $event->id),
   'class' => "form-horizontal well")) }}
@else
{{ Form::open(array(
   'route' => array('events.store'),
   'class' => "form-horizontal well")) }}
@endif

<div>
<p class="bold">Numele evenimentului</p>
{{Form::text('event', Input::old('event') ? Input::old('event') : $event->event)}}
</div>

<div>
<p class="bold">Sportul</p>
{{Form::select('sport_id', $sports, Input::old('sport_id') ? Input::old('sport_id') : $event->sport_id);}}

</div>

<?php
$arenaIsbhidden = (isset($event) && $event->arena_id ? $event->arena_id : (Input::old('arena_id') ? Input::old('arena_id') : false)) ? '' : 'bhidden';
?>

<div class="arenaholder <?php echo $arenaIsbhidden?>">
<p class="bold">Arena / Teren </p>
   <div style="float:left">

   {{Form::select('arena_id', $arene, Input::old('arena_id') ? Input::old('arena_id') : $event->arena_id, array('class' => "arena","data-placeholder" => "Alege o arena..."));}}
   </div>

   <div class="<?php echo (count($arene) ?  'bhidden' : '')?> holderadaugapopup" style="float:left">
      &nbsp;Locatia nu e in lista? <a class="adaugapopup" href="javascript:void(0);">Adauga locatia</a>
   </div>
   <div style="clear: both;float: none;"></div>
</div>

<div>
   <p class="bold">Data si ora</p>
   {{Form::text('data', Input::old('data') ? Input::old('data') : date2js($event->data),
      array("id" =>"datepicker"))}}
ora
   {{Form::text('ora', Input::old('ora') ? Input::old('ora') : time2js($event->ora),
      array("id" =>"timepicker"))}}
</div>

<div>
<p class="bold">Locatie</p>
{{Form::select('oras_id', $orase, Input::old('oras_id') ? Input::old('oras_id') : $event->oras_id, array("class"=>"localitate"))}}
</div>

<?php

/*delayed for future versions
<div>
<p class="bold">Eveniment de tip</p>
<?php
$eventTypes = array(
   array('id' => TYPE_PUBLIC, 'name' => TYPE_PUBLIC_TEXT),
   array('id' => TYPE_PRIVATE, 'name' => TYPE_PRIVATE_TEXT)
);
echo form_simple_dropdown('tip_eveniment', $eventTypes, 'id', 'name',
   array(), isset($event) ? $event->arena_id : set_value('tip_eveniment'));
?>
</div>
*/
?>

<div>
<p class="bold">Pret de persoana</p>

{{Form::text('pret', Input::old('pret') ? Input::old('pret') : $event->pret)}}
Lei
</div>

<div>
<p class="bold">Nr. locuri disponibile</p>
{{Form::text('locuri', Input::old('locuri') ? Input::old('locuri') : $event->locuri)}}
</div>

<?php
/*if ($this->ion_auth->logged_id() == EVENT_OWNER_ID) {*/
?>

<div>
<br />
{{Form::submit($event->id ? 'Salveaza modificarile' : 'Creeaza eveniment', array('class' => "btn btn-primary"))}}
&nbsp;
{{Form::reset('Reset', array('class' => 'btn'))}}
{{Form::close()}}
</div>

<script type="text/javascript">
$(function() {
  var firstRedir = true;
   $.datepicker.setDefaults({
      dateFormat: 'DD, d MM yy',
      dayNames: ['Duminica', 'Luni', 'Marti', 'Miercuri', 'Joi', 'Vineri', 'Sambata'],
      monthNames: ['Ianuarie','Februarie','Martie','Aprilie','Mai','Iunie','Iulie','August','Septembrie','Octombrie','Noiembrie','Decemberie']
    });


   /*
   $('#datepicker').datetimepicker({
      dateFormat: 'DD, d MM yy',
      hourGrid: 4,
      minuteGrid: 10,
      timeFormat: 'hh:mm'
   });
   */

   $( "#datepicker").datepicker({
      showOtherMonths: true,
      selectOtherMonths: true,
      showButtonPanel: true,
      minDate: 0,
      showOn: "both",
      buttonImage: '{{ URL::asset('/img/icalsm.png') }}',
      buttonImageOnly: true
   });

   $('#timepicker').timepicker({
      hourGrid: 4,
      minuteGrid: 10,
      timeFormat: 'HH:mm',
      showOn: "both",
      buttonImage: '{{ URL::asset('/img/clock.png') }}',
      buttonImageOnly: true
   });

   $('.arena').chosen({no_results_text: "Nici o locatie nu contine"}).change(function() {
      //$('.holderadaugapopup').show();
   });

   $(document).on('keyup', '.chzn-search input', function() {
      if ($('.chzn-drop .no-results').is(':visible')) {
         var value = $('.chzn-search input').val();
         $( "#locatiedialog-form" ).data('search', {
            "searched": value,
            "sportId": $('select[name="sport_id"]').val(),
            "sportName": $('select[name="sport_id"] option:selected').text()
         });
      }
   });

   $('.adaugapopup').click(function() {
      $( "#locatiedialog-form" ).dialog( "open" );
   });

   $('select[name="sport_id"]').change(function() {
      $('.holderadaugapopup').show();
      //ajax get arenes filtrare dupa sport.$
      var that = $(this);
      var sportId = $(this).val();
      var locatieId = $('.localitate').val();
      if (isNaN(parseInt(sportId,10))) {
         return false;
      }
      $.get('{{URL::to("/locatii/sport-locatii/")}}' + '/' + sportId,
         function(data) {
            $('.arena').empty();
            $('.arena').append($('<option>').text('Alege o arena').attr('value', '0'));
            $.each(data.locatii, function(key,value) {
               $('.arena').append($('<option>').text(value.name).attr('value', value.id));
            });
            @if (Input::has('aid')) {
              if (firstRedir) {
                $('.arena').val({{(int)Input::get('aid')}});
              }
              firstRedir = false;
            }
            @elseif (Input::old('arena_id'))
               $('.arena').val({{(int)Input::old('arena_id')}});
            @endif
            if (that.data("selected") && that.data("selected")) {
               $('.arena').val(that.data("selected"));
            }
            $(".arena").trigger("liszt:updated");

         }
      , "json");

      $('.arenaholder').show();
   });
   //$('arenaholder').show();

   //$('.holderadaugapopup').show();
   //$( "#locatiedialog-form" ).dialog( "open" );
   //@todo remove it
   // $('.go').trigger('click');
});
</script>

<div class="locatiedialog-form" id="locatiedialog-form"
   title="Adauga arena">
<p class="validateTips">Toate campurile sunt obligatorii</p>
<form>
<fieldset><label for="name">Numele locatiei</label> <input type="text"
   name="name" id="name" class="text ui-widget-content ui-corner-all" />
   <label for="email">Adresa locatiei</label> <input type="text"
   name="address" id="address" value=""
   class="text ui-widget-content ui-corner-all" /> <label for="email">Orasul</label>
   {{Form::select('dialoglocalitate', $orase, Input::old('oras_id') ? Input::old('oras_id') : $event->oras_id, array("class"=>"dialoglocalitate"))}}
</fieldset>
</form>
</div>

<script type="text/javascript">
$(function() {

  @if (Input::has('sid') || Input::old('sport_id'))
    $('select[name="sport_id"]').trigger('change');
  @endif


   var name = $( "#name" ),
     address = $( "#address" ),
     localitate = $( ".dialoglocalitate:visible" ),
     allFields = $( [] ).add( name ).add( address ).add( localitate ),
     tips = $( ".validateTips" );

   function updateTips( t ) {
     tips
       .text( t )
       .addClass( "ui-state-highlight" );
     setTimeout(function() {
       tips.removeClass( "ui-state-highlight", 1500 );
     }, 500 );
   }

   function checkLength( o, n, min, max ) {
     if ( o.val().length > max || o.val().length < min ) {
       o.addClass( "ui-state-error" );
       updateTips( "Numarul de caractere pentru " + n + " trebuie sa fie intre " +
         min + " si " + max + "." );
       return false;
     } else {
       return true;
     }
   }

   function checkRegexp( o, regexp, n ) {
     if ( !( regexp.test( o.val() ) ) ) {
       o.addClass( "ui-state-error" );
       updateTips( n );
       return false;
     } else {
       return true;
     }
   }

   $( "#locatiedialog-form" ).dialog({
     autoOpen: false,
     height: 'auto',
     width: 555,
     modal: true,
     open: function() {
        if ($(this).data("search")) {
           name.val($(this).data("search").searched);
           $( "#locatiedialog-form" ).dialog( "option", "title", "Adauga arena " + $(this).data("search").sportName);
        }

     },
     buttons: {
       "Adauga": function() {
         var bValid = true;
         allFields.removeClass( "ui-state-error" );

         bValid = bValid && checkLength( name, "numele locatiei", 3, 250 );
         bValid = bValid && checkLength( address, "adresa locatiei", 5, 500 );
         bValid = bValid && checkRegexp( localitate, /^[1-9][0-9]*/,"Orasul lipseste. Alege un oras din lista." );
         bValid = bValid && checkRegexp( localitate, /^(?!45$)/,"Orasul lipseste. Alege un oras din lista." );

         var sport_val = parseInt($('select[name="sport_id"]').val(), 10);
         if (isNaN(sport_val) || !sport_val) {
            updateTips('Nu ai selectat nici un sport pentru aceasta arena.');
            bValid = false;
         }

         if ( bValid ) {
            var $that = $(this);
            $.post(
               "/locatii/add/",
               { "name": name.val(), "address": address.val(), "localitate": localitate.val(), "sport":sport_val },
               function(data){
                  if (data.status == 'ok') {
                     $('select[name="sport_id"]').data("selected", data.id);
                     $('select[name="sport_id"]').trigger('change');
                     $that.dialog( "close" );
                  }
                  else {
                     updateTips(data.msg);
                  }
               }, "json");
         }
       },
       "Renunta": function() {
         $( this ).dialog( "close" );
       }
     },
     close: function() {
       allFields.val( "" ).removeClass( "ui-state-error" );
       $(this).data('search', "");
     }
   });
 });
</script>


@stop {{-- content --}}