@extends('master')

@section('content')

@section('breadcrumb')
   <li class="active">Evenimente</li>
@stop

<div>
   <a href="{{route('events.create')}}" class="btn btn-primary">Creeaza eveniment</a>
   <br />
   <br />
</div>

@if ($events)
<table class="table table-hover table-striped">
<tr>
   <th>Eveniment</th>
   <th>Data</th>
   <th>Ora</th>
   <th>Sport</th>
   <th>Arena</th>
   <th>Actiuni</th>
</tr>

@foreach($events as $row)

   <?php $expired = strtotime($row->data . ' ' . $row->ora) < strtotime("now"); ?>

   <tr class="@if($expired) faded @endif">
      <td>
         <a href="{{route('events.show' , $row->id)}}">{{$row->event}}</a>
      </td>
      <td>@if ($expired)<strike>@endif
          {{date2js($row->data)}}
          @if ($expired)</strike>@endif
       </td>
      <td>{{time2js($row->ora)}} </td>

      <td>{{-- $row->sports->name --}} </td>
      <td>
         {{a_locatie($row->arene)}}
      </td>
      <td>
         <a href="{{route('events.edit', $row->id)}}">Edit</a> |
         @if ($row->active)
            <a href="{{URL::action('EventsController@getHide', $row->id)}}">Ascunde</a>
         @else
            <a href="{{URL::action('EventsController@getShow', $row->id)}}">Activeaza</a>
         @endif
      </td>
   </tr>
@endforeach

</table>

{{$events->links()}}

@endif

@stop