@extends('master')

@section('head')
@parent
{{ Html::style('css/lightbox.css') }}
{{ Html::script('js/lightbox.js') }}

<style type="text/css">
#map_canvas {
   display:none;
   width: 660px;
   height: 500px;
   border: 1px solid black;
}
</style>
@stop

@section('content')

@section('breadcrumb')
<li>
   <a href="{{route('locatii.index')}}">Lista locatii</a>
   <span class="divider">/</span>
</li>

<li class="active">@if ($locatie->id) Editeaza locatie @else Creeaza locatie @endif</li>
@stop

@if ($locatie->id)
<h4>
   {{a_locatie($locatie)}}
</h4>
@endif

@if ($locatie->id)
{{ Form::open(array(
   'files' => 1,
   'method' => 'put',
   'route' => array('locatii.update', $locatie->id),
   'class' => "form-horizontal well")) }}
   @else
   {{ Form::open(array(
      'files' => 1,
      'route' => array('locatii.store'),
      'class' => "terenaction well")) }}
      @endif

      <div>
         <p class="bold"><b>Detalii locatie</b></p>
         <div class="pull-left right-space">
            <p class="bold">Numele locatiei*</p>
            {{Form::text('name', Input::old('name') ? Input::old('name') : $locatie->name, array('size' => "53"))}}
         </div>

         <div class="pull-left">
            <p class="bold">Orasul*</p>
            {{Form::select('oras_id', $orase, Input::old('oras_id') ? Input::old('oras_id') : $locatie->oras_id, array("class"=>"localitate"))}}
            ex: <a href="javascript:void(0);" id='preselect'>Bucuresti</a>
         </div>
      </div>
      <div class="clearfix"></div>

      <div>
         <div class="pull-left right-space">
            <p class="bold">Adresa*</p>
            {{Form::textarea('adresa', Input::old('adresa') ?: $locatie->adresa, array('class' => 'adresa','rows' => "2"))}}
         </div>

         <div class="pull-left right-space">
            <p class="bold">Tip locatie*</p>
            {{Form::select('locatie_type', $tipLocatii, Input::old('locatie_type') ?: $locatie->locatie_type, array("class"=>"locatie_type"))}}
         </div>


         <div class="pull-left">
            <p class="bold">Puncte de reper</p>
            {{Form::textarea('puncte_reper', Input::old('puncte_reper') ? Input::old('puncte_reper') : $locatie->puncte_reper, array('rows' => "2"))}}
         </div>
      </div>
      <div class="clearfix"></div>


      <div class="right-space">
        <p class="bold">Descriere / Dotari</p>
        {{Form::textarea('descriere', Input::old('descriere') ? Input::old('descriere') : $locatie->descriere, array(
         'rows' => "4" ,
         'placeholder' => "Parcare / Vestiare / Dusuri / Bar, etc.",
         'style' => 'width: 400px'))}}
      </div>

      <a id="loadgmap" href="#loadmap">incarca harta</a>
      <div class="maperror"></div>
      <div id="map_canvas" class="hidden"></div>
      <div>

         <p class="bold"><b>Coordonate GPS si harta</b></p>
         <div class="pull-left right-space">
            <p class="bold">Latitudine(4x.xxx)</p>
            {{Form::text('latitudine', Input::old('latitudine') ? Input::old('latitudine') : $locatie->latitudine, array('size' => "53"))}}
         </div>

         <div class="pull-left right-space">
            <p class="bold">Longitudine(2x.xxx)</p>
            {{Form::text('longitudine', Input::old('longitudine') ? Input::old('longitudine') : $locatie->longitudine, array('size' => "53"))}}
         </div>
      </div>
      <div class="clearfix"></div>




      <div>
       <br />
       Imagini locatie
       @if ($locatie->images)
       <div>
         <br />
         <b>Imagini locatie</b>

         <div class="row-fluid">
            <ul id="gallery" class="thumbnails">
               @foreach($locatie->images as $image)
               @if (file_exists(public_path() . Config::get('sitesettings.imglocatiitb') . $image->filename))
               <li class="span3 thumbnail">
                  <a rel="lightbox[teren]" href="{{Config::get('sitesettings.imglocatii') . $image->filename}}">
                     <img src="{{Config::get('sitesettings.imglocatiitb') . $image->filename}}">
                  </a>
                  <br />
                  <label class="checkbox">
                     Sterge {{Form::checkbox('delimg[]', $image->id);}}
                  </label>
               </li>
               @endif
               @endforeach
            </ul>
         </div>
      </div>
      @endif

      <div class='list_location'></div>
      <button class="btn btn-file add_img">+ Adauga imagine</button>
   </div>

   <div>
      <p class="bold">Sport(uri - selectie multipla)</p>
      {{Form::select('sport[]', $sports, Input::old('sport') ? Input::old('sport') : $locatie->sports->lists('id'), array('multiple'=>'multiple'))}}
      <br />
   </div>

   <hr class="dotted"/>

   <b>Date de contact locatie</b>
   <div>
      <div class="pull-left right-space">
         <p class="bold">Nume Contact</p>
         {{Form::text('nume_contact', Input::old('nume_contact') ? Input::old('nume_contact') : $locatie->nume_contact, array('size' => "53"))}}
      </div>

      <div class="pull-left right-space">
         <p class="bold">Telefon Contact</p>
         {{Form::text('telefon', Input::old('telefon') ? Input::old('telefon') : $locatie->telefon, array('size' => "53"))}}
      </div>
      <div class="clearfix"></div>

      <div>
         <div class="pull-left right-space">
            <p class="bold">Email</p>
            {{Form::text('email', Input::old('email') ? Input::old('email') : $locatie->email, array('size' => "53"))}}
         </div>

         <div class="pull-left right-space">
            <p class="bold">Website</p>
            {{Form::text('website', Input::old('website') ? Input::old('website') : $locatie->website, array('size' => "53"))}}
         </div>
         <div class="clearfix"></div>

         <br />

         @if (!is_logged())
         <b>Cod verficare</b>
         {{$captcha->html()}}
         @endif

         <div class="form-actions">
            {{Form::submit($locatie->id ? 'Salveaza modificarile' : 'Creeaza locatie', array('class' => "btn btn-primary"))}}
            &nbsp;
            {{Form::reset('Reset', array('class' => 'btn'))}}
         </div>

         {{Form::close()}}

         <script type="text/javascript" src="{{Config::get('sitesettings.google_maps_url')}}"></script>

         <script type="text/javascript">

         $(document).ready(function(){
            $(".add_img").click(function(){
               $(".list_location")
               .append('<input type="file" name="img[]" class="imgaction"><br />')
               .append('<input type="text" name="url[]" placeholder="Copyright"><br /><hr />');
               return false;
            });
         })

         $(function() {
            $('form.terenaction').submit(function() {
               var img = $('.imgaction[value!=""]').size();
               if (!img) {
                  var c = confirm("Vrei sa salvezi locatia fara sa uploadezi vreo imagine?");
                  if (!c) {
                     return false;
                  }
                  $('.imgaction').remove();
               }
            });

            var geocoder;
            var map, marker1;

            function initialize(lat, lon) {
               lat = lat || 44.436160158242714;
               lon = lon || 26.1021785736084;
               var latlng = new google.maps.LatLng(lat, lon);
               var mapOptions = {
                 zoom: 16,
                 center: latlng,
                 mapTypeId: google.maps.MapTypeId.ROADMAP
              }
              map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
              if (typeof marker1 == 'undefined') {
               marker1 = new google.maps.Marker({
                  map: map,
                  draggable:true,
                  position: new google.maps.LatLng(lat, lon),
                  animation: google.maps.Animation.DROP
               });
            }
            map.setCenter(marker1.position);
            $('#map_canvas').removeClass('hidden');
         }

         function show() {
            $('input[name="latitudine"]').val(marker1.getPosition().lat());
            $('input[name="longitudine"]').val(marker1.getPosition().lng());
         }

         function codeAddress() {
          var adresa = $('textarea[name="adresa"]').val();
          var oras = $('.localitate option:selected').text();
          var tara = 'Romania';
          if (adresa == '' || oras == '') {
             return false;
          }
          var address = adresa + ', ' + oras + ', ' + tara;

          ret = false;

          geocoder = new google.maps.Geocoder();
          geocoder.geocode( { 'address': address}, function(results, status) {
           if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            marker1.setPosition(results[0].geometry.location);
            /*
            marker1 = new google.maps.Marker({
                map: map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                position: results[0].geometry.location
            });
          */
          show();
          ret = {lat: results[0].geometry.location.lat(),lng: results[0].geometry.location.lng()};
       } else {
         alert('Geocode was not successful for the following reason: ' + status);
      }
   });

          return ret;
       }

       $('#loadgmap').click(function() {
         var $localitate = $('.localitate');
         var $adresa = $('.adresa');

         $.hideMsg('.maperror');
         if ($localitate.val() == "0") {
            $.showError('Pentru incarcarea corecta a hartii, orasul trebuie selectat','.maperror');
            return false;
         }

         if (!$adresa.val()) {
            $.showError('Pentru incarcarea corecta a hartii, adresa trebuie completata si sa fie de forma Str. Unirii, Nr. 10','.maperror');
            return false;
         }

         $.showMsg('Pentru a selecta pozitia locatiei, mutati indicatorul rosu, tinand click stanga apasat pe el.', '.maperror');

         $('#map_canvas').show();

         var lat = $('input[name="latitudine"]').val(), lng = $('input[name="longitudine"]').val();
         initialize(lat, lng);
         if (lat == '' || lng == '') {
            var obj = codeAddress();
         }

         google.maps.event.addDomListener(marker1, 'dragend', show);
         google.maps.event.addDomListener(marker1, 'mouseup', show);
         google.maps.event.addDomListener(map, 'click', function(data) {
           marker1.setPosition(data.latLng);
           show();
        });
      //load gmap
   });

$('#preselect').click(function() {
   var $that = $(this);
   $('.localitate option').each(function() {
      if ($(this).text().toLowerCase() == $that.html().toLowerCase()) {
         $('.localitate').val($(this).val()).change();
         return false;
      }
   });
});

});
</script>

@stop

