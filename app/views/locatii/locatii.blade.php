@extends('master')

@section('content')

@section('breadcrumb')
   <li class="active">
      Lista locatii
   </li>
@stop

<ol class="unstyled row-fluid">

   @foreach($locatii as $locatie)


   <li class="row-fluid @if (Html::oddeven('users')) even @else odd @endif">
      <div class="span3 tac">
         <div class="">
            <a class="thumbnail" href="{{href_locatie($locatie)}}">

            @if (isset($imgIdObj[$locatie->id]) && file_exists(public_path() . Config::get('sitesettings.imglocatiitb') . $imgIdObj[$locatie->id]->filename))
               {{Html::image(Config::get('sitesettings.imglocatiitb') . $imgIdObj[$locatie->id]->filename, $locatie->name)}}
            @else
               {{Html::image('/img/phcamera.jpg', 'Fara imagine')}}
            @endif
            </a>
         </div>
         <div class="caption">
            {{a_locatie($locatie)}}
         </div>
      </div>

      <div class="span8">

         @if (is_admin())
            Status: @if ($locatie->active == 1) Activa @endif
            <br />
            Aprobata: @if (!$locatie->approved) Nope - <a href="{{route('locatii.edit', $locatie->id)}}">aproba</a>
            @endif
            <br />
         @endif



         @if (Auth::check() && (is_admin() || Auth::user()->id == $locatie->owner_id))
            <a href="{{route('locatii.edit', $locatie->id)}}" class="btn btn-primary btn-small">edit</a>
            <a onclick="return confirm('Sigur vrei sa stergi {{$locatie->name}}')" href="{{route('locatii.destroy', $locatie->id)}}" class="btn btn-inverse btn-small">delete</a>
            <br />
         @endif
         <table class="info">
         <tr>
            <td class="greyinfo">Nume:</td>
            <td>{{$locatie->name}}</td>
         </tr>
         <tr>
            <td class="greyinfo">Adresa:</td>
            <td>{{$locatie->adresa}}, <span class="label">{{$locatie->orase->oras}}</span></td>
         </tr>
         @if(count($locatie->sports))
         <tr>
            <td class="greyinfo">Sporturi: </td>
            <td>
               @foreach ($locatie->sports as $sport)
               <a class="label label-info" href="/sports/{{$sport->name}}">{{$sport->name}}</a>
               @endforeach
            </td>
         </tr>
         @endif
         @if ($locatie->telefon)
         <tr>
            <td class="greyinfo">Tel.:</td>
            <td>{{fmtel($locatie->telefon)}}</td>
         </tr>
         @endif
         @if($locatie->email)
         <tr>
            <td class="greyinfo">Email:</td>
            <td>{{$locatie->email}}</td>
         </tr>
         @endif
         <tr>
            <td colspan="2">
               <a href="{{href_locatie($locatie)}}">mai multe detalii</a>
            </td>
         </tr>
         </table>
      </div>
   </li>

   <li><hr style="margin: 5px 0 10px 0" /></li>

   @endforeach

</ol>

{{$locatii->links()}}

@stop