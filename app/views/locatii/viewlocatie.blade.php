@extends('master')

@section('head')
   @parent

   {{ Html::style('css/lightbox.css') }}
   {{ Html::style('css/panel.css') }}
   {{ Html::script('js/lightbox.js') }}

   <style type="text/css">
   #map_canvas {
      display:none;
      width: 510px;
      height: 320px;
      border: 1px solid #F5F5F5;
      margin: 0 auto 10px;
      -moz-border-radius: 4px;
      -webkit-border-radius: 4px;
      border-radius: 4px;
   }
   @media (max-width: 480px) {
      #map_canvas {
         width: 240px;
      }
    }
 </style>
@stop

@section('content')

@section('breadcrumb')
   <li>
      <a href="{{route('locatii.index')}}">Lista locatii</a>
      <span class="divider">/</span>
   </li>

   <li class="active">{{$locatie->name}}</li>
@stop

<div class="panel panel-default">
   <div class="panel-heading">
      <div class="pull-left">
         <h4><span style="font-size: medium">@if($locatie->locatiitype){{$locatie->locatiitype->tip}}@else Teren @endif de </span>
         @foreach ($locatie->sports as $sport)
            <span style="font-size: medium">{{lower($sport->name)}}</span>
         @endforeach {{$locatie->name}}, <span style="font-size: medium">{{ucf($locatie->orase->oras)}}</span>
      </h4>
      </div>

      @if (Auth::check() && (is_admin() || Auth::user()->id == $locatie->owner_id))
      <div class="pull-right">
            @if (is_admin())
               Status:
               @if ($locatie->active)
               <span class="label label-success">active</span>
               @else
               <span class="label">inactive</span>
               @endif
            @endif

            @if (is_admin() && !$locatie->approved)
               {{link_to(action('LocatiiController@getAproba', $locatie->id), $title = 'aproba', $attr = ['class' => "btn btn-success btn-small"]);}}
            @endif

            <a href="{{route('locatii.edit', $locatie->id)}}" class="btn btn-primary btn-small">edit</a>
            <a onclick="return confirm('Sigur vrei sa stergi {{$locatie->name}}')" href="{{URL::to('locatiiapi/delete/' . $locatie->id)}}"  class="btn btn-inverse btn-small">delete</a>
      </div>
      @endif
   {{--   Twitter | Facebook | G+ | Reddit --}}
      <div class="clearfix"></div>
   </div>


   <div class="panel-body">


      {{-- Short desc. panel body --}}


   <div>
      <div class="pull-left" style="margin-right: 10px; max-width: 305px;">
      {{-- contact panel div --}}

      <div class="panel panel-default">
         <div class="panel-heading">
            Contact
         </div>
         <div class="panel-body">
             Adresa: {{$locatie->adresa}}, <span>{{ucf($locatie->orase->oras)}}</span>

            @if ($locatie->approved && $locatie->website)
               <div class="voffset1"><a href="{{$locatie->website}}">{{$locatie->website}}</a></div>
            @endif
            @if($locatie->nume_contact)<div class="voffset1">Nume contact: {{$locatie->nume_contact}}</div>@endif
            @if($locatie->telefon)<div class="voffset1">Tel.: {{fmtel($locatie->telefon)}}</div>@endif
            @if($locatie->email)<div class="voffset1">Email: {{$locatie->email}}</div>@endif

            @if ($locatie->puncte_reper)
            <div class="voffset1">
               <div>Puncte de reper</div>
               {{{$locatie->puncte_reper}}}
            </div>
            @endif
         </div>
      </div>

      {{-- descriere panel div --}}
      @if ($locatie->descriere)
      <div class="panel panel-default">
         <div class="panel-heading">
            Descriere / Dotari
         </div>
         <div class="panel-body">
           {{nl2br($locatie->descriere)}}
         </div>
      </div>
      @endif



      <div class="panel panel-default">
         <div class="panel-heading">
            Sporturi disponibile
         </div>
         <div class="panel-body">
         <ul>
         @foreach ($locatie->sports as $sport)
           <li><a href="/sports/{{$sport->name}}">{{$sport->name}}</a></li>
         @endforeach
         </ul>
         </div>
      </div>


      </div>
      <div class="pull-left">
         {{-- map panel div --}}
         @if ($locatie->longitudine && $locatie->latitudine)
            <div id="map_canvas" class="panel-body">map canvas</div>
         @endif
      </div>
   </div>{{-- div containing contact, descriere, harta --}}
   <div class="clearfix"></div>

      {{-- images div --}}
      @if (count($locatie->images))
      <div class="panel panel-default">
         <div class="panel-heading">
            Imagini locatie
         </div>
         <div class="panel-body">
            <ul id="gallery" class="thumbnails">
            <?php
               $copyrights = array();
            ?>
            @foreach($locatie->images as $image)
               @if (file_exists(public_path() . Config::get('sitesettings.imglocatiitb') . $image->filename))
               <?php
               if (!in_array($image->copyright, $copyrights)) {
                  if ($image->copyright && !preg_match('@^https?://@', $image->copyright))
                  {
                     $image->copyright = 'http://' . $image->copyright;
                  }
                  $copyrights[$image->copyright] = $image->copyright;
               }
               ?>
               <li class="thumbnail">
                  <a rel="lightbox[teren]" href="{{URL::to(Config::get('sitesettings.imglocatii') . $image->filename)}}">
                     {{Html::image(Config::get('sitesettings.imglocatiitb') . $image->filename)}}
                  </a>
               </li>
               @endif
            @endforeach
            </ul>

            @if(count($copyrights))
               Credit imagini: <br />
               @foreach($copyrights as $copyright)
                  <a href="{{$copyright}}">{{$copyright}}</a>
               @endforeach
               <br />
            @endif
         </div>
      </div>
      @endif

   {{-- comment panel div --}}
   @include('_parts/comments')

   </div> {{-- panel body --}}
</div> {{-- pane header --}}

@if ($locatie->longitudine && $locatie->latitudine)
   @if (app()->environment() == 'local')

   @endif

<script type="text/javascript" src="{{Config::get('sitesettings.google_maps_url')}}"></script>
<script type="text/javascript">
function initialize(lat, lon) {

   lat = lat || null;
   lon = lon || null;

   if (!lat || !lon) {
      return; //nothing to show on map
   }

  var myOptions = {
    center: new google.maps.LatLng(lat, lon),
    zoom: 16,

    scrollwheel: false,

    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById("map_canvas"),
      myOptions);
  $('#map_canvas').show();
  marker1 = new google.maps.Marker({
     map: map,
     position: new google.maps.LatLng(lat, lon),
     animation: google.maps.Animation.DROP
   });

  onMarkerClick = function() {
     //var latLng = marker.getPosition();
     infoWindow.setContent('<h3>Marker position is:</h3>');
     infoWindow.open(map, marker1);
   };

  google.maps.event.addListener(marker1, 'click', onMarkerClick);

  infoWindow = new google.maps.InfoWindow;

}
</script>


<script>
   $(function() {
      //$('#gallery a').lightBox();
      initialize({{$locatie->latitudine}}, {{$locatie->longitudine}});
   });
</script>
@endif

@stop