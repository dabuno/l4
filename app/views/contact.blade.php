@extends('master')

@section('breadcrumb')
   <li>Contact</li>
@stop

@section('content')

   <fieldset>
   <legend>Parerea, propunerea ta conteaza!</legend>

   Multumim pentru vizita! <br />
   Foloseste formularul de mai jos pentru a trimite parerile si sugestiile tale.
   <br />
   <br />

   {{ Form::open(array('class' => "form-horizontal", "style" => "line-height: 12px;")) }}


   @if (!is_logged())
   <div class="input-append">
      {{Form::text('email', Input::old('email'), array('class' => 'span3', "placeholder" => "Adresa E-mail"));}}
    <span class="add-on"><i class="icon-envelope"></i></span>
   </div>
   @endif

   <br />
   <br />

   {{Form::textarea('text', Input::old('text'), array(
    'rows'        => '10',
    'cols'        => '120',
    'style'       => "width:500px",
    'placeholder' => "Propunerea ta este..."));}}


   <br />

   @if (!is_logged())
      {{$captcha->html()}}
   @endif

   <br />

   {{Form::submit('Trimite', array('class' => "btn btn-primary"));}}
   {{Form::close();}}

   </fieldset>



@stop
