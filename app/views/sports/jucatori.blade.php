@extends('master')

@section('content')

@section('breadcrumb')
   <li class="active">
      <a href="{{URL::to('/sports/' . $sport->name)}}">{{$sport->name}}</a>
      <span class="divider">/</span>
   </li>

   <li class="active">Jucatori</li>
@stop


<h3>Jucatori</h3>

@if (!count($users))
Momentan nu este inregistrat nici un alt jucator pentru {{$sport->name}}.
@endif

<ol class="unstyled row-fluid">

@foreach($users as $user)
   {{-- #skip user if it's the owner --}}
   <li id="uid_{{$user->id}}" class="userlist row-fluid {{Html::oddeven('jucatori')}}" style="padding-top: 5px">
      <div class="span3 tac">
         <div class="">
            <a class="thumbnail" href="{{href_profile($user)}}" title="{{$user->alias}}">
               @if($user->profile_image)
                  {{Html::image(Config::get('sitesettings.uimgtb') . $user->profile_image , $user->alias)}}
               @else
                  {{Html::image('/img/phcamera.jpg', 'Fara imagine')}}
               @endif
            </a>
         </div>
         <div class="caption">
            {{a_profile($user)}}
         </div>
      </div>


      <div class="span8">
         <?php
            //handle not logged convoaca action
            $whatToDo = 'javascript:;';
            if(!Auth::check()) {
               $whatToDo = URL::to('login');
            }
         ?>

         <div>{{$gender[$user->gen]}}
            @if ($user->gen == 2)
               <img src="{{URL::to('img/Venus_symbol.svg.png')}}" style="width: 20px; vertical-align: top" alt="Feminin">
            @else
               <img src="{{URL::to('img/Mars_symbol.svg.png')}}" style="width: 20px; vertical-align: top" alt="Masculin">
            @endif

         </div>

         <div class="$sport->id sportsch">
            Vreau sa joc <span class="label label-info">
               {{$sport->name}}
            </span>
         </div>

         <div>in orasul
            <span class="label">
               @if ($user->oras)
                  {{$user->oras}}
               @endif
            </span>
         </div>
            <?php
            /*
             * RATING ZONE
             */

            /*

            ?>

            <div class="fl urdiv">
               <div class="bold">Nivel</div>
                  <form id="selrate_<?php echo $user->id;?>" name="selrate_<?php echo $user->id;?>">

                  <?php
                     $defaultDisabled = true;
                     if($this->ion_auth->logged_in() && !$this->ion_auth->is_admin() && !in_array($this->ion_auth->logged_id(), $ua->from_uid)) {
                        $defaultDisabled = false;
                     }
                  ?>

                  <div class="fl prating star" id="ustar_<?php echo $ua->id;?>">
                  </div>
                  <div class="fl prating" style="margin-left: 5px" id="precision_<?php echo $ua->id;?>">
                  </div>
                  <div class="cl"></div>
                  <div class="prating" style="margin-left: 5px" id="novotes_<?php echo $ua->id;?>">
                  </div>

                  <script type="text/javascript">
                     $('#ustar_<?php echo $ua->id;?>').raty({
                        <?php if ($defaultDisabled) {?>
                        readOnly: true,
                        <?php }?>
                        half:       true,
                        precision:  true,
                        target:     '#precision_<?php echo $ua->id;?>',
                        space: false,
                        start:       <?php echo  isset($ua->rating) && $ua->rating ? (float)$ua->rating : 0 ?>,
                        targetType: 'number',
                        noRatedMsg: 'fara rating momentan',
                        click: function(score, evt) {
                           var that = this;
                           $.ajax({
                              url: '<?php echo site_url('sports/asr')?>/' + this.id.split('_')[1] + '/' + score,
                              success: function(json) {
                                 data = $.parseJSON(json);
                                 if (data == null || typeof data.msg == 'undefined' || data.msg == null)
                                 {
                                    return false;
                                 }
                                 $("#novotes_<?php echo $ua->id;?>").html(data.msg.no_votes + ' votes');
                                 $(that).raty('start', data.msg.rating);
                                 $(that).raty('readOnly', true);
                              }
                           });
                        }
                     }).click(function () {
                       <?php if (!$this->ion_auth->logged_in()) {
                          echo 'alert("Trebuie sa fii logat ca sa poti da un rating!");';
                       } else if ($defaultDisabled) {
                          echo 'alert("Ai votat deja acest utilizator!");';
                       }?>
                     });
                  </script>
                  </form>
               </div> <!-- end rating div -->
            */
           ?>
            <div>
               <br>

               @if (Auth::check() && Auth::user()->id == $user->id)
               @else
               <a href="<?php echo $whatToDo?>" class="go btn btn-success btn-small" id="go_<?php echo $user->id?>">Convoaca</a>
               <div class="convoaca bhidden">
               </div>
               @endif
            </div>
         </div>
   </li>

   <li><div class="separator"></div></li>

@endforeach
</ol>

{{$users->links()}}

@include('_parts.convoaca')

@stop
