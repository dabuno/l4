@extends('master')

@section('content')

@section('breadcrumb')
   <li>
      <a href="{{URL::to("profile")}}">Profil</a>
      <span class="divider">/</span>
   </li>
   <li class="active">Edit</li>
@stop

{{ Form::open(array(
   'method' => 'post',
   'url' => 'sports/create',
   'class' => "form-horizontal well")) }}

<fieldset>
   <legend>Sport</legend>

   <div class="control-group">
      <label class="control-label" for="input01">Nume</label>
      <div class="controls">
         {{Form::text('name', Input::old('name') ? Input::old('name') : $sport->name, array('class' => "input-xlarge"))}}
      </div>
   </div>

   <div class="control-group">
      <label class="control-label" for="input01">Jucatori</label>
      <div class="controls">
         {{Form::checkbox('jucatori', 'jucatori', Input::old('name') && !Input::old('jucatori') ? false : true, array('class' => "input-xlarge"))}}
         
      </div>
   </div>

   <div class="control-group">
      <label class="control-label" for="input01">Locatii</label>
      <div class="controls">
         {{Form::checkbox('locatii', 'locatii', Input::old('name') && !Input::old('locatii') ? false : true, array('class' => "input-xlarge"))}}
         
      </div>
   </div>

   <div class="form-actions">
   {{Form::submit('Salveaza', array('class' => "btn btn-primary"))}}
   &nbsp;
   {{Form::reset('Reset', array('class' => 'btn'))}}
  </div>
  

</fieldset>



{{Form::close();}}

@stop