@extends('master')

@section('content')

@section('breadcrumb')
   <li class="active">
      <a href="{{URL::to('/sports/' . $sport->name)}}">{{$sport->name}}</a>
      <span class="divider">/</span>
   </li>

   <li class="active">Locatii {{$sport->name}}</li>
@stop

<h3>Locatii {{$sport->name}}</h3>

@if (is_admin())
<div>
   <a href="<?php echo route('locatii.create')?>" class="btn btn-primary btn-small">Creeaza Arena</a>
   <br />
   <br />
</div>
@endif

@if (!count($locatii))
   Momentan nu este inregistrata nici o locatie pentru {{$sport->name}}.
@endif

<ol class="unstyled row-fluid">

   @foreach($locatii as $locatie)
   <li class="row-fluid {{Html::oddeven('locatii')}}" style="padding-top:5px;">
      <div class="span3 tac">
         <div class="">
            <a class="thumbnail" href="{{href_locatie($locatie)}}">

            @if (isset($imgIdObj[$locatie->id]) && file_exists(public_path() . Config::get('sitesettings.imglocatiitb') . $imgIdObj[$locatie->id]->filename))
               {{Html::image(Config::get('sitesettings.imglocatiitb') . $imgIdObj[$locatie->id]->filename, $locatie->name)}}
            @else
               {{Html::image('/img/phcamera.jpg', 'Fara imagine')}}
            @endif
            </a>
         </div>
         <div class="caption">
            {{a_locatie($locatie)}}
         </div>
      </div>

      <div class="span8">
         @if (Auth::check() && (is_admin() || Auth::user()->id == $locatie->owner_id))
            <a href="{{route('locatii.edit', $locatie->id)}}" class="btn btn-primary btn-small">edit</a>
            <a onclick="return confirm('Sigur vrei sa stergi {{$locatie->name}}')" href="{{route('locatii.destroy', $locatie->id)}}" class="btn btn-inverse btn-small">delete</a>
            <br />
         @endif

         <table class="info">
         <tr>
            <td class="greyinfo">Nume:</td>
            <td>{{$locatie->name}}</td>
         </tr>

         <tr>
            <td class="greyinfo">Adresa:</td>
            <td>{{$locatie->adresa}} <span class="label"> BUCURESTI</span></td>
         </tr>

         <tr><td class="greyinfo">Sport: </td>
            <td>
               <a class="label label-info" href="{{URL::to('sports/' . $sport->name)}}">{{$sport->name}}</a>
            </td>
         </tr>

         @if ($locatie->telefon)
         <tr>
            <td class="greyinfo">Tel.:</td>
            <td>{{fmtel($locatie->telefon)}}</td>
         </tr>
         @endif

         @if($locatie->email)
         <tr>
            <td class="greyinfo">Email:</td>
            <td>{{$locatie->email}}endif</td>
         </tr>
         @endif

         <tr>
            <td colspan="2">
               <a href="{{href_locatie($locatie)}}" class="pull-left">mai multe detalii</a>
                <a href="{{url('events/create?sid=' . $sport->id . '&aid=' . $locatie->id)}}" class="pull-right">Creeaza eveniment</a>
            </td>
         </tr>
         </table>
      </div>
   </li>

   <li><div class="separator"></div></li>

   @endforeach

{{$locatii->links()}}

</ol>

@stop