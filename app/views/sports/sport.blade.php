@extends('master')

@section('content')

@section('breadcrumb')
   <li class="active">{{$sport->name}}</li>
@stop


<div class="hero-unit">

<p>If you would like to edit this page you'll find it located at:</p>
<code>application/views/main.php</code>

<p>The corresponding controller for this page is found at:</p>
<code>application/controllers/main.php</code>

<p>If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>
</div>

@stop {{-- content --}}