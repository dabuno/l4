@extends('master')

@section('content')

@section('breadcrumb')
   <li class="active">@if (isset($sport)) {{$sport->name}}@else Evenimente sportive @endif</li>
@stop

<h3>Evenimente @if (isset($sport)) {{$sport->name}}@else sportive @endif</h3>

@if (count($events))


@foreach($events as $event)

<div class="panel panel-default">
   <div class="panel-heading">
      <a href="{{route('events.show' , $event->id)}}">{{$event->event}}</a>
   </div>
   <div class="panel-body">

      <div>
      Data si ora de desfasurare:
      <b>
      <?php $expired = strtotime($event->data . ' ' . $event->ora) < strtotime("now"); ?>
      @if ($expired)<strike>@endif
      {{date2js($event->data)}} ora
      {{time2js($event->ora)}}
      @if ($expired)</strike>@endif
      </b>

      <br />
      Nume eveniment: <a href="{{route('events.show' , $event->id)}}">{{$event->event}}</a>

      <br />
      Locatia: {{a_locatie($event->arene)}} din <span>{{ucf($event->orase->oras)}}</span>

      <br />
      Organizator: {{a_profile($event->users)}}
      <br/>
      @if (!isset($sport))
         <b>Sport: </b>
         <a class="label label-info" href="{{URL::to('sports/' . $event->sports->name)}}">{{$event->sports->name}}</a>
         <br />
         <br />
      @endif

      @if (Auth::check() && $event->user_id == Auth::user()->id)
         {{--
            <a href="{{route('events.edit', $event->id)}}">Edit</a> |
            @if ($event->active)
               <a href="{{URL::action('EventsController@getHide', $event->id);}}">Ascunde</a>
            @else
               <a href="{{URL::action('EventsController@getShow', $event->id);}}">Activeaza</a>
            @endif
         --}}
         @else
         <div class="holder" style="text-align: center; margin-top: -30px;">
            <a id="event_{{$event->id}}" class="btn btn-success btn-small vreausajoc" href="javascript:;">
               Vreau sa joc!
            </a>
         </div>
      @endif
      </div>
   </div>
   <br />
</div>
@endforeach


@include('_parts.vreausajoc')

{{$events->links()}}

@else

Momentan nu este organizat nici un eveniment sportiv.
<br />
<br />
<a href="{{url('events/create')}}" class="btn btn-primary btn-small">Creeaza eveniment</a>
@endif {{-- ($events) --}}

@stop