<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
   </head>
   <body>
      <h2>Resetare parola vreausajoc.ro</h2>
      <div>
         Noua parola generata este: {{$password}} <br />
         Pentru a o schimba: {{ URL::to('profile/pwd')}}
      </div>
   </body>
</html>