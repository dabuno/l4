@extends('master')

@section('content')

@section('breadcrumb')
   <li class="active">Profil</li>
@stop

@if (is_logged() && is_logged() == $user->id && !count($user->orase))
<div class="alert alert-info">
   <button type="button" class="close" data-dismiss="alert">&times;</button>
   Selecteaza orasul in care vrei sa faci sport, mergand in pagina de editeaza profil.
</div>
@endif

<div class="panel panel-default">
   <div class="panel-heading comment-counter">
      <h4>{{$user->alias}}</h4>
   </div>{{-- panel-heading --}}
   <div class="panel-body">
      <div class="row-fluid" style="margin-top:5px">
         <div class="span3 tac">
            <div class="thumbnail">
              @if($user->profile_image)
                  {{Html::image(Config::get('sitesettings.uimgtb') . $user->profile_image, $user->alias)}}
              @else
                  {{Html::image('/img/phcamera.jpg', 'Fara imagine')}}
              @endif
            </div>
            <div class="caption">
              <?php echo $user->alias; ?>
            </div>
         </div>

         <div class="span8">

      <div>Genul: {{$gender[$user->gen]}}
         @if ($user->gen == 2)
            <img src="{{URL::to('img/Venus_symbol.svg.png')}}" style="width: 20px; vertical-align: top" alt="Feminin">
         @else
            <img src="{{URL::to('img/Mars_symbol.svg.png')}}" style="width: 20px; vertical-align: top" alt="Masculin">
         @endif

      </div>
      <br />
      @if ($user->sports)
         Vreau sa joc:
            @for ($i = 0, $co = count($user->sports); $i < $co; $i++)
               <a href="{{URL::to('sports/' . $user->sports[$i]['name'])}}">{{$user->sports[$i]['name']}}</a>
               @if ($i < $co -1)
                |
               @endif
            @endfor
            <br />
         @endif
         <br />
         @if ($user->orase)
            in localitatea: <span>{{$user->orase[0]['oras']}}</span>
         @endif
      </div>

      @if (Auth::user() && $user->id == Auth::user()->id)
      <div class="span8">
         <br />
         {{-- //if owner show edit button --}}
         <a href="{{route('profile.edit', $user->id)}}" class="btn btn-small btn-info">Editeaza</a>
      </div>
      @else

      <div id="userlist_{{$user->id}}" class="span8 userlist">
         <br><br>
         <a href="@if (!is_logged()){{URL::to('login')}} @else javascript:; @endif" class="go btn btn-success btn-small" id="go_{{$user->id}}">Convoaca</a>
         <div class="convoaca bhidden">
         </div>
      </div>
      @include('_parts.convoaca')

      @endif

      </div>{{--row-fluid --}}

      <br />
      @include('_parts/comments')

   </div>{{-- panel-body --}}
</div>{{-- panel-default --}}



{{-- commented
<br />
<div style="height: 1px; background-color: #F5F5F5; text-align: left">
  <span style="background-color: white; margin-left:20px; padding: 0 10px; position: relative; top: -0.7em;">
    Notificari
  </span>
</div>

<div class="well">
<br />
convocare anulata de OneShot
<br />
ai fost convocat de OneShot
<br />
l-ai provocat pe Alias
</div>

--}}

@stop