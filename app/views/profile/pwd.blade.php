@extends('master')

@section('content')

@section('breadcrumb')
   <li class="active">Profil</li>
@stop

{{ Form::open(array(
   'method' => 'post',
   'url' => 'profile/pwd',
   'class' => "form-horizontal well")) }}

<fieldset>
   <legend>Schimbare parola</legend>

   <div class="control-group">
      <label class="control-label" for="input01">Parola curenta</label>
      <div class="controls">
         {{Form::password('parola', array('class' => "input-xlarge", 'autocomplete' => 'off'))}}
      </div>
   </div>

   <div class="control-group">
      <label class="control-label" for="input01">Parola noua</label>
      <div class="controls">
         {{Form::password('newpassword', array('class' => "input-xlarge", 'autocomplete' => 'off'))}}
      </div>
   </div>

   <div class="control-group">
      <label class="control-label" for="input01">Confirma parola noua</label>
      <div class="controls">
         {{Form::password('newpassword_confirmation', array('class' => "input-xlarge", 'autocomplete' => 'off'))}}
      </div>
   </div>

   <div class="form-actions">
   {{Form::submit('Salveaza', array('class' => "btn btn-primary"))}}
   &nbsp;
   {{Form::reset('Reset', array('class' => 'btn'))}}
  </div>


</fieldset>



{{Form::close();}}

@stop