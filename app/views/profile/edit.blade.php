@extends('master')

@section('head')
   @parent
   {{ Html::style('css/bootstrap-fileupload.min.css') }}
   {{ Html::script('js/bootstrap-fileupload.min.js') }}
@stop

@section('content')

@section('breadcrumb')
   <li>
      <a href="{{URL::to("profile")}}">Profil</a>
      <span class="divider">/</span>
   </li>
   <li class="active">Edit</li>
@stop

{{ Form::open(array(
   'files' => 1,
   'method' => 'put',
   'route' => array('profile.update', $user->id),
   'class' => "form-horizontal well")) }}

<fieldset>
   <legend>Setari cont</legend>
   <div class="control-group">
   <label class="control-label" for="input01"></label>
      <div class="controls">
      <div class="fileupload fileupload-new" data-provides="fileupload">
         <div class="fileupload-new thumbnail" style="">
         @if($user->profile_image)
            {{Html::image(Config::get('sitesettings.uimgmtb') . $user->profile_image)}}
         @else
            {{Html::image('/img/noimage.gif')}}
         @endif
         </div>
         <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
         </div>
         <div>
            <span class="btn btn-file"><span class="fileupload-new">Select image</span>
            <span class="fileupload-exists">Change</span><input type="file" name="profile_image"/></span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
         </div>
      </div>
      </div>
   </div>

   <div class="control-group">
      <label class="control-label" for="input01">Prenume</label>
      <div class="controls">
         {{Form::text('prenume', Input::old('prenume') ? Input::old('prenume') : $user->firstname, array('class' => "input-xlarge"))}}
      </div>
   </div>

   <div class="control-group">
      <label class="control-label" for="input01">Nume</label>
      <div class="controls">
        {{Form::text('nume', Input::old('nume') ? Input::old('nume') : $user->lastname, array('class' => "input-xlarge"))}}
      </div>
   </div>

   <div class="control-group">
      <label class="control-label" for="input01">Alias</label>
      <div class="controls">
        {{Form::text('alias', Input::old('alias') ? Input::old('alias') : $user->alias, array('class' => "input-xlarge"))}}
        <p class="help-block">Poti seta un alias pentru a fi vizibil in site.</p>
      </div>
   </div>
   <div class="control-group">
      <label class="control-label" for="input01">Numele vizibil in site</label>
      <div class="controls">
         <label class="radio">
            {{Form::radio('user_pref', USER_PREF_SHOW_FULLNAME, Input::old('user_pref') ?  Input::old('user_pref') : $user->nic_option == USER_PREF_SHOW_FULLNAME ? true : (!$user->nic_option ? true : false));}}
            <span class="preffull">{{$user->firstname}} {{$user->lastname}}</span>(prenume nume)
         </label>

         <label class="radio">
            {{Form::radio('user_pref', USER_PREF_SHOW_NAMETRIM, Input::old('user_pref') ?  Input::old('user_pref') : $user->nic_option == USER_PREF_SHOW_NAMETRIM ? true : false);}}
            <span class="preffull">{{$user->firstname}} {{substr($user->lastname,0,1)}}</span>(prenume n.)
         </label>

         <label class="radio">
            {{Form::radio('user_pref', USER_PREF_SHOW_ALIAS, Input::old('user_pref') ?  Input::old('user_pref') : $user->nic_option == USER_PREF_SHOW_ALIAS ? true : false);}}
            <span class="prefalias">{{$user->alias}}</span>(alias)
         </label>
      </div>
   </div>

   <div class="control-group">
      <label class="control-label" for="input01">Gen</label>
      <div class="controls">
         {{Form::select('gen', $gen, $user->gen)}}
      </div>
   </div>

   <div class="control-group">
      <label class="control-label" for="disabledInput">Email</label>
      <div class="controls">
         {{Form::text('email', $user->email, array('class' => "input-xlarge disabled", "id" => "disabledInput", "placeholder" => $user->email, "disabled"=>"disabled"))}}
      </div>
   </div>

   <div class="control-group">
      <label class="control-label" for="input01">Vreau sa joc</label>
      <div class="controls">
         {{Form::select('sport[]', $sports, $user->sports, array('multiple'=>'multiple'))}}
      </div>
   </div>

   <div class="control-group">
      <label class="control-label" for="input01">in orasul</label>
      <div class="controls">
         {{Form::select('orase', $orase, $user->orase)}}
      </div>
   </div>

  <div class="form-actions">
   {{Form::submit('Modifica', array('class' => "btn btn-primary"))}}
   &nbsp;
   {{Form::reset('Reset', array('class' => 'btn'))}}
  </div>
   </fieldset>
   {{Form::close()}}

<script type="text/javascript">
$('input[name="prenume"]').keyup(function() {
   $('.preffull').html($(this).val() + ' ' + $('input[name="nume"]').val());
   $('.prefshort').html($(this).val() + ' ' + $('input[name="nume"]').val().substr(0,1) + '.');
   //get radio pref button
   //if it's 1 or two write it in span
});

$('input[name="nume"]').keyup(function() {
   $('.preffull').html($('input[name="prenume"]').val() + ' ' + $(this).val());
   $('.prefshort').html($('input[name="prenume"]').val() + ' ' + $(this).val().substr(0,1) + '.');
});

$('input[name="alias"]').keyup(function() {
   $('.prefalias').html($('input[name="alias"]').val());
});
</script>

@stop