@extends('master')


@section('content')

<div>
   <div class="pull-left">
      <h1>Vrei sa faci sport?</h1>
      <h2>E mai usor sa faci sport cu vreausajoc.ro</h2>

      <p>Vrei sa:
            <br />
            - faci sport, sa <b>joci tenis</b>, sa <b>joci fotbal</b>, sau orice alt sport si nu ai cu cine?
            <br />
            - participi sau sa organizeazi evenimente cu sporturile tale preferate?
            <br />
      </p>
      <h3><a href="{{URL::to('/register')}}">Inregistreaza-te</a> in comunitatea vreausajoc.ro.</h3>
      De la pasionati de sport pentru pasionati de sport.


       <h3>Ai o locatie, un teren sintetic sau sala de fitness? <a href="{{URL::to("locatii/create")}}" class="btn btn-warning">Adauga locatia ta</a></h3>
   </div>
   <div class="pull-right">
   <div class="panel panel-default pull-left" style="width: 260px">
     <div class="panel-heading">
         Ultimele noutati pe vreausajoc
      </div>
      <div class="panel-body">
         @foreach($ultimeleXnoutati as $object)
            {{Html::skipfirst('events', '<div class="smallseparator"></div>')}}
            <div>
               @if ($object instanceof UserEvent)
                  evenimentul <a href="{{route('events.show' , $object->id)}}">{{$object->event}}</a>
                  {{--, dmy2js($object->data) --}}
                  {{-- in <span>{{ucfirst(strtolower($object->orase->oras))}}.</span> --}}
               @endif
               @if ($object instanceof User)
                  user-ul {{a_profile($object)}} <!-- s-a inregistrat. -->
               @endif
               @if ($object instanceof Locatie)
                  locatia {{a_locatie($object)}} {{-- din <span>{{ucf($object->orase->oras)}}</span> --}}
               @endif
            </div>

         @endforeach
      </div>
   </div>

<?php
/*
{{--
<div class="panel panel-default pull-left" style="margin-right:10px;width: 280px">
  <div class="panel-heading">
      Ultimii useri inregistrati
   </div>
   <div class="panel-body">
      @foreach($last10users as $user)
      @if(!$user->alias)
         <?php continue;?>
      @endif
         {{Html::skipfirst('users', '<div class="smallseparator"></div>')}}
         <div>
            {{a_profile($user)}}
         </div>
      @endforeach
   </div>
</div>


<div class="panel panel-default pull-left" style="width: 280px">
  <div class="panel-heading">
      Ultimele locatii adaugate
   </div>
   <div class="panel-body">
      @foreach($last10locatii as $locatie)
         {{Html::skipfirst('locatie', '<div class="smallseparator"></div>')}}
         <div>
            {{a_locatie($locatie)}} din <span>{{ucf($locatie->orase->oras)}}</span>
         </div>
      @endforeach
   </div>
</div>
--}}
*/
?>

   </div>
</div>

@stop

{{--
<h4><a href="{{URL::to("locatii/create")}}" class="btn btn-primary">Adauga locatia ta</a></h4>
<h4><a href="{{URL::to("locatii/create")}}" class="btn btn-info">Adauga locatia ta</a></h4>
<h4><a href="{{URL::to("locatii/create")}}" class="btn btn-success">Adauga locatia ta</a></h4>
<h4><a href="{{URL::to("locatii/create")}}" class="btn btn-danger">Adauga locatia ta</a></h4>
<h4><a href="{{URL::to("locatii/create")}}" class="btn btn-inverse">Adauga locatia ta</a></h4>
<h4><a href="{{URL::to("locatii/create")}}" class="btn btn-warning">Adauga locatia ta</a></h4>


btn btn-primary   Provides extra visual weight and identifies the primary action in a set of buttons
Info  btn btn-info   Used as an alternative to the default styles
Success  btn btn-success   Indicates a successful or positive action
Warning  btn btn-warning   Indicates caution should be taken with this action
Danger   btn btn-danger Indicates a dangerous or potentially negative action
Inverse  btn btn-inverse   Alternate dark gray button, not tied to a semantic action or use
Link  btn btn-link
--}}