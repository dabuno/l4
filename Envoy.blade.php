@servers(['web' => 'bdurla@188.241.113.225'])

@task('deploy')
	cd /home/bdurla/vreausajoc.ro
   source /home/bdurla/.bash_profile
   pwd
   composer self-update
   php artisan down
   {{--composer install --no-dev --}}
   git pull origin
   php artisan migrate
   php artisan up
@endtask
