$(function() {

$.showError = function(msg, selector) {
   return $.showMsg(msg, selector, 'error')
};

$.showMsg = function(msg, selector, type) {
   type = type || null;
   $selector = $(selector);
   if (!type) {
      $selector.removeClass('alert-danger').addClass('alert-success');
   }
   else if (!$selector.hasClass('alert-danger')) {
      $selector.addClass('alert-danger').removeClass('alert-success');
   }

   if (!$selector.hasClass('alert')) {
      $selector.addClass('alert');  
   }

   $selector.html(msg);
   $(selector).show();
}

$.hideMsg = function(selector) {
   $(selector).hide();
}

});