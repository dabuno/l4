#!/usr/local/bin/php
<?php

$MAINPATH = getenv('VSJ_MAINPATH');
$BEANPATH = getenv('VSJ_BEANPATH');
$BEANHOST = getenv('VSJ_BEANHOST');
$BEANPORT = getenv('VSJ_BEANPORT');
$ARTISANPATH = $MAINPATH;


echo date('Y-m-d H:i:s') . "\n";

$longoptions = array("artisan::", "help::", "bean::");
$artisanoptions = array('stop', 'start', 'list');
$beanoptions = array('stop', 'start', 'list');

$options = getopt(null, $longoptions);

if (isset($options['help'])) {
   print_r($longoptions);
   print_r($artisanoptions);
   print_r($beanoptions);
}


if (isset($options['artisan'])) {

   if ($options['artisan'] == 'stop') {
      $pidOfArtisan = showArtisanPid();
      if ($pidOfArtisan) {
         $pidOfArtisan = exec("kill $pidOfArtisan");
         showArtisanPid();
      }
   }
   elseif ($options['artisan'] == 'list') {
      showArtisanPid();
   }
   elseif ($options['artisan'] == 'start') {
      $pidOfArtisan = showArtisanPid();
      if (!$pidOfArtisan) {
         startArtisan();
         showArtisanPid();
      }
   }

   else {
      echo 'unhandled value ['.$options['artisan'].'] for artisan' . "\n";
      print_r($artisanoptions);
   }

}

if (isset($options['bean'])) {
   if ($options['bean'] == 'stop') {
      $pidOfBean = showBeanPid();
      if ($pidOfBean) {
         $pidOfBean = exec("kill $pidOfBean");
         showBeanPid();
      }
   }
   elseif ($options['bean'] == 'list') {
      showBeanPid();
   }
   elseif ($options['bean'] == 'start') {
      $pidOfBean = showBeanPid();
      if (!$pidOfBean) {
         startBean();
         showBeanPid();
      }
   }

   else {
      echo 'unhandled value ['.$options['artisan'].'] for bean' . "\n";
      print_r($artisanoptions);
   }

}

function getBeanPid() {
   return exec("pidof -s beanstalkd");
}

function showBeanPid() {
   $pid = getBeanPid();
   echo "pidOfBean - $pid\n";

   return $pid;
}

function startBean() {
   global $BEANPATH, $BEANHOST, $BEANPORT, $BEANPATH;
   exec("$BEANPATH/beanstalkd $BEANHOST $BEANPORT -b $BEANPATH/beanstore > /dev/null 2>/dev/null &");
}


function getArtisanPid() {
   $pidOfArtisan = exec("ps -ef | grep 'artisan queue:listen' | grep -v grep | awk '{print $2}'");

   return $pidOfArtisan;
}

function showArtisanPid() {
   $pidOfArtisan = getArtisanPid();
   echo "pidOfArtisan - ".$pidOfArtisan."\n";

   return $pidOfArtisan;
}

function startArtisan() {
   global $MAINPATH;
   chdir($MAINPATH);
   exec("php artisan queue:listen > /dev/null 2>/dev/null &");
}