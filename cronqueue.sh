#!/bin/bash
#localhost
MAINPATH=$VSJ_MAINPATH
BEANPATH=$VSJ_BEANPATH
BEANHOST=$VSJ_BEANHOST
BEANPORT=$VSJ_BEANPORT
ARTISANPATH=$VSJ_MAINPATH
CRONLOG=${MAINPATH}/app/storage/logs/beanlog$(date +"%Y%m%d")

cd ${ARTISANPATH}

echo $(date +"%H:%M:%S") "starting queues" >> ${CRONLOG}

if [[ ! `pidof -s beanstalkd` ]]; then
   ${BEANPATH}/beanstalkd $BEANHOST $BEANPORT -b ${BEANPATH}/beanstore &
   echo $(date +"%H:%M:%S") "restarted beanstalkd pid "`pidof beanstalkd` >> ${CRONLOG}
fi

if [[ ! `ps -ef | grep 'artisan queue:listen' | grep -v grep | awk '{print $2}'` ]]; then

   #/usr/local/bin/php ${ARTISANPATH}artisan queue:listen >> ${CRONLOG} 2>&1
   php artisan queue:listen & >> ${CRONLOG} 2>&1

   #/usr/local/bin/php ${ARTISANPATH}artisan queue:listen --sleep=5 --timeout=60 &
   #/usr/local/bin/php ${ARTISANPATH}artisan queue:work --queue="default" --delay=0 --memory=128 --sleep=5 --env=production &

   echo $(date +"%H:%M:%S") "restarted php artisan queue pid "`ps -ef | grep 'artisan queue:listen' | grep -v grep | awk '{print $2}'` >> ${CRONLOG}
fi

# m h  dom mon dow   command
#* * * * * /home/bdurla/work/l4/cronqueue.sh > /home/bdurla/logs/cronlog 2>&1
#* * * * * sleep 15; /home/bdurla/work/l4/cronqueue.sh  > /home/bdurla/logs/cronlog 2>&1
#* * * * * sleep 30; /home/bdurla/work/l4/cronqueue.sh > /home/john/logs/backup.log 2>&1
#* * * * * sleep 45; /home/bdurla/work/l4/cronqueue.sh > /home/john/logs/backup.log 2>&1
