#include <ctype.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


#define CMDSTART "start"
#define CMDSTOP  "stop"


#define RUNCMD(cmd, ...) runCmd(cmd,## __VA_ARGS__, "r")


static
void usage(const char* progname) {
   (void)fprintf(stderr, "usage: %s [-a start|restart|stop] [-b start|restart|stop]\n\nOptions:\n -a artisan : start|restart|stop\n -b bean    : start|restart|stop\n", progname);
}

int runCmd(const char *cmd, ...) {
   int pid = 0;
   FILE *fp = NULL;
   char path[1025];
   va_list ap;

   char *mod = NULL;

   va_start (ap, cmd);         /* Initialize the argument list. */
   mod = va_arg(ap, char *);
   if (NULL == mod) {
      mod = "r";
   }
   va_end (ap);                  /* Clean up. */

#ifdef DEBUG
   printf("run command %s\n", cmd);
#endif

   fp = popen(cmd, "r");
   if (fp == NULL) {
      printf("Failed to run command %s\n", cmd);
      abort();
   }

   /* Read first line of the output */
   while (fgets(path, sizeof(path)-1, fp) != NULL) {
      pid = atoi(path);
      break;
   }

   pclose(fp);

   return pid;
}

int getBeanId() {
   int pid = 0;

   pid = RUNCMD("ps -ef | grep beanstalkd  | grep -v grep | awk '{print $2}'");

   return pid;
}

int getArtisanId() {
   int pid = 0;

   pid = RUNCMD("ps -ef | grep 'artisan queue:listen' | grep -v grep | awk '{print $2}'");

   return pid;
}

void startArtisan() {
   char* cmd = NULL;
   char* startCmd = NULL;
   char* vsjPath = getenv("VSJ_MAINPATH");
   startCmd = "cd %s ; php artisan queue:listen > /dev/null 2>/dev/null &";
   cmd = malloc(strlen(startCmd) + strlen(vsjPath));
   if (!sprintf(cmd, startCmd, vsjPath)) {
      fprintf (stderr, "failed to write cmd %s", startCmd);
      exit(1);
   }

   RUNCMD(cmd);


   if (NULL != cmd) {
      free(cmd);
   }
}

void stopArtisan() {
   char* cmd = NULL;
   char* stopCmd = NULL;
   stopCmd = "kill %d";
   cmd = malloc(strlen(stopCmd) + sizeof(int));
   if (!sprintf(cmd, stopCmd, getArtisanId())) {
      fprintf (stderr, "failed to write cmd %s", stopCmd);
      exit(1);
   }

   RUNCMD(cmd);


   if (NULL != cmd) {
      free(cmd);
   }
}

void startBean() {
   char* cmd = NULL;
   char* startCmd = NULL;
   char* beanPath = getenv("VSJ_BEANPATH");
   char* beanHost = getenv("VSJ_BEANHOST");
   char* beanPort = getenv("VSJ_BEANPORT");
   char* beanStor = getenv("VSJ_BEANSTORE");

   if (NULL == beanPath) {
      fprintf (stderr, "failed to get environment VSJ_BEANPATH");
      exit(1);
   }

   if (NULL == beanHost) {
      beanHost = "-l 127.0.0.1";
   }

   if (NULL == beanPort) {
      beanPort = "-p 11300";
   }

   if (NULL == beanStor) {
      beanStor = "";
   }

   startCmd = "cd %s ; ./beanstalkd %s %s %s > /dev/null 2>/dev/null &";
   cmd = malloc(strlen(startCmd) + strlen(beanPath) + strlen(beanHost) + strlen(beanPort) + strlen(beanStor));
   if (!sprintf(cmd, startCmd, beanPath, beanHost, beanPort, beanStor)) {
      fprintf (stderr, "failed to write cmd %s", startCmd);
      exit(1);
   }

#ifdef DEBUG
   printf("startbean cmd %s\n", cmd);
#endif

   RUNCMD(cmd);


   if (NULL != cmd) {
      free(cmd);
   }
}

void stopBean() {
   char* cmd = NULL;
   char* stopCmd = NULL;
   stopCmd = "kill %d";
   cmd = malloc(strlen(stopCmd) + sizeof(int));
   if (!sprintf(cmd, stopCmd, getBeanId())) {
      fprintf (stderr, "failed to write cmd %s", stopCmd);
      exit(1);
   }

   RUNCMD(cmd);


   if (NULL != cmd) {
      free(cmd);
   }
}



int
main(int argc, char**argv) {
   int artiflag = 0;
   int beanflag = 0;
   int beanpid = 0,artisanpid = 0;
   int c;
   char* artiarg = NULL;
   char* beanarg = NULL;

   while ((c = getopt (argc, argv, "a:b:")) != -1) {
      switch(c) {
         case('a'):
            artiflag = 1;
            artiarg = optarg;
            break;
         case('b'):
            beanflag = 1;
            beanarg = optarg;
            break;
         case '?':
            if (isprint (optopt))
               fprintf (stderr, "Unknown option `-%c'.\n", optopt);
            else
               fprintf (stderr,
                     "Unknown option character `\\x%x'.\n",
                     optopt);

         default:
            usage(argv[0]);
            exit(1);
      }
   }


   beanpid    = getBeanId();
   artisanpid = getArtisanId();
   printf("beanpid - %d\n", beanpid);
   printf("artisanpid - %d\n", artisanpid);


   if (artiflag && artiarg) {
      if (strcmp(CMDSTART,artiarg) == 0 && !artisanpid) {
         startArtisan();
         printf("artisanpid - %d\n", getArtisanId());
      }

      if (strcmp(CMDSTOP,artiarg) == 0 && artisanpid) {
         stopArtisan();
         printf("artisanpid - %d\n", getArtisanId());
      }
   }

   if (beanflag && beanarg) {
      if (strcmp(CMDSTART,beanarg) == 0 && !beanpid) {
         startBean();
         printf("beanpid - %d\n", getBeanId());
      }

      if (strcmp(CMDSTOP,beanarg) == 0 && beanpid) {
         stopBean();
         printf("beanpid - %d\n", getBeanId());
      }
   }

#ifdef DEBUG
   printf("artiarg - %s, artiflag %d\n", artiarg, artiflag);
   printf("beanarg - %s, beanflag %d\n", beanarg, beanflag);
#endif

   return 0;
}